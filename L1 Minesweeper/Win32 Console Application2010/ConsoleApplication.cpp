// Win32 Console Application.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "console.h"

int _tmain(int argc, _TCHAR* argv[])
{
	CConsole c;

	return c.Run(argc,argv);
}

