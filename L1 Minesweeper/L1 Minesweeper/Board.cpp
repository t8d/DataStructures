/***********************************************************************
* Author:		Tommy Miller
* Filename:		Board.cpp
* Date Created:	1/15/2016
***********************************************************************/

//Time.h for board generation.
#include "Board.h"
#include <time.h>

//Defines for key codes
#define KUP 72
#define KDOWN 80
#define KLEFT 75
#define KRIGHT 77
#define KM 109
#define KSPACE 0x20
#define KC 0x63

/***************************************************
* Purpose: Board Basic Constructor
*
* Precondition: None.
*
* Postcondition: Board is constructed.
*
* Note: This should NEVER be called. Only call the
*	2nd constructor with passed difficulty (to control
*	difficulty of game)
***************************************************/
Board::Board()
	:m_grid(nullptr), m_difficulty(Beginner), m_row(0), m_col(0), m_bombsleft(0), m_selectrow(0), m_selectcol(0), m_vbombs(0)
{ }

/***************************************************
* Purpose: Board 1 arg constructor. Initializes all elements.
*
* Precondition: Chosen difficulty passed in.
*
* Postcondition: Board is constructed.
***************************************************/
Board::Board(Difficulty diff)
	:m_grid(nullptr), m_finished(false), m_difficulty(diff), m_row(0), m_col(0), m_bombsleft(0), m_selectrow(0), m_selectcol(0), m_vbombs(0)
{ }

/***************************************************
* Purpose: Board Destructor
*
* Precondition: Board exists.
*
* Postcondition: Board doesn't exist.
***************************************************/
Board::~Board()
{
	delete m_grid;
}

/***************************************************
* Purpose: Board Copy Constructor.
*	(should have 0 need to call this)
*
* Precondition: Existing board to copy in.
*
* Postcondition: Board now has same data as copy.
***************************************************/
Board::Board(Board & copy)
{
	*this = copy;
}

/***************************************************
* Purpose: Board operator =, to copy data passed in
* to this board
*
* Precondition: Board is passed in.
*
* Postcondition: This Board now has same data as copy.
***************************************************/
Board& Board::operator=(Board& copy)
{
	if (this != &copy)
	{
		delete m_grid;
		m_grid = new Array2D<Cell>(copy.m_row, copy.m_col);
		*m_grid = *copy.m_grid;
		m_row = copy.m_row;
		m_col = copy.m_col;
		m_bombsleft = copy.m_bombsleft;
		m_finished = copy.m_finished;
		m_difficulty = copy.m_difficulty;
	}
	return *this;
}

/***************************************************
* Purpose: Start a game of Minesweeper
*
* Precondition: Board exists.
*
* Postcondition: Game of Minesweeper starts.
***************************************************/
void Board::Start()
{
	GenerateBoard();
	RefreshScreen();
}

/***************************************************
* Purpose: Generates a randomized board based by
*	difficulty.
*
* Precondition: Board exists.
*
* Postcondition: Board is generated with randomized
*	mine locations.
***************************************************/
void Board::GenerateBoard()
{
	//Creates board.
	delete m_grid;
	int bombsToCreate = 0;
	srand(time(nullptr));
	switch (m_difficulty)
	{
	case Beginner: //10x10 + 10 bombs
		m_grid = new Array2D<Cell>(10,10);
		bombsToCreate = 10;
		m_bombsleft = 10;
		m_vbombs = 10;
		m_row = 10;
		m_col = 10;
		break;
	case Intermediate: //16x16 + 40 bombs
		m_grid = new Array2D<Cell>(16, 16);
		bombsToCreate = 40;
		m_bombsleft = 40;
		m_vbombs = 40;
		m_row = 16;
		m_col = 16;
		break;
	case Expert: //16x30 + 100 bombs
		m_grid = new Array2D<Cell>(16, 30);
		bombsToCreate = 100;
		m_bombsleft = 100;
		m_vbombs = 100;
		m_row = 16;
		m_col = 30;
		break;
	} //No default, because it isn't possible to have any other case.
	//Inserts bombs
	int row = 0;
	int col = 0;

	while (bombsToCreate != 0)
	{
		row = rand() % (m_row);
		col = rand() % (m_col);

		if ((*m_grid)[row][col].Bomb != true)
		{
			(*m_grid)[row][col].Bomb = true;
			(*m_grid)[row][col].Blank = false;
			bombsToCreate--;
			UpdateNumbers(row, col);
		}
	}
}

/***************************************************
* Purpose: When bomb is placed, increment surrounding
* cell's numbers.
*
* Precondition: Board exists and Generate Board is
* working.
*
* Postcondition: Number of surrouding mines are incramented.
***************************************************/
void Board::UpdateNumbers(int row, int col)
{
	//This function will have Array2D handle the
	//out of bounds problems (by throwing an exception we just ignore and move on.)
	try //left
	{
		(*m_grid)[row][col - 1].Number++;
		(*m_grid)[row][col - 1].Blank = false;
	}
	catch (...)
	{ } //Ignoring.
	try //top left
	{
		(*m_grid)[row - 1][col - 1].Number++;
		(*m_grid)[row - 1][col - 1].Blank = false;
	}
	catch (...)
	{
	} //Ignoring.
	try //top
	{
		(*m_grid)[row - 1][col].Number++;
		(*m_grid)[row - 1][col].Blank = false;
	}
	catch (...)
	{
	} //Ignoring.
	try //top right
	{
		(*m_grid)[row - 1][col + 1].Number++;
		(*m_grid)[row - 1][col + 1].Blank = false;
	}
	catch (...)
	{
	} //Ignoring.
	try //right
	{
		(*m_grid)[row][col + 1].Number++;
		(*m_grid)[row][col + 1].Blank = false;
	}
	catch (...)
	{
	} //Ignoring.
	try //bottom right
	{
		(*m_grid)[row + 1][col + 1].Number++;
		(*m_grid)[row + 1][col + 1].Blank = false;
	}
	catch (...)
	{
	} //Ignoring.
	try //bottom
	{
		(*m_grid)[row + 1][col].Number++;
		(*m_grid)[row + 1][col].Blank = false;
	}
	catch (...)
	{
	} //Ignoring.
	try //bottom left
	{
		(*m_grid)[row + 1][col - 1].Number++;
		(*m_grid)[row + 1][col - 1].Blank = false;
	}
	catch (...)
	{
	} //Ignoring.
}

/***************************************************
* Purpose: Re-displays entire board, calls RefreshScreen()
*	and if a bomb is uncovered, the game is lost.
*
* Precondition: Board exists and Generate Board is
* working.
*
* Postcondition: The game commences
***************************************************/
void Board::RefreshScreen()
{
	while (m_finished == false)
	{
		system("cls");
		//Refresh screen
		ChangeColor(6);
		cout << "---------------------------------MINESWEEPER------------------------------------" << endl;
		cout << "-------------------------------By Tommy Miller----------------------------------" << endl;
		cout << endl;
		ChangeColor(0); //Reset to white.
		for (int i = 0; i < m_row; i++)
		{
			for (int j = 0; j < m_col; j++)
			{
				if (i == m_selectrow && j == m_selectcol)
				{ //Shows current curser if not on uncovered bomb.
					ChangeColor(5);
					cout << static_cast<char>(177) << ' ';
				}
				else {
					if ((*m_grid)[i][j].Bomb && !(*m_grid)[i][j].Covered) //Uncovered Bomb.
					{
						ChangeColor(1);
						cout << static_cast<char>(233) << ' ';
						m_finished = true;
					}
					else if ((*m_grid)[i][j].Marked) //Marked location
					{
						ChangeColor(4);
						cout << static_cast<char>(232) << ' ';
					}
					else if ((*m_grid)[i][j].Number != 0 && !(*m_grid)[i][j].Bomb && !(*m_grid)[i][j].Covered) //Uncovered Number
					{
						ChangeColor(3);
						cout << (*m_grid)[i][j].Number << ' ';
					}
					else if ((*m_grid)[i][j].Number == 0 && !(*m_grid)[i][j].Bomb && !(*m_grid)[i][j].Covered) //Uncovered Blank
					{
						ChangeColor(0);
						cout << "  ";
					}
					else //Covered Cell
					{
						ChangeColor(4);
						cout << static_cast<char>(254) << ' ';
					}
				}
			}
			cout << endl;
		}
		//Displays prompt, or lose message if lost.
		if (m_finished)
		{
			ChangeColor(1);
			cout << endl << "YOU LOST" << endl;
		}
		else
		{
			ChangeColor(2);
			cout << endl << "Move using arrow keys, mark with \"M\", or uncover with \"Space\" key" << endl;
			cout << "Press \"C\" to exit the game" << endl;
			PromptCommand();
		}
	}

}

/***************************************************
* Purpose: Prompts for user input.
*
* Precondition: Board exists and Generate Board is
* working.
*
* Postcondition: Prompts for user input, and reacts
* with known key inputs.
***************************************************/
void Board::PromptCommand()
{
	int command = 0;
	switch (command = _getch())
	{
	case KUP:
		if (m_selectrow != 0)
			m_selectrow--;
		break;
	case KDOWN:
		if (m_selectrow != m_row - 1)
			m_selectrow++;
		break;
	case KLEFT:
		if (m_selectcol != 0)
			m_selectcol--;
		break;
	case KRIGHT:
		if (m_selectcol != m_col - 1)
			m_selectcol++;
		break;
	case KM:
		if ((*m_grid)[m_selectrow][m_selectcol].Marked)
		{
			(*m_grid)[m_selectrow][m_selectcol].Marked = false;
			if ((*m_grid)[m_selectrow][m_selectcol].Bomb)
				m_bombsleft++;
			m_vbombs++;
			(*m_grid)[m_selectrow][m_selectcol].Blank = true;
		}
		else
		{
			(*m_grid)[m_selectrow][m_selectcol].Marked = true;
			if ((*m_grid)[m_selectrow][m_selectcol].Bomb)
				m_bombsleft--;
			m_vbombs--;
			(*m_grid)[m_selectrow][m_selectcol].Blank = false;
		}
		break;
	case KSPACE:
		if (!(*m_grid)[m_selectrow][m_selectcol].Marked)
		{
			if ((*m_grid)[m_selectrow][m_selectcol].Number != 0 && (*m_grid)[m_selectrow][m_selectcol].Covered)
			{
				(*m_grid)[m_selectrow][m_selectcol].Covered = false;
				(*m_grid)[m_selectrow][m_selectcol].Blank = false;
			}
			else if ((*m_grid)[m_selectrow][m_selectcol].Number == 0 && (*m_grid)[m_selectrow][m_selectcol].Covered)
			{
				(*m_grid)[m_selectrow][m_selectcol].Covered = false;
				(*m_grid)[m_selectrow][m_selectcol].Blank = false;
				FloodReveal(m_selectrow, m_selectcol);
			}
		}
		break;
	case KC:
		m_finished = true;
		break;
	}
	WinByUncoveringAllCells();
	if (m_bombsleft == 0 && (m_bombsleft == m_vbombs))
		WinGame();
}

/***************************************************
* Purpose: Flood reveals area around passed row/col
*	until it hits numbers or grid boundries.
*
* Precondition: The selected row/col is a blank cell.
*
* Postcondition: Flood reveals an area leaving only
*	numbers or grid boundries from the passed row/col
***************************************************/
void Board::FloodReveal(int row, int col)
{
	try //North
	{
		if ((*m_grid)[row - 1][col].Blank)
		{
			(*m_grid)[row - 1][col].Covered = false;
			(*m_grid)[row - 1][col].Blank = false;
			FloodReveal(row - 1, col);
		}
		if ((*m_grid)[row - 1][col].Number != 0 && !(*m_grid)[row - 1][col].Bomb)
		{
			(*m_grid)[row - 1][col].Covered = false;
			(*m_grid)[row - 1][col].Blank = false;
		}
	}
	catch (...)
	{  } //ignores if not possible.

	try //North East
	{
		if ((*m_grid)[row - 1][col + 1].Blank)
		{
			(*m_grid)[row - 1][col + 1].Covered = false;
			(*m_grid)[row - 1][col + 1].Blank = false;
			FloodReveal(row - 1, col + 1);
			
		}
		if ((*m_grid)[row - 1][col + 1].Number != 0 && !(*m_grid)[row - 1][col + 1].Bomb)
		{
			(*m_grid)[row - 1][col + 1].Covered = false;
			(*m_grid)[row - 1][col + 1].Blank = false;
		}
	}
	catch (...)
	{ } //ignores if not possible.

	try //East
	{
		if ((*m_grid)[row][col + 1].Blank)
		{
			(*m_grid)[row][col + 1].Covered = false;
			(*m_grid)[row][col + 1].Blank = false;
			FloodReveal(row, col + 1);
		}
		if ((*m_grid)[row][col + 1].Number != 0 && !(*m_grid)[row][col + 1].Bomb)
		{
			(*m_grid)[row][col + 1].Covered = false;
			(*m_grid)[row][col + 1].Blank = false;
		}
	}
	catch (...)
	{ } //ignores if not possible.

	try //South East
	{
		if ((*m_grid)[row + 1][col + 1].Blank)
		{
			(*m_grid)[row + 1][col + 1].Covered = false;
			(*m_grid)[row + 1][col + 1].Blank = false;
			FloodReveal(row + 1, col + 1);
		}
		if ((*m_grid)[row + 1][col + 1].Number != 0 && !(*m_grid)[row + 1][col + 1].Bomb)
		{
			(*m_grid)[row + 1][col + 1].Covered = false;
			(*m_grid)[row + 1][col + 1].Blank = false;
		}
	}
	catch (...)
	{ } //ignores if not possible.

	try //South
	{
		if ((*m_grid)[row + 1][col].Blank)
		{
			(*m_grid)[row + 1][col].Covered = false;
			(*m_grid)[row + 1][col].Blank = false;
			FloodReveal(row + 1, col);
		}
		if ((*m_grid)[row + 1][col].Number != 0 && !(*m_grid)[row + 1][col].Bomb)
		{
			(*m_grid)[row + 1][col].Covered = false;
			(*m_grid)[row + 1][col].Blank = false;
		}
	}
	catch (...)
	{ } //ignores if not possible.

	try //South West
	{
		if ((*m_grid)[row + 1][col - 1].Blank)
		{
			(*m_grid)[row + 1][col - 1].Covered = false;
			(*m_grid)[row + 1][col - 1].Blank = false;
			FloodReveal(row + 1, col - 1);
		}
		if ((*m_grid)[row + 1][col - 1].Number != 0 && !(*m_grid)[row + 1][col - 1].Bomb)
		{
			(*m_grid)[row + 1][col - 1].Covered = false;
			(*m_grid)[row + 1][col - 1].Blank = false;
		}
	}
	catch (...)
	{ } //ignores if not possible.

	try //West
	{
		if ((*m_grid)[row][col - 1].Blank)
		{
			(*m_grid)[row][col - 1].Covered = false;
			(*m_grid)[row][col - 1].Blank = false;
			FloodReveal(row, col - 1);
		}
		if ((*m_grid)[row][col - 1].Number != 0 && !(*m_grid)[row][col - 1].Bomb)
		{
			(*m_grid)[row][col - 1].Covered = false;
			(*m_grid)[row][col - 1].Blank = false;
		}
	}
	catch (...)
	{ } //ignores if not possible.

	try //North West
	{
		if ((*m_grid)[row + 1][col - 1].Blank)
		{
			(*m_grid)[row + 1][col - 1].Covered = false;
			(*m_grid)[row + 1][col - 1].Blank = false;
			FloodReveal(row + 1, col - 1);
		}
		if ((*m_grid)[row + 1][col - 1].Number != 0 && !(*m_grid)[row + 1][col - 1].Bomb)
		{
			(*m_grid)[row + 1][col - 1].Covered = false;
			(*m_grid)[row + 1][col - 1].Blank = false;
		}
	}
	catch (...)
	{ } //ignores if not possible.
}

/***************************************************
* Purpose: Displays winning message + sets
*	m_completed to true.
*
* Precondition: The game is won
*
* Postcondition: m_completed is set to true, and user
*	is notified of winning the game.
***************************************************/
void Board::WinGame()
{
	system("cls");
	cout << "----------------------------------YOU WIN---------------------------------------" << endl;
	cout << endl;
	cout << "Press any key to exit." << endl;
	_getch();
	m_finished = true;
}

/***************************************************
* Purpose: Detects if player won by uncovering all
*	non-bomb squares
*
* Precondition: Board exists and Generate Board is
* working.
*
* Postcondition: if user won by uncovering all
*	non-bomb cells, the player wins.
***************************************************/
void Board::WinByUncoveringAllCells()
{
	bool completed = true;
	for (int i = 0; i < m_row; i++)
	{
		for (int j = 0; j < m_col; j++)
		{
			if ((*m_grid)[i][j].Covered && !(*m_grid)[i][j].Bomb)
				completed = false;
		}
	}
	if (completed == true)
		WinGame();
}

/***************************************************
* Purpose: Changes current color to passed number
*	index.
*
* Precondition: None.
*
* Postcondition: Sets console's current curser's
*	color to passed index's choice.
*
* Index:
* 1) Red
* 2) Green
* 3) Blue
* 4) Red + Green
* 5) Red + Blue
* 6) Green + Blue
* 0) White
***************************************************/
void Board::ChangeColor(int number)
{
	HANDLE hStdout = 0;
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	switch (number)
	{
	case 1: //Red
		SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
		break;
	case 2: //Green
		SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		break;
	case 3: //Blue
		SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		break;
	case 4: //Red + Green
		SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		break;
	case 5: //Red + Blue
		SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		break;
	case 6: //Green + Blue
		SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		break;
	case 0: //White
		SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
		break;
	default:
		break;
	}
}
