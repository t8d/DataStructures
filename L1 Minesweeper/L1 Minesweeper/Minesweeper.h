#ifndef MINESWEEPER_H
#define MINESWEEPER_H

#include "Board.h"

/************************************************************************
* Class: Minesweeper
*
* Purpose: Handle the Minesweeper game instance.
*
* Manager functions:
* Minesweeper ( )
*
* Minesweeper (const Minesweeper & copy) //copy constructor.
*
* Minesweeper & operator = (const Minesweeper & copy)
*	for Minesweeper = Mineseeeper.
*
* ~Minesweeper() //Destructor
*
* Methods:
*
* Start() //Calls RestartGame() && DifficultyChoice() 
*	and calls boards start() to start game.
* RestartGame() //Recreates new board object.
* DifficultyChoice() //Have user select difficulty for game.
*
*************************************************************************/
class Minesweeper
{
public:
	Minesweeper();
	~Minesweeper();
	Minesweeper(Minesweeper & copy);
	Minesweeper & operator=(Minesweeper & copy);
	void Start();
private:
	void RestartGame();
	void DifficultyChoice();

	//Variables
	Board * m_board;
	Difficulty m_diff;

};

#endif
