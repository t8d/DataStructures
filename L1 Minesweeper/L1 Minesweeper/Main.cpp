/**************************************************
*
* Lab: 1	-	Minesweeper
*
* <Overview>
* This is Minesweeper! The beloved game
* known for being included in almost every version
* of Windows since 3.1, now making it's way to a
* classic old style of Console! Yes, thats right,
* you can probably play this game in MSDOS!
* (disclaimer, not tested for MSDOS).
*
* This game's rules are very simple.
* *You choose difficulty
* *You see a grid of boxes, all uncovered.
* *Choose a box by typing the coordinates, and hope
*	that it isn't a bomb.
* *Come across numbers while uncovering boxes.
* *Try and figure out what the numbers mean.
* *If you come across a box where you think it's a 
*	bomb: either dont uncover it, or flag it as
*	possible bomb location.
*
* *You win by uncovering all non-bomb box's, or
*	flagging all bombs correctly.
*
* Good luck!
*
* </overview>
*
* Note:
*
* No, I'm not going to tell the user how to play
*	this game. If they know from previous
*	knowledge, they have an advantage over the
*	new players.
*
**************************************************/

#include "Minesweeper.h"
#define _CRTDBG_MAP_ALLOC

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	Minesweeper game; //Start instance.
	game.Start(); //Starts game.
	return 0; //Ends program.
}