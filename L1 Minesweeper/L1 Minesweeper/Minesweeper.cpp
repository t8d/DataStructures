/***********************************************************************
* Author:		Tommy Miller
* Filename:		Minesweeper.cpp
* Date Created:	1/15/2016
***********************************************************************/

#include "Minesweeper.h"

/***************************************************
* Purpose: Minesweeper 0-arg Constructor
*
* Precondition: None.
*
* Postcondition: Minesweeper is constructed.
*
***************************************************/
Minesweeper::Minesweeper()
	:m_board(nullptr), m_diff(Beginner)
{ }

/***************************************************
* Purpose: Minesweeper destructor
*
* Precondition: Minesweeper exists
*
* Postcondition: Minesweeper is destructed
*
***************************************************/
Minesweeper::~Minesweeper()
{
	delete m_board;
}

/***************************************************
* Purpose: Minesweeper copy constructor
*
* Precondition: Passed Minesweeper exists.
*
* Postcondition: This Minesweeper carries a copy
*	of passed minesweeper's data.
*
***************************************************/
Minesweeper::Minesweeper(Minesweeper & copy)
{
	*this = copy;
}

/***************************************************
* Purpose: Minesweeper op=
*
* Precondition: This & copy exists
*
* Postcondition: This carries a copy of data from copy.
*
***************************************************/
Minesweeper & Minesweeper::operator=(Minesweeper & copy)
{
	if (this != &copy)
	{
		delete m_board;
		m_board = new Board(*copy.m_board);
		m_diff = copy.m_diff;
	}
	return *this;
}

/***************************************************
* Purpose: Start game
*
* Precondition: Minesweeper exists.
*
* Postcondition: The game is started.
*
***************************************************/
void Minesweeper::Start()
{
	RestartGame(); //Recreates a fresh new board.
	m_board->Start(); //Starts the game.
}

/***************************************************
* Purpose: Game Restarter
*
* Precondition: This exists.
*
* Postcondition: A new board object is created
*
***************************************************/
void Minesweeper::RestartGame()
{
	DifficultyChoice();
	delete m_board;
	m_board = new Board(m_diff);
}

/***************************************************
* Purpose: Asks user for difficulty choice
*
* Precondition: This exists.
*
* Postcondition: Difficulty is chosen and stored.
*
***************************************************/
void Minesweeper::DifficultyChoice()
{
	HANDLE hStdout = 0;
	int input;
	bool chosen = false;
	while (chosen == false)
	{
		//Implement choice of difficulty later.
		hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		cout << "--------------------------MINESWEEPER DIFFICULTY MENU---------------------------" << endl;
		cout << "-------------------------------By Tommy Miller----------------------------------" << endl;
		SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		cout << "Choice 1) Beginner" << endl;
		cout << "Choice 2) Intermediate" << endl;
		cout << "Chocie 3) Expert" << endl;
		cout << endl;
		cout << "Enter your difficulty: ";
		input = _getch();

		if (input == '1')
		{
			m_diff = Beginner;
			chosen = true;
		}
		else if (input == '2')
		{
			m_diff = Intermediate;
			chosen = true;
		}
		else if (input == '3')
		{
			m_diff = Expert;
			chosen = true;
		}
		else
		{
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
			system("cls"); //Bad practice, but it's much easier than using official methods.
			cout << "Invalid choice, please enter 1, 2, or 3" << endl;
			cout << "Press any key to continue" << endl;
			_getch();
			system("cls");
		}

	}
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	cout << endl << "Starting game, please wait" << endl;
	system("timeout 5");
	system("cls");
}
