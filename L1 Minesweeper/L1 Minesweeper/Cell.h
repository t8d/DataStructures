#ifndef CELL_H
#define CELL_H

typedef bool Flag; //Because it makes since.

/************************************************************************
* Struct: Cell
*
* Purpose: Simple struct carring information a Minesweeper cell needs
*	to carry, like if Covered, Blank, Bomb, Marked, and surrounding mines
*	count.
*************************************************************************/
struct Cell
{
	//Cell Flags + Default values
	Flag Covered = true;
	Flag Blank = true;
	Flag Bomb = false;
	Flag Marked = false;

	//Number of bombs surrounding this cell.
	int Number = 0; 
};

#endif
