#ifndef BOARD_H
#define BOARD_H

//Standard Namespaces.
#include <string>
#include <Windows.h>
#include <conio.h>
#include <iostream>

//Required Classes/Structs/Enums.
#include "Cell.h"		//is a struct
#include "Array2D.h"	//is a class
#include "Difficulty.h" //is an enum
using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::getchar;

/************************************************************************
* Class: Board
*
* Purpose: Handle the Minesweeper game instance.
*
* Manager functions:
* Board ( )
*
* Board (Difficulty diff)
* Constructor with pre-selected difficulty.
*
* Board (const Board & copy) //copy constructor.
*
* Board & operator = (const Board & copy) //handles Board = Board.
*
* ~Board() //Destructor
*
* Methods:
*
* Start() //Starts game.
* GenerateBoard() //Generates random game board.
* UpdateNumbers(int row, int col) //Updates surrounding cells around 
*	selected row/col's numbers ++.
* RefreshScreen() //Displays the current board
* PromptCommand() //Awaits and handles user input.
* FloodReveal() //Flood reveals board in case of hitting blank cell.
* WinGame() //Win game message.
* WinByUncoveringAllCells() //Detects if game is won by uncovering all squares.
* ChangeColor(int number) //Changes console's current color.
*
*************************************************************************/
class Board
{
private:
	//Manager Functions
	Board(); //Won't do much, since Difficulty was not defined.
	Board(Difficulty diff);
	~Board();
	Board(Board & copy);
	Board & operator=(Board & copy);

	//Game Functions
	void Start();
	void GenerateBoard(); //Generates Board. Only called once per game.
	void UpdateNumbers(int row, int col);
	void RefreshScreen(); //Displays new frame. 
						  //sidenote: Totally slowest game ever, running less than 1 frame per second, right?
	void PromptCommand(); //User will select coordinate.
	void FloodReveal(int row, int col);   //If Unreaveal reveals a 0, this flood reveals all surrounding cells.
	void WinGame();	      //Win Game Msg.
	void WinByUncoveringAllCells();
	static void ChangeColor(int number) ;

	//Friend Class
	friend class Minesweeper; //Only ever want Board to be created by Minesweeper class.

	//Data
	Array2D<Cell> * m_grid;
	bool m_finished;
	Difficulty m_difficulty;
	int m_row;
	int m_col;
	int m_vbombs;
	int m_bombsleft;
	int m_selectrow;
	int m_selectcol;
};

#endif
