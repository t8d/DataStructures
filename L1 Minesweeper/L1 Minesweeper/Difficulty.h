#ifndef DIFFICULTY_H
#define DIFFICULTY_H

/************************************************************************
* Enum: Difficulty
*
* Purpose: Data type carring one of 3 values, Beginner, Intermediate,
*	or Expert.
*************************************************************************/
enum Difficulty
{
	Beginner,
	Intermediate,
	Expert
};


#endif
