#ifndef ARRAY_H
#define ARRAY_H

#include "Exception.h"
/************************************************************************
* Class: Array
*
* Purpose: This class creates a dynamic one-dimensional array that can be
* started at any base.
*
* Manager functions:
* Array ( )
* The default size is zero and the base is zero.
*
* Array (int length, int start_index = 0)
* Creates an appropriate sized array with the starting index
*              either zero or the supplied starting value.
*
* Array (const Array & copy)
*
* operator = (const Array & copy)
*
* ~Array()
*
* Methods:
*
* operator [ ] (int index)
*
* int getStartIndex ()
* Returns the start index.
*
* setStartIndex (int start_index)
* Set's the start index.
*
* int getLength ()
* Returns the length of array.
*
* setLength (int length)
* Sets the length of array.
*************************************************************************/
//Error messages


template <typename T>
class Array
{
private:
	T * m_array;
	int m_length;
	int m_start_index;

public:
	Array();
	Array(int length, int start_index = 0);
	Array(const Array&);
	~Array();
	Array & operator=(const Array &);
	T & operator[](int index);
	int getStartIndex() const;
	void setStartIndex(int start_index);
	int getLength() const;
	void setLength(int length);
};

/***************************************************
* Purpose: Array 0-arg Constructor
*
* Precondition: None.
*
* Postcondition: Array is constructed.
*
***************************************************/
template <typename T>
Array<T>::Array()
	:m_array(nullptr), m_length(0), m_start_index(0)
{ }

/***************************************************
* Purpose: Array 2-arg Constructor
*
* Precondition: None.
*
* Postcondition: Array is constructed.
*
***************************************************/
template <typename T>
Array<T>::Array(int length, int start_index = 0)
	: m_array(nullptr), m_length(length), m_start_index(start_index)
{
	if (length < 0)
	{
		Exception length_exception("Length of array cannot be a negative value, defaulting to 0.");
		cout << length_exception << endl;
		m_length = 0; //replace with exception throwing from Exception class
	}

	if (length >= 1)
		m_array = new T[m_length];
}

/***************************************************
* Purpose: Array copy Constructor
*
* Precondition: Passed constructor exists.
*
* Postcondition: Array is constructed with
*	copy of copy's data.
*
***************************************************/
template <typename T>
Array<T>::Array(const Array<T>& copy)
	:m_length(copy.m_length), m_start_index(copy.m_start_index)
{
	m_array = new T[m_length];

	for (int i = 0; i < m_length; i++)
	{
		m_array[i] = copy.m_array[i];
	}
}

/***************************************************
* Purpose: Array destructor
*
* Precondition: This exists
*
* Postcondition: Array is destructed.
*
***************************************************/
template <typename T>
Array<T>::~Array()
{
	delete [] m_array;
}

/***************************************************
* Purpose: Array op=
*
* Precondition: this exists.
*
* Postcondition: Array carries copy of passed array's
*	data.
*
***************************************************/
template <typename T>
Array<T>& Array<T>::operator=(const Array<T>& copy)
{
	if (this != &copy)
	{
		delete[] m_array; //delete's old, because it is going to be replaced

		m_start_index = copy.m_start_index;
		m_length = copy.m_length;
		m_array = new T[m_length];

		for (int i = 0; i < m_length; i++)
		{
			m_array[i] = copy.m_array[i];
		}
	}
	return *this;
}

/***************************************************
* Purpose: Array op[]
*
* Precondition: This exists with stored data.
*
* Postcondition: Returns requested data by index value.
*
***************************************************/
template <typename T>
T& Array<T>::operator[](int index)
{
	T * out = nullptr;
	if (m_length != 0)
	{
		if ((index - m_start_index) >= m_length)
		{
			throw Exception("Index out of bounds! (higher than last index)");
		}
		else if ((index - m_start_index) < 0)
		{
			throw Exception("Index out of bounds! (lower than start index)");
		}
		else
		{
			out = &m_array[index - m_start_index];
		}
	}
	else
	{
		throw Exception("Array cannot be index'd because there is no array!");
	}
	return *out;
}

/***************************************************
* Purpose: Getter for StartIndex value
*
* Precondition: this exists
*
* Postcondition: Returns the start index for array.
*
***************************************************/
template <typename T>
int Array<T>::getStartIndex() const
{
	return m_start_index;
}

/***************************************************
* Purpose: Setter for start index
*
* Precondition: this exists
*
* Postcondition: Sets start index to passed value.
*
***************************************************/
template <typename T>
void Array<T>::setStartIndex(int start_index)
{
	m_start_index = start_index;
}

/***************************************************
* Purpose: Getter for length of array
*
* Precondition: this exists
*
* Postcondition: Value of length is returned.
*
***************************************************/
template <typename T>
int Array<T>::getLength() const
{
	return m_length;
}

/***************************************************
* Purpose: Setter for length
*
* Precondition: this exists
*
* Postcondition: Resizes the size of stored array to
*	requested value (unless negative)
*
***************************************************/
template <typename T>
void Array<T>::setLength(int length)
{
	if (length > 0)
	{
		T * old = m_array;
		m_array = new T[length];
		for (int i = 0; i < length; i++)
		{
			if (i < m_length)
				m_array[i] = old[i];
		}
		delete [] old;
		m_length = length;
	}
	else
	{
		if (length != 0)
		{
			Exception lengtherror_exception(LENGTHERROR);
			cout << lengtherror_exception << endl;
		}
		delete [] m_array;
		m_array = nullptr;
		m_length = 0;
	}
}

#endif