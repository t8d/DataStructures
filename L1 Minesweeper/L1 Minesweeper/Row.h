#ifndef ROW_H
#define ROW_H

#include "Exception.h"

/************************************************************************
* Class: Row
*
* Purpose: This class acts and handles the row portion of a 2D array.
*
* Manager functions:
* Row(Array2D<T> arrayIn, int row)
* ~Row()
* T & operator[](int column) //Access data from m_array2D.
*
*************************************************************************/

template <typename T> class Array2D;
template <typename T>
class Row
{
private:
	Array2D<T> & m_array2D;
	int m_row;

public:
	Row(Array2D<T> & arrayIn, int row);
	~Row(); //Nothing to delete.
	T & operator[](int column);
};


#endif

/***************************************************
* Purpose: Row 2 arg constructor
*
* Precondition: passed in Array2D's object and row
*	index.
*
* Postcondition: Row is constructed.
***************************************************/
template<typename T>
inline Row<T>::Row(Array2D<T> & array, int row)
	:m_array2D(array), m_row(row)
{ }

/***************************************************
* Purpose: Row destructor
*
* Precondition: this exists.
*
* Postcondition: Row is destructed.
***************************************************/
template<typename T>
inline Row<T>::~Row()
{ }

/***************************************************
* Purpose: Row operator[]
*
* Precondition: this exists.
*
* Postcondition: Returns requested data value from
*	passed in index's.
***************************************************/
template<typename T>
inline T & Row<T>::operator[](int column)
{
	//Bounds testing. Messages are self-documenting.
	int maxrow = m_array2D.getRow();
	int maxcol = m_array2D.getColumn();
	if (m_row >= maxrow) 
		throw Exception("Row is out of bounds!");
	else if (m_row < 0)
		throw Exception("Row cannot be a negative number!");
	else if (column >= maxcol)
		throw Exception("Column is out of bounds!");
	else if (column < 0)
		throw Exception("Column cannot be a negative number!");

	//Good value.
	return m_array2D.Select(m_row, column);
}
