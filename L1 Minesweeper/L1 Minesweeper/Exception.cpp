/***********************************************************************
* Author:		Tommy Miller
* Filename:		Exception.cpp
* Date Created:	1/15/2016
***********************************************************************/
#include "Exception.h"

/***************************************************
* Purpose: Exception 0-arg Constructor
*
* Precondition: None.
*
* Postcondition: Exception is constructed.
*
***************************************************/
Exception::Exception()
	:m_msg(nullptr)
{ }

/***************************************************
* Purpose: Exception 1-arg Constructor
*
* Precondition: None.
*
* Postcondition: Exception is constructed with message.
*
***************************************************/
Exception::Exception(const char * msg)
{
	m_msg = new char[strlen(msg) + 1];
	strcpy(m_msg, msg);
}

/***************************************************
* Purpose: Exception Copy Constructor
*
* Precondition: Existing Exception is passed in.
*
* Postcondition: Exception is constructed with passed-
*	in's Exceptions message.
*
***************************************************/
Exception::Exception(Exception & copy)
{
	if (this != &copy)
	{
		m_msg = new char[strlen(copy.m_msg) + 1];
		strcpy(m_msg, copy.m_msg);
	}
}

/***************************************************
* Purpose: Exception Destructor
*
* Precondition: This exception exists
*
* Postcondition: Exception is destructed.
*
***************************************************/
Exception::~Exception()
{
	delete [] m_msg;
}

/***************************************************
* Purpose: Exception operator=
*
* Precondition: Passed exception exists
*
* Postcondition: coppies copy's message to this
*	Exception.
*
***************************************************/
Exception & Exception::operator=(const Exception & copy)
{
	if (this != &copy)
	{
		delete[] m_msg;
		m_msg = new char[strlen(copy.m_msg) + 1];
		strcpy(m_msg, copy.m_msg);
	}
	return *this;
}

/***************************************************
* Purpose: Get Exception Message
*
* Precondition: Exception exists
*
* Postcondition: pointer to m_msg is returned.
*
***************************************************/
const char * Exception::getMessage() const
{
	return m_msg;
}

/***************************************************
* Purpose: Set's Exceptions message to passed msg
*
* Precondition: passed msg is a pointer to a real
*	char *
*
* Postcondition: New message is stored in this.
*
***************************************************/
void Exception::setMessage(const char * msg)
{
	delete[] m_msg;
	m_msg = new char[strlen(msg) + 1];
	strcpy(m_msg, msg);
}

/***************************************************
* Purpose: Exception operator<< to output stored msg
*
* Precondition: This exists, and this carries a real msg.
*
* Postcondition: msg is passed to ostream.
*
***************************************************/
ostream & operator<<(ostream & stream, const Exception & except)
{
	if (except.m_msg != nullptr)
		stream << except.m_msg;
	return stream;
}
