#ifndef ARRAY2D_H
#define ARRAY2D_H

#include "Array.h"
#include "Row.h"

/************************************************************************


* Class: Array2D
*
* Purpose: This class creates a dynamic two-dimensional array that's
* values can be accessed by expected double-array notation.
* example: Array2D<int> test(2,4);
*		   test[1][2] = 4;
*
* Manager functions:
* Array2D ( )
* The default size is zero.
*
* Array2D (int row, int col)
* Creates an single dimensional array length = row * col.
*
* Array2D (const Array2D & copy) //copy constructor.
*
* Array2D & operator = (const Array2D & copy) //handles Array2D = Array2D.
*
* ~Array() //Destructor
*
* Methods:
*
* Row<T> operator [ ] (int index) //Returns a very temporary Row object to
* use it's [] operator to find the correct address, that itself returns a T &.
*
* int getRow(). //returns m_row.
*
* void setRow(int rows). //sets m_row, and recreates the m_array to the new size.
*
* getColumn() //returns m_col.
*
* setColumn(int columns) //sets m_col, and recreates the m_array to the new size.
*
* T & Select(int row, int column) //non-operator form of Array[][].
*
*************************************************************************/

template <typename T>
class Array2D
{
private:
	Array<T> m_array;
	int m_row;
	int m_col;
public:
	Array2D();
	Array2D(int row, int col);
	Array2D(const Array2D & copy);
	~Array2D();
	Array2D & operator=(const Array2D & copy);
	Row<T> operator[](int index);
	int getRow() const;
	void setRow(int rows);
	int getColumn() const;
	void setColumn(int columns);
	T & Select(int row, int column);
};

/***************************************************
* Purpose: Array2D 0-arg Constructor
*
* Precondition: None.
*
* Postcondition: Array2D is constructed.
*
***************************************************/
template<typename T>
inline Array2D<T>::Array2D()
	:m_array(0), m_row(0), m_col(0)
{ }

/***************************************************
* Purpose: Array2D 2-arg Constructor
*
* Precondition: None.
*
* Postcondition: Array2D is constructed with requested
*	size.
*
***************************************************/
template<typename T>
inline Array2D<T>::Array2D(int row, int col)
	:m_array(row * col), m_row(row), m_col(col)
{ }

/***************************************************
* Purpose: Array2D copy Constructor
*
* Precondition: None.
*
* Postcondition: Array is constructed with copy of
*	copy's data.
*
***************************************************/
template<typename T>
inline Array2D<T>::Array2D(const Array2D & copy)
	:m_row(0), m_col(0)
{
	*this = copy;
}

/***************************************************
* Purpose: Array2D destructor
*
* Precondition: None.
*
* Postcondition: Array2D is destructed.
*
***************************************************/
template<typename T>
inline Array2D<T>::~Array2D()
{ }

/***************************************************
* Purpose: Array2D op=
*
* Precondition: this exists
*
* Postcondition: This gets copy of copy's data.
*
***************************************************/
template<typename T>
inline Array2D<T> & Array2D<T>::operator=(const Array2D & copy)
{
	if (this != &copy)
	{
		m_array = copy.m_array;
		m_row = copy.m_row;
		m_col = copy.m_col;
	}
	return *this;
}

/***************************************************
* Purpose: Array2D op[]
*
* Precondition: this exists
*
* Postcondition: Returns very temporary row carrying
*	this passed index + array data.
*
***************************************************/
template<typename T>
Row<T> Array2D<T>::operator[](int index)
{
	return Row<T>(*this, index);
}

/***************************************************
* Purpose: Get row size
*
* Precondition: this exists
*
* Postcondition: Returns the size of Row
*
***************************************************/
template<typename T>
inline int Array2D<T>::getRow() const
{
	return m_row;
}

/***************************************************
* Purpose: Setter for row
*
* Precondition: this exists
*
* Postcondition: Rezises array with new requested row
*	size
*
***************************************************/
template<typename T>
inline void Array2D<T>::setRow(int rows)
{
	if (rows < 0)
	{
		cout << Exception("Row cannot be set to a negative number, defaulting to 0.") << endl;
		rows = 0;
		m_col = 0;
	}
	m_row = rows;
	m_array.setLength(m_row * m_col);
}

/***************************************************
* Purpose: Getter for column
*
* Precondition: this exists
*
* Postcondition: Returns column size of Array2D
*
***************************************************/
template<typename T>
inline int Array2D<T>::getColumn() const
{
	return m_col;
}

/***************************************************
* Purpose: Setter for column
*
* Precondition: this exists
*
* Postcondition: Resizes array2d to requested column
*	size.
*
***************************************************/
template<typename T>
inline void Array2D<T>::setColumn(int columns)
{
	if (columns < 0)
	{
		cout << Exception("Columns cannot be set to a negative number, defaulting to 0.") << endl;
		columns = 0;
		m_row = 0;
	}
	m_col = columns;
	m_array.setLength(m_row * m_col);
}

/***************************************************
* Purpose: Select and return requested data.
*
* Precondition: this exists
*
* Postcondition: Returns the data at correct index.
*
***************************************************/
template<typename T>
inline T & Array2D<T>::Select(int row, int column)
{
	return m_array[(row * m_col) + column];
}

#endif