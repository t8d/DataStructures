#include "Array.h"
#define _CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC

#include <string>
using std::string;

int main()
{
	//Memory leak checker
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	//

	try //One try to catch them all.
	{
		try
		{
			cout << "Creating an array of 10 int's." << endl;
			Array<int> thingy(10);
			cout << "Adding values into the 10 ints" << endl;
			for (int i = 0; i < 10; i++)
			{
				thingy[i] = i;
				cout << "Adding " << i << " to array[" << i << "]" << endl;
			}
			cout << "Finished adding to array!" << endl;
			cout << "Displaying array: " << endl;

			for (int i = 0; i < 10; i++)
			{
				cout << thingy[i] << endl;
			}
			cout << "Done displaying!" << endl;

			cout << "Testing creating an array of char's with a negative length." << endl;
			Array<char> lenghprob(-10);
			cout << "Testing creating an array of 10 int's with start index of -10" << endl;
			Array<int> thingy2(10, -10);
			cout << "Adding numbers to that array" << endl;
			for (int i = -10; i < 0; i++)
			{
				thingy2[i] = i;
				cout << "Adding " << i << " to array[" << i << "]" << endl;
			}
			cout << "Finished adding to array!" << endl;
			cout << "Displaying array: " << endl;

			for (int i = -10; i < 0; i++)
			{
				cout << thingy2[i] << endl;
			}
			cout << "Done displaying!" << endl;

			cout << "Changing length to 5 (losing data)" << endl;
			thingy2.setLength(5);
			cout << "Displaying array[-6]" << endl;
			cout << thingy2[-6] << endl;
			cout << "Now purposly causing index error by calling array[-5]. (high)" << endl;
			thingy2[-5] = 1; //should throw error and land in catch below.
		}
		catch (Exception problem)
		{
			cout << problem << endl;
		}

		try
		{
			cout << "Creating another int array to test index error (low)" << endl;
			Array<int> again(10);
			again[-1] = 1;
		}
		catch (Exception problem)
		{
			cout << problem << endl;
		}
		try
		{
			cout << "Creating another int array to test setting existing array to length 0." << endl;
			Array<int> again2(10);
			again2.setLength(0);
			cout << "Testing accessing array[0]" << endl;
			again2[0] = 1;
		}
		catch (Exception problem)
		{
			cout << problem << endl;
		}

		cout << "Testing copy constructors." << endl;
		Array<int> from(10);
		for (int i = 0; i < 10; i++)
			from[i] = i;
		Array<int> to(from);
		cout << "Done copying, testing to see if contents match." << endl;

		bool good = true;
		for (int i = 0; i < 10; i++)
			if (from[i] != to[i])
				good = false;
		cout << "Matched (0 or 1): " << good << endl;
		Exception test1("Exception copy constructor works!");
		Exception test2(test1);
		cout << test2 << endl << endl;

		cout << "Testing op='s" << endl;
		to[3] = -9;
		from = to;
		if (from[3] == -9)
			cout << "Array op= works!" << endl;
		else
			cout << "Array op= failed!" << endl;

		test2.setMessage("Exception's op= works!");
		test1 = test2;
		cout << test1 << endl;

		from.setStartIndex(50);
		if (from.getStartIndex() == 50)
			cout << "Set and Get StartIndex works!" << endl;
		else
			cout << "Either the setter or getter for StartIndex is borked." << endl;

		from.setLength(5);
		if (from.getLength() == 5)
			cout << "Set and Get Length works correctly!" << endl;
		else
			cout << "Either the setter or getter for Length is borked." << endl;


		cout << "Testing with String's" << endl;

		try {
			Array<std::string> strtest(15);

			strtest[0] = "Hello ";
			strtest[1] = "World! ";
			strtest[3] = "\n";
			strtest[4] = "How ";
			strtest[5] = "are ";
			strtest[6] = "you ";
			strtest[7] = "doing ";
			strtest[8] = "today";
			strtest[9] = "? ";
			strtest[10] = "\n";
			strtest[11] = "\t";
			strtest[12] = "One ";
			strtest[13] = "Last ";
			strtest[14] = "Time ";

			for (int i = 0; i < 15; i++)
			{
				cout << strtest[i];
			}
			cout << endl;
			cout << "Finished testing holding String's in the Array." << endl;
		}
		catch (Exception problem)
		{
			cout << problem << endl;
		}

		cout << "Done testing Array & Exception class., exiting program." << endl;

	}
	catch (Exception problem)
	{
		cout << problem << endl;
	}
	return 0;
}