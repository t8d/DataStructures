#ifndef BACKWARDSITERATOR_H
#define BACKWARDSITERATOR_H

#include "ListIterator.h"

template <typename T>
class BackwardsIterator : public ListIterator<T>
{
public:
	void MoveNext() override;
	bool IsDone() override;
	void Reset() override;
	BackwardsIterator(DoubleLinkedList<T> * instance);
	BackwardsIterator(const BackwardsIterator * copy);
	BackwardsIterator & operator=(const BackwardsIterator * copy);
	~BackwardsIterator();
private:
};

template <typename T>
BackwardsIterator<T>::BackwardsIterator(DoubleLinkedList<T>* instance)
	:ListIterator(instance)
{ }

template <typename T>
BackwardsIterator<T>::BackwardsIterator(const BackwardsIterator* copy)
	:ListIterator(copy)
{ }

template <typename T>
BackwardsIterator<T>& BackwardsIterator<T>::operator=(const BackwardsIterator* copy)
{
	if (this != &copy)
	{
		ListIterator<T>::operator=(copy);
	}
	return *this;
}

template <typename T>
BackwardsIterator<T>::~BackwardsIterator()
{ }

template <typename T>
void BackwardsIterator<T>::MoveNext()
{
	if (ListIterator<T>::m_current == nullptr)
	{
		throw "Cannot move next, iterator is already finished!";
	}
	ListIterator<T>::m_current = ListIterator<T>::m_current->GetPrev();
}

template <typename T>
bool BackwardsIterator<T>::IsDone()
{
	return ListIterator<T>::m_current == nullptr;
}

template<typename T>
inline void BackwardsIterator<T>::Reset()
{
	ListIterator<T>::m_current = ListIterator<T>::m_instance->getTail();
	AbstractIterator<T>::undefinedState = false;
}

#endif