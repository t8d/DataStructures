#ifndef ABSTRACTITERATOR_H
#define ABSTRACTITERATOR_H

template <typename T>
class AbstractIterator
{
public:
	AbstractIterator();
	AbstractIterator(const AbstractIterator & copy);
	AbstractIterator & operator=(const AbstractIterator & copy);
	virtual ~AbstractIterator();
	virtual void MoveNext() = 0;
	virtual void Reset() = 0;
	virtual bool IsDone() = 0;
	virtual T GetCurrent() = 0;
	bool GetUndefinedState() const;
	template <typename U>
	friend class DoubleLinkedList;
protected:
	bool undefinedState;
};

template <typename T>
inline AbstractIterator<T>::AbstractIterator()
	:undefinedState(true)
{ }

template <typename T>
inline AbstractIterator<T>::AbstractIterator(const AbstractIterator& copy)
	:undefinedState(copy.undefinedState)
{
	*this = copy;
}

template<typename T>
inline AbstractIterator<T> & AbstractIterator<T>::operator=(const AbstractIterator & copy)
{
	if (this != &copy)
		undefinedState = copy.undefinedState;
	return *this;
}

template<typename T>
inline AbstractIterator<T>::~AbstractIterator()
{ }

template<typename T>
inline bool AbstractIterator<T>::GetUndefinedState() const
{
	return undefinedState;
}

#endif