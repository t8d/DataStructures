#ifndef FORWARDITERATOR_H
#define FORWARDITERATOR_H

#include "ListIterator.h"

template <typename T>
class ForwardsIterator : public ListIterator<T>
{
public:
	void MoveNext() override;
	bool IsDone() override;
	void Reset() override;
	ForwardsIterator(DoubleLinkedList<T> * instance);
	ForwardsIterator(const ForwardsIterator * copy);
	ForwardsIterator & operator=(const ForwardsIterator * copy);
	~ForwardsIterator();
private:
};

template <typename T>
ForwardsIterator<T>::ForwardsIterator(DoubleLinkedList<T>* instance)
	:ListIterator(instance)
{ }

template <typename T>
ForwardsIterator<T>::ForwardsIterator(const ForwardsIterator* copy)
	:ListIterator(copy)
{ }

template <typename T>
ForwardsIterator<T>& ForwardsIterator<T>::operator=(const ForwardsIterator* copy)
{
	if (this != &copy)
	{
		ListIterator<T>::operator=(copy);
	}
	return *this;
}

template <typename T>
ForwardsIterator<T>::~ForwardsIterator()
{ }

template <typename T>
void ForwardsIterator<T>::MoveNext()
{
	if (ListIterator<T>::m_current == nullptr)
	{
		throw "Cannot move next, iterator is already finished!";
	}
	ListIterator<T>::m_current = ListIterator<T>::m_current->GetNext();
}

template <typename T>
bool ForwardsIterator<T>::IsDone()
{
	return ListIterator<T>::m_current == nullptr;
}
template<typename T>
inline void ForwardsIterator<T>::Reset()
{
	ListIterator<T>::m_current = ListIterator<T>::m_instance->getHead();
	AbstractIterator<T>::undefinedState = false;
}
#endif
