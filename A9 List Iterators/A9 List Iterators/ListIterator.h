#ifndef LISTITERATOR_H
#define LISTITERATOR_H

#include "AbstractIterator.h"

template <typename T>
class ListIterator : public AbstractIterator<T>
{
public:
	ListIterator(DoubleLinkedList<T> * instance);
	ListIterator(const ListIterator & copy);
	ListIterator & operator=(const ListIterator & copy);
	virtual ~ListIterator();
	T GetCurrent() override;

protected:
	DoubleLinkedList<T> * m_instance;
	Node<T> * m_current;
	Node<T> * GetHead();
};

template<typename T>
inline ListIterator<T>::ListIterator(DoubleLinkedList<T>* instance)
	:m_instance(instance), m_current(nullptr)
{
	m_current = m_instance->getHead();
}

template<typename T>
inline ListIterator<T>::ListIterator(const ListIterator & copy)
	:m_instance(copy.m_instance), m_current(nullptr)
{ }

template<typename T>
inline ListIterator<T> & ListIterator<T>::operator=(const ListIterator & copy)
{
	if (this != &copy)
	{
		m_instance = copy.m_instance;
		m_current = copy.m_current;
	}
	return *this;
}

template<typename T>
inline ListIterator<T>::~ListIterator()
{ }

template<typename T>
inline T ListIterator<T>::GetCurrent()
{
	if (m_current == nullptr)
		throw "Iterator is finished!";
	return m_current->GetData();
}

template <typename T>
Node<T>* ListIterator<T>::GetHead()
{
	return m_instance->getHead();
}
#endif
