#include <iostream>
#include "doublelinkedlist.h"
using std::cout;
using std::endl;

int main()
{
	cout << "Creating list and adding a few items" << endl;
	DoubleLinkedList<int> list;
	list.Append(2);
	list.Append(3);
	list.Append(6);
	list.Append(488);
	cout << "Creating Forwards and Backwards iterators" << endl;
	AbstractIterator<int> * fIter = list.CreateFordwardsIterator();
	AbstractIterator<int> * bIter = list.CreateBackwardsIterator();

	cout << "Testing iterating forwards through list" << endl;
	for (fIter->Reset();!fIter->IsDone();fIter->MoveNext())
		cout << fIter->GetCurrent() << endl;
	cout << "Testing moving next past it's list" << endl;
	try
	{
		fIter->MoveNext();
	}
	catch (char * msg)
	{
		cout << msg << endl;
	}
	cout << "Testing iterating backwards through list" << endl;
	for (bIter->Reset(); !bIter->IsDone(); bIter->MoveNext())
		cout << bIter->GetCurrent() << endl;
	cout << "Testing moving next past it's list" << endl;
	try
	{
		bIter->MoveNext();
	}
	catch (char * msg)
	{
		cout << msg << endl;
	}

	//if you delete something from list, the Undefined bool gets set. Reset puts set's it to false.
	//Creating the iterator, and deleting item from list sets it to true. Purge resets the list for you.
	//To get the defined state, call this.
	bIter->GetUndefinedState(); //returns true if in undefined state, false if not.
	//Iterators won't stop you from continuing if in undefined state, but it's the client fault for crashes
	//after deleting items from list without resetting their iterator.

	return 0;
}