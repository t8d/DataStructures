#ifndef ARRAY2D_H
#define ARRAY2D_H

#include "Row.h"

/************************************************************************
* Class: Array2D
*
* Purpose: This class creates a dynamic two-dimensional array that's
* values can be accessed by expected double-array notation.
* example: Array2D<int> test(2,4);
*		   test[1][2] = 4;
*
* Manager functions:
* Array2D ( )
* The default size is zero.
*
* Array2D (int row, int col)
* Creates an single dimensional array length = row * col.
*
* Array2D (const Array2D & copy) //copy constructor.
*
* Array2D & operator = (const Array2D & copy) //handles Array2D = Array2D.
*
* ~Array() //Destructor
*
* Methods:
*
* Row<T> operator [ ] (int index) //Returns a very temporary Row object to
* use it's [] operator to find the correct address, that itself returns a T &.
*
* int getRow(). //returns m_row.
*
* void setRow(int rows). //sets m_row, and recreates the m_array to the new size.
*
* getColumn() //returns m_col.
*
* setColumn(int columns) //sets m_col, and recreates the m_array to the new size.
*
* T & Select(int row, int column) //non-operator form of Array[][].
*
*************************************************************************/

template <typename T>
class Array2D
{
private:
	T** m_array;
	int m_row;
	int m_col;
public:
	Array2D();
	Array2D(int row, int col);
	Array2D(const Array2D & copy);
	~Array2D();
	Array2D & operator=(const Array2D & copy);
	Row<T> operator[](int index);
	int getRow() const;
	void setRow(int rows);
	int getColumn() const;
	void setColumn(int columns);
	T & Select(int row, int column);
};

#endif

template<typename T>
inline Array2D<T>::Array2D()
	:m_array(nullptr), m_row(0), m_col(0)
{ }

template<typename T>
inline Array2D<T>::Array2D(int row, int col)
	:m_array(nullptr), m_row(row), m_col(col)
{
	if (row > 0 && col > 0)
	{
		m_array = new T*[row * col]; //creates a list of pointers that large.
		for (int i = 0; i < (row * col); i++)
			m_array[i] = new T;
	}
}

template<typename T>
inline Array2D<T>::Array2D(const Array2D & copy)
	:m_array(nullptr), m_row(0), m_col(0)
{
	*this = copy;
}

template<typename T>
inline Array2D<T>::~Array2D()
{
	for (int i = 0; i < (m_row * m_col); i++)
		delete m_array[i];
	delete [] m_array;
	m_array = nullptr;
}

template<typename T>
inline Array2D<T> & Array2D<T>::operator=(const Array2D & copy)
{
	if (this != &copy)
	{
		//Delete old if it exists.
		if (m_array != nullptr)
		{
			for (int i = 0; i < (m_row * m_col); i++)
			{
				delete m_array[i];
			}
			delete[] m_array;
			m_array = nullptr;
		}
		//m_array = copy.m_array; cannot do this anymore!
		//Lets, instead, delete the old one, create a new one, and copy its stuff into it.
		m_array = new T *[copy.m_row * copy.m_col];
		for (int i = 0; i < (copy.m_row * copy.m_col); i++)
		{
			m_array[i] = new T;
			*m_array[i] = *copy.m_array[i];
		}
		m_row = copy.m_row;
		m_col = copy.m_col;
	}
	return *this;
}

template<typename T>
Row<T> Array2D<T>::operator[](int index)
{
	//safe limits
	return Row<T>(*this, index);
}

template<typename T>
inline int Array2D<T>::getRow() const
{
	return m_row;
}

template<typename T>
inline void Array2D<T>::setRow(int n_row)
{
	int n_col = m_col; //copy for old array.

	if (n_row < 0)
	{
		cout << Exception("Row cannot be set to a negative number, defaulting to 0.") << endl;
		n_row = 0;
		n_col = 0;
	}

	//creating new array, copying old into it.
	T ** newarray = new T *[n_row * n_col];
	for (int i = 0; i < (n_row * n_col); i++)
	{
		newarray[i] = new T;
		if (i < (m_col * m_row))
			*newarray[i] = *m_array[i];
	}

	//deleting old array.
	for (int i = 0; i < (m_row * m_col); i++)
	{
		delete m_array[i];
	}
	delete [] m_array;
	m_array = nullptr;

	//copying pointers/size data over to class data.
	m_array = newarray;
	m_col = n_col;
	m_row = n_row;
}

template<typename T>
inline int Array2D<T>::getColumn() const
{
	return m_col;
}

template<typename T>
inline void Array2D<T>::setColumn(int n_col)
{
	int n_row = m_row; //copy for old array.
	if (n_col < 0)
	{
		cout << Exception("Columns cannot be set to a negative number, defaulting to 0.") << endl;
		n_col = 0;
		n_row = 0;
	}

	//creating new array, copying old into it.
	T ** newarray = new T *[n_row * n_col];
	for (int i = 0; i < (n_row * n_col); i++)
	{
		newarray[i] = new T;
		if (i < (m_col * m_row))
			*newarray[i] = *m_array[i];
	}

	//deleting old array.
	for (int i = 0; i < (m_row * m_col); i++)
	{
		delete m_array[i];
	}
	delete[] m_array;
	m_array = nullptr;

	//copying pointers/size data over to class data.
	m_array = newarray;
	m_col = n_col;
	m_row = n_row;
}

template<typename T>
inline T & Array2D<T>::Select(int row, int column)
{
	return *m_array[(row * m_col) + column];
}
