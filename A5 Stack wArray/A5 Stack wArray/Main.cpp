#include <iostream>
#include <string>
#include "StackA.h"

using std::cout;
using std::endl;
using std::string;

int main()
{
	//Creatings stacks testing both constructors
	cout << "Creating Stacks" << endl << endl;
	StackA<int> iTest;
	StackA<int> iTest2(200);
	StackA<string> sTest;
	StackA<string> sTest2(200);

	//Filling all the stacks.
	cout << "Filling stacks" << endl << endl;
	for (int i = 0; i < 100; i++)
	{
		iTest.Push(i);
		sTest.Push("Test100");
	}

	for (int i = 0; i < 200; i++)
	{
		iTest2.Push(i);
		sTest2.Push("Test200");
	}

	//Testing Peek
	cout << "Peeking last values" << endl << endl;
	cout << '\t' << sTest.Peek() << endl;
	cout << '\t' << sTest2.Peek() << endl;
	cout << endl;

	//Testing Size
	if (sTest2.Size() == 200)
		cout << "Size test passed" << endl << endl;
	else
		cout << "Size test failed, value= " << sTest2.Size()  << endl << endl;

	//Testing Stack OverFlow
	cout << "Did Overflow test pass?" << endl;
	try
	{
		sTest2.Push("Hi");
	}
	catch (Exception msg)
	{
		cout << "\tYes" << endl << endl;
	}

	//Testing popping out all values into cout;
	cout << "Popping out all values" << endl << endl;
	for (int i = 0; i < 100; i++)
	{
		sTest.Pop();
		iTest.Pop();
	}
	for (int i = 0; i < 200; i++)
	{
		sTest2.Pop();
		iTest2.Pop();
	}

	//Testing isEmpty
	if (sTest2.isEmpty() == true)
		cout << "isEmpty test passed" << endl << endl;
	else
		cout << "isEmpty test failed" << endl << endl;
	//Testing Stack Underflow
	cout << "Did Underflow test pass?" << endl;
	try
	{
		sTest2.Pop();
	}
	catch (Exception msg)
	{
		cout << "\tYes" << endl << endl;
	}
	//Testing Copy Constructor
	iTest.Push(1);
	StackA<int> iTest3(iTest);
	if (iTest3.Peek() == 1)
		cout << "Copy Constructor test passed" << endl << endl;
	else
		cout << "Copy Constructor test failed" << endl << endl;
	//Testing OP=
	StackA<int> iTest4;
	iTest4.Push(2);
	iTest3 = iTest4;
	if (iTest3.Peek() == 2)
		cout << "Operator= test passed" << endl << endl;
	else
		cout << "Operator= test failed" << endl << endl;
	return 0;
}