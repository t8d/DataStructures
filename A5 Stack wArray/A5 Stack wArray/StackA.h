#ifndef STACKA_H
#define STACKA_H

#include "Array.h"

template <typename T>
class StackA
{
public:
	StackA();
	StackA(int stackSize);
	StackA(const StackA & copy);
	~StackA();
	StackA & operator=(const StackA & copy);
	void Push(T pushed);
	T Pop();
	T Peek();
	int Size() const;
	bool isEmpty() const;
	bool isFull() const;

private:
	Array<T> m_stack;
	int m_size;
	int m_pointer;
};

template<typename T>
inline StackA<T>::StackA()
	:m_stack(100), m_size(100), m_pointer(0)
{ }

template<typename T>
inline StackA<T>::StackA(int stackSize)
	:m_stack(stackSize), m_size(stackSize), m_pointer(0)
{ }

template<typename T>
inline StackA<T>::StackA(const StackA & copy)
	:m_stack(copy.m_stack), m_size(copy.m_size), m_pointer(copy.m_pointer)
{ }

template<typename T>
inline StackA<T>::~StackA()
{
	m_size = 0;
	m_stack.setLength(0);
	m_pointer = 0;
}

template<typename T>
StackA<T> & StackA<T>::operator=(const StackA & copy)
{
	m_size = copy.m_size;
	m_stack = copy.m_stack;
	m_pointer = copy.m_pointer;
	return *this;
}

template<typename T>
inline void StackA<T>::Push(T pushed)
{
	if (m_pointer < m_size)
		m_stack[m_pointer++] = pushed;
	else
		throw Exception("Stack Overflow Exception");
}

template<typename T>
inline T StackA<T>::Pop()
{
	T * out = nullptr;
	if (m_pointer > 0)
		out = &m_stack[--m_pointer];
	else
		throw Exception("Stack Underflow Exception");
	return *out;
}

template<typename T>
inline T StackA<T>::Peek()
{
	T * out = nullptr;
	if (m_pointer > 0)
		out = &m_stack[m_pointer - 1];
	else
		throw Exception("Stack Underflow Exception");
	return *out;
}

template<typename T>
inline int StackA<T>::Size() const
{
	return m_pointer;
}

template<typename T>
inline bool StackA<T>::isEmpty() const
{
	return m_pointer == 0;
}

template<typename T>
inline bool StackA<T>::isFull() const
{
	return m_pointer == m_size;
}

#endif