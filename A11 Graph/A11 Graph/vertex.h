#ifndef Vertex_H
#define Vertex_H
#include "Edge.h"
using std::list;
template <typename T, typename U>
class Vertex
{
public:
	//public (because xmemory) canonical methods
	Vertex();
	Vertex(T vd);
	Vertex(const Vertex & copy);
	Vertex & operator=(const Vertex & copy);
	~Vertex();

	bool operator==(const Vertex & compare) const;
	bool operator==(const T & compare) const;

	template <typename V, typename W>
	friend class Graph;
private:
	//data members
	T m_vd; //Vertex data.
	list<Edge<T, U>> m_el;
	bool m_proccessed;
};

template <typename T, typename U>
Vertex<T, U>::Vertex()
	:m_proccessed(false)
{ }

template<typename T, typename U>
inline Vertex<T, U>::Vertex(T vd)
	:m_vd(vd), m_proccessed(false)
{ }

template<typename T, typename U>
inline Vertex<T, U>::Vertex(const Vertex & copy)
	:m_vd(copy.m_vd), m_proccessed(copy.m_proccessed)
{ }

template <typename T, typename U>
Vertex<T, U>::~Vertex()
{ }

template<typename T, typename U>
inline Vertex<T, U> & Vertex<T, U>::operator=(const Vertex & copy)
{
	if (this != &copy)
	{
		m_vd = copy.m_vd;
		m_proccessed = copy.m_proccessed;
		//m_el = copy.m_el;
	}
	return *this;
}

template<typename T, typename U>
inline bool Vertex<T, U>::operator==(const Vertex<T, U>& compare) const
{
	return m_vd == compare.m_vd;
}
template<typename T, typename U>
inline bool Vertex<T, U>::operator==(const T & compare) const
{
	return m_vd == compare;
}
#endif
