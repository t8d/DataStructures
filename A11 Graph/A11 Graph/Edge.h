#ifndef EDGE_H
#define EDGE_H

template <typename T, typename U>
class Vertex;

template <typename T, typename U>
class Edge
{
public:
	Edge();
	Edge(Vertex<T, U> * de, U edge, int weight);
	Edge(const Edge & copy);
	Edge & operator=(const Edge & copy);
	~Edge();

	bool operator==(const Edge & compare) const;

	template <typename V, typename W>
	friend class Graph;
private:
	Vertex<T, U> * m_de; //destination edge
	int m_weight; //weight.
	U m_ed; //edge data
};

template<typename T, typename U>
inline Edge<T, U>::Edge()
	:m_de(nullptr), m_weight(0)
{ }

template<typename T, typename U>
inline Edge<T, U>::Edge(Vertex<T, U>* de, U edge, int weight)
	:m_de(de), m_weight(weight), m_ed(edge)
{ }

template<typename T, typename U>
inline Edge<T, U>::Edge(const Edge & copy)
	: m_de(copy.m_de), m_weight(copy.m_weight), m_ed(copy.m_ed)
{ }

template<typename T, typename U>
inline Edge<T, U>& Edge<T, U>::operator=(const Edge<T, U>& copy)
{
	if (this != &copy)
	{
		m_de = copy.m_de;
		m_weight = copy.m_weight;
		m_ed = copy.m_ed;
	}
	return *this;
}

template<typename T, typename U>
inline Edge<T, U>::~Edge()
{ }

template<typename T, typename U>
inline bool Edge<T, U>::operator==(const Edge<T, U>& compare) const
{
	return (m_weight == compare.m_weight && m_de == compare.m_de && m_ed == compare.m_ed);
}

#endif