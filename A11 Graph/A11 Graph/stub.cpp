#include <iostream>
#include "Graph.h"

using std::cout;
using std::endl;

void print(int i);
void print(char i);

int main()
{
	cout << "Creating int,int graph" << endl;
	Graph<int, int> test;
	cout << "Inserting a bunch of vertex's and edges into graph" << endl;
	test.InsertVertex(1);
	test.InsertVertex(2);
	test.InsertVertex(3);
	test.InsertVertex(4);
	test.InsertVertex(5);
	test.InsertEdge(1, 2, 5, 1);
	test.InsertEdge(1, 4, 12, 1);
	test.InsertEdge(1, 5, 30, 1);
	test.InsertEdge(2, 4, 10, 1);
	test.InsertEdge(4, 5, 2, 1);
	test.InsertEdge(5, 3, 70, 1);
	test.InsertEdge(4, 3, 3, 1);
	cout << "Removing 1 vertex and 1 edge" << endl;
	test.RemoveData(5);
	test.RemoveEdge(2, 4);
	cout << "Traversing Depth and Breadth First" << endl;
	test.TraverseDepthFirst(print);
	cout << endl;
	test.TraverseBreadthFirst(print);
	cout << endl;
	cout << "Testing op= and copy constructor (with above's data)" << endl;
	Graph<int, int> test2 = test;
	Graph<int, int> test3;
	test3 = test;
	cout << "BreadthFirst'ing both new graph's" << endl;
	test2.TraverseBreadthFirst(print);
	cout << endl;
	test3.TraverseBreadthFirst(print);
	cout << endl;
	cout << "One last test with char's, int's" << endl;
	Graph<char, int> test4;
	test4.InsertVertex('a');
	test4.InsertVertex('b');
	test4.InsertEdge('a', 'b', 3, 1);
	test4.TraverseBreadthFirst(print);
	cout << endl;
	return 0;
}

void print(int i)
{
	cout << i << ' ';
}

void print (char i)
{
	cout << i << ' ';
}