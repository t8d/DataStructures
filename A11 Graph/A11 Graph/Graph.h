#ifndef GRAPH_H
#define GRAPH_H
#include <list>
#include "vertex.h"
#include <stack>
#include <queue>

using std::list;
using std::queue;
using std::stack;

template <typename T, typename U>
class Graph
{
public:
	//Canonical
	Graph();
	Graph(const Graph & copy);
	Graph & operator=(const Graph & copy);
	~Graph();

	//Manager
	void InsertVertex(const T & vData);
	void RemoveData(const T & vData);
	void InsertEdge(const T & startVx, const T & destVx, const U & ed, int weight);
	void RemoveEdge(const T & startVx, const T & destVx);
	bool IsEmpty() const;

	//Traversals
	void TraverseDepthFirst(void(*visit)(T vData));
	void TraverseBreadthFirst(void(*visit)(T vData));
private:
	list<Vertex<T, U>> m_vl; //vertex list.
	int m_vCount;
};

template<typename T, typename U>
inline Graph<T, U>::Graph()
	:m_vCount(0)
{ }

template<typename T, typename U>
inline Graph<T, U>::Graph(const Graph & copy)
	:m_vCount(0)
{
	*this = copy; //passing work to the op=.
}

template<typename T, typename U>
inline Graph<T, U> & Graph<T, U>::operator=(const Graph & copy)
{
	if (this != &copy)
	{
		//list<Vertex<T, U>> CopyVL = copy.m_vl;
		m_vl = copy.m_vl;
		m_vCount = copy.m_vCount;
		//go through every vertex in copy's vl, replicating each one's edges.

		for(const Vertex<T, U> & vertex : copy.m_vl)
		{
			for(const Edge<T,U> & edge : vertex.m_el)
			{
				InsertEdge(vertex.m_vd, edge.m_de->m_vd, edge.m_ed, edge.m_weight);
			}
		}

		/*for (typename list<Vertex<T, U>>::iterator iter = copy.m_vl.begin(); iter != copy.m_vl.end(); ++iter)
		{
			for (typename list<Edge<T, U>>::iterator iterE = iter->m_el.begin(); iterE != iter->m_el.end(); ++iterE)
			{
				InsertEdge(iter->m_vd, iterE->m_de->m_vd, iterE->m_ed, iterE->m_weight);
			}
		}*/
	}
	return *this;
}

template<typename T, typename U>
inline Graph<T, U>::~Graph()
{ }

template<typename T, typename U>
inline void Graph<T, U>::InsertVertex(const T & vData)
{
	m_vl.push_back(Vertex<T, U>(vData));
	m_vCount++;
}

template<typename T, typename U>
inline void Graph<T, U>::RemoveData(const T & vData)
{
	bool exit = false;
	//Find vData in list.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin();!exit && iter != m_vl.end();)
	{
		if ((*iter) == vData)
		{
			//Remove any edge pointing to vData.
			for (typename list<Vertex<T, U>>::iterator iter2 = m_vl.begin(); iter2 != m_vl.end(); ++iter2)
			{ //iterate through list to iterate through their edges.
				bool deleted = false;
				for (typename list<Edge<T, U>>::iterator iterE = (*iter2).m_el.begin(); !deleted && iterE != (*iter2).m_el.end();)
				{ 
					//find edges pointing to vData.
					if (*(*iterE).m_de == vData)
					{
						(*iter2).m_el.erase(iterE);
						deleted = true;
					}
					else
					{
						++iterE;
					}
				}
			}
			//now delete that vertex.
			m_vl.erase(iter);
			m_vCount--;
			exit = true;
		}
		else
		{
			++iter;
		}
	}
	
	
}

template<typename T, typename U>
inline void Graph<T, U>::InsertEdge(const T & startVx, const T & destVx, const U & ed, int weight)
{
	//I can probably combine a few of these iterators together for efficiency.
	if (startVx != destVx)
	{ 
		bool foundS = false; //found start
		bool foundD = false; //found destination
		bool connected = false; //is connected.
		for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); iter != m_vl.end(); ++iter)
		{ //make sure both vertex's exist.
			if (*iter == startVx)
				foundS = true;
			if (*iter == destVx)
				foundD = true;
		}
		if (foundS && foundD)
		{
			for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); iter != m_vl.end(); ++iter)
			{ //Iterate to find start vertex.
				if (*iter == startVx) //find start vertex.
				{
					for (typename list<Edge<T, U>>::iterator iterE = (*iter).m_el.begin(); iterE != (*iter).m_el.end(); ++iterE)
						if ((*iterE).m_de->m_vd == destVx && (*iterE).m_ed == ed) //determine if it's already connected.
							connected = true;
					for (typename list<Vertex<T, U>>::iterator iter2 = m_vl.begin(); iter2 != m_vl.end() && !connected; ++iter2)
					{  //iterator for destination vertex.
						if ((*iter2) == destVx) //find dest vertex.
						{
							//push edges to both vertexs to eachother.
							(*iter).m_el.push_back(Edge<T, U>(&*iter2, ed, weight));
							(*iter2).m_el.push_back(Edge<T, U>(&*iter, ed, weight));
						}
					}
				}
			}
		}
	}
}

template<typename T, typename U>
inline void Graph<T, U>::RemoveEdge(const T & startVx, const T & destVx)
{
	//iterate to find startvx.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); iter != m_vl.end(); ++iter)
	{
		if ((*iter) == startVx)
		{
			//iterate through Edge
			bool deleted = false;
			for (typename list<Edge<T, U>>::iterator iterE = (*iter).m_el.begin(); !deleted && iterE != (*iter).m_el.end();)
			{
				//find edges pointing to vData.
				if (*(*iterE).m_de == destVx)
				{
					//before erase, remove inside edge backwards.
					bool deleted2 = false;
					for (typename list<Edge<T, U>>::iterator iterE2 = (*iterE).m_de->m_el.begin(); !deleted2 && iterE2 != (*iterE).m_de->m_el.end();)
					{
						if (*(*iterE2).m_de == startVx)
						{
							(*iterE).m_de->m_el.erase(iterE2);
							deleted2 = true;
						}
						else
						{
							++iterE2;
						}
					}
					(*iter).m_el.erase(iterE);
					deleted = true;
				}
				else
				{
					++iterE;
				}
			}
		}
	}
}

template<typename T, typename U>
inline bool Graph<T, U>::IsEmpty() const
{
	return m_vCount <= 0;
}

template<typename T, typename U>
inline void Graph<T, U>::TraverseDepthFirst(void(*visit)(T vData))
{
	stack<Vertex<T, U> *> vStack;
	Vertex<T, U> * tempvertex = nullptr;
	vStack.push(&m_vl.front());
	while (!vStack.empty())
	{
		tempvertex = vStack.top();
		vStack.pop();
		tempvertex->m_proccessed = true;
		visit(tempvertex->m_vd);
		//push edges to stack.
		for (typename list<Edge<T, U>>::iterator iterE = tempvertex->m_el.begin(); iterE != tempvertex->m_el.end(); ++iterE)
		{
			if ((*iterE).m_de->m_proccessed == false)
				vStack.push((*iterE).m_de);
		}
	}
	//reset proccessed flags to false.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); m_vl.end() != iter; ++iter)
	{
		(*iter).m_proccessed = false;
	}
}

template<typename T, typename U>
inline void Graph<T, U>::TraverseBreadthFirst(void(*visit)(T vData))
{
	queue<Vertex<T, U> *> vQueue;
	Vertex<T, U> * tempvertex = nullptr;
	vQueue.push(&m_vl.front());
	while (!vQueue.empty())
	{
		tempvertex = vQueue.front();
		vQueue.pop();
		tempvertex->m_proccessed = true;
		visit(tempvertex->m_vd);
		//push edges to stack.
		for (typename list<Edge<T, U>>::iterator iterE = tempvertex->m_el.begin(); iterE != tempvertex->m_el.end(); ++iterE)
		{
			if ((*iterE).m_de->m_proccessed == false)
				vQueue.emplace(iterE->m_de);
		}
	}
	//reset proccessed flags to false.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); m_vl.end() != iter; ++iter)
	{
		(*iter).m_proccessed = false;
	}
}

#endif
