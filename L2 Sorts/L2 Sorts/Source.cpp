/*************************************************************
*
* Lab: Lab 2 � Sorts
*
* Overview:
* This program take one argument (int), create that many
* elements in Array, int[], and vector. The program will pass
* array's to every function to have them sorted, while timing
* how long it takes, and reset it back to be sorted again.
*
* Input:
* command line argument - one integer.
* Ex) L2 Sorts.exe 2000
*
* Output:
* The time it takes to sort n data with my Array class, carray,
* and vector's, for each of the following sorts:
*
* BubbleSort
* FlagBubbleSort
* InsertionSort
* SelectionSort
* ShellSort
* HeapSort
* MergeSort
* QuickSort
*
************************************************************/

//Includes
#include <iostream>
#include <iomanip>
#include <time.h>
#include <vector>
#include <chrono>
#include <ios>
#include <fstream>
#include "Array.h"

#define _CRTDBG_MAP_ALLOC

//Using
using std::vector;
using std::chrono::system_clock;
using std::chrono::duration_cast;
using std::chrono::nanoseconds;
using std::ios;
using std::ofstream;
using std::cout;
using std::endl;

//Function declarations
template<typename T>
void BubbleSort(Array<T>& data);
void BubbleSort(int n, int *data);
template<typename T>
void BubbleSort(int n, vector<T> &data);

template<typename T>
void FlagBubbleSort(Array<T>& data);
void FlagBubbleSort(int n, int *data);
template<typename T>
void FlagBubbleSort(int n, vector<T>& data);

template<typename T>
void SelectionSort(Array<T>& data);
void SelectionSort(int n, int *data);
template<typename T>
void SelectionSort(int n, vector<T>& data);

template<typename T>
void InsertionSort(Array<T>& data);
void InsertionSort(int n, int *data);
template<typename T>
void InsertionSort(int n, vector<T>& data);

template<typename T>
void ShellSort(Array<T>& data);
void ShellSort(int n, int *data);
template<typename T>
void ShellSort(int n, vector<T>& data);

template<typename T>
void HeapSort(Array<T>& data);
template<typename T>
void HeapSortMoveDown(Array<T>& data, int firstindex, int lastindex);
void HeapSort(int n, int *data);
void HeapSortMoveDown(int *data, int firstindex, int lastindex);
template<typename T>
void HeapSort(int n, vector<T>& data);
template<typename T>
void HeapSortMoveDown(vector<T>& data, int firstindex, int lastindex);

//Merge with Array<T>
template <typename T>
void MergeSort(Array<T>& data);
template <typename T>
void MergeSort(Array<T>& data, int left, int right);
template <typename T>
void Merge(Array<T>& data, int low, int mid, int high);
//Merge with int*
void MergeSort(int* data, int n);
void MergeSort(int* data, int left, int right);
void Merge(int* data, int low, int mid, int high);
//Merge with vector<T>
template <typename T>
void MergeSort(vector<T>& data, int n);
template <typename T>
void MergeSort(vector<T>& data, int left, int right);
template <typename T>
void Merge(vector<T>& data, int low, int mid, int high);

template<typename T>
void QuickSort(Array<T>& data);
template<typename T>
void QuickSort(Array<T>& data, int leftIndex, int rightIndex);
void QuickSort(int n, int *data);
void QuickSort(int * data, int leftIndex, int rightIndex);
template<typename T>
void QuickSort(int n, vector<T>& data);
template<typename T>
void QuickSort(vector<T>& data, int leftIndex, int rightIndex);

void CreateRandom(int toCreate, Array<int> & data);
template <typename T>
void IsSorted(int n, T & original_array, T & copy_array);

/**********************************************************************
* Purpose: Create array's, Call Sorts, and output data.
*
* Precondition:
*	   None.
*
* Postcondition:
*      None.
*
************************************************************************/
int main(int argc, char *argv[])
{

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);

	ofstream out("out.txt");
	if (out.is_open())
	{
		int toCreate = 1000; //defaults to 100
		if (argc == 2) //one argument
		{
			toCreate = atoi(argv[1]); //int toCreate.
			out << "Testing Sorts with " << toCreate << " elements." << endl;
		}
		else
			out << "No argument defined, defaulting to 1000" << endl;

		//Creating array with random values.
		Array<int> original_array;
		CreateRandom(toCreate, original_array);

		//Creating CArray with same values as original_array.
		int * original_carray = new int[toCreate];
		for (int i = 0; i < toCreate; i++)
		{
			original_carray[i] = original_array[i];
		}

		//Creating a vector with same values as original_array.
		vector<int> original_vector(toCreate);
		for (int i = 0; i < toCreate; i++)
		{
			original_vector[i] = original_array[i];
		}

		//Create copy arrays

		Array<int> copy_array(original_array);

		//for comparing correctly sorted sorts.
		Array<int> correctly_sorted_array(original_array);
		BubbleSort(correctly_sorted_array); //I know bubble sort works.
		int * correctly_sorted_carray = new int[toCreate];
		for (int i = 0; i < toCreate; i++)
		{
			correctly_sorted_carray[i] = original_carray[i];
		}
		BubbleSort(toCreate, correctly_sorted_carray);
		vector<int> correctly_sorted_vector(original_vector);
		BubbleSort(toCreate, correctly_sorted_vector);

		//used to pass into sort functions
		vector<int> copy_vector(original_vector);
		int * copy_carray = new int[toCreate];
		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		//Start with sorts.
		out.setf(ios::fixed);

		out << "************************************Bubble Sort*********************************" << endl;
		//Array
		system_clock::time_point start = system_clock::now();
		BubbleSort(copy_array);
		int timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_array, copy_array);

		copy_array = original_array;

		out << "Time taken for Array:  " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//CArray
		start = system_clock::now();
		BubbleSort(toCreate, copy_carray);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_carray, copy_carray);

		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		out << "Time taken for CArray: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//Vector
		start = system_clock::now();
		BubbleSort(toCreate, copy_vector);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_vector, copy_vector);

		copy_vector = original_vector;

		out << "Time taken for Vector: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		out << "************************************Flag Bubble Sort****************************" << endl;
		//Array
		start = system_clock::now();
		FlagBubbleSort(copy_array);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_array, copy_array);

		copy_array = original_array;

		out << "Time taken for Array:  " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//CArray
		start = system_clock::now();
		FlagBubbleSort(toCreate, copy_carray);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_carray, copy_carray);

		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		out << "Time taken for CArray: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//Vector
		start = system_clock::now();
		FlagBubbleSort(toCreate, copy_vector);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_vector, copy_vector);

		copy_vector = original_vector;

		out << "Time taken for Vector: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		out << "************************************Insertion Sort******************************" << endl;
		//Array
		start = system_clock::now();
		InsertionSort(copy_array);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_array, copy_array);

		copy_array = original_array;

		out << "Time taken for Array:  " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//CArray
		start = system_clock::now();
		InsertionSort(toCreate, copy_carray);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_carray, copy_carray);

		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		out << "Time taken for CArray: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//Vector
		start = system_clock::now();
		InsertionSort(toCreate, copy_vector);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_vector, copy_vector);

		copy_vector = original_vector;

		out << "Time taken for Vector: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		out << "************************************Selection Sort******************************" << endl;
		//Array
		start = system_clock::now();
		SelectionSort(copy_array);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_array, copy_array);

		copy_array = original_array;

		out << "Time taken for Array:  " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//CArray
		start = system_clock::now();
		SelectionSort(toCreate, copy_carray);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_carray, copy_carray);

		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		out << "Time taken for CArray: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//Vector
		start = system_clock::now();
		SelectionSort(toCreate, copy_vector);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_vector, copy_vector);

		copy_vector = original_vector;

		out << "Time taken for Vector: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		out << "************************************Shell Sort**********************************" << endl;
		//Array
		start = system_clock::now();
		ShellSort(copy_array);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_array, copy_array);

		copy_array = original_array;

		out << "Time taken for Array:  " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//CArray
		start = system_clock::now();
		ShellSort(toCreate, copy_carray);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_carray, copy_carray);

		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		out << "Time taken for CArray: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//Vector
		start = system_clock::now();
		ShellSort(toCreate, copy_vector);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_vector, copy_vector);

		copy_vector = original_vector;

		out << "Time taken for Vector: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		out << "************************************Heap Sort***********************************" << endl;
		//Array
		start = system_clock::now();
		HeapSort(copy_array);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_array, copy_array);

		copy_array = original_array;

		out << "Time taken for Array:  " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//CArray
		start = system_clock::now();
		HeapSort(toCreate, copy_carray);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_carray, copy_carray);

		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		out << "Time taken for CArray: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//Vector
		start = system_clock::now();
		HeapSort(toCreate, copy_vector);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_vector, copy_vector);

		copy_vector = original_vector;

		out << "Time taken for Vector: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		out << "************************************Merge Sort**********************************" << endl;
		//Array
		start = system_clock::now();
		MergeSort(copy_array);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_array, copy_array);

		copy_array = original_array;

		out << "Time taken for Array:  " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//CArray
		start = system_clock::now();
		MergeSort(copy_carray, toCreate);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_carray, copy_carray);

		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		out << "Time taken for CArray: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//Vector
		start = system_clock::now();
		MergeSort(copy_vector, toCreate);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_vector, copy_vector);

		copy_vector = original_vector;

		out << "Time taken for Vector: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		out << "************************************Quick Sort**********************************" << endl;
		//Array
		start = system_clock::now();
		QuickSort(copy_array);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_array, copy_array);

		copy_array = original_array;

		out << "Time taken for Array:  " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//CArray
		start = system_clock::now();
		QuickSort(toCreate, copy_carray);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_carray, copy_carray);

		for (int i = 0; i < toCreate; i++)
		{
			copy_carray[i] = original_carray[i];
		}

		out << "Time taken for CArray: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		//Vector
		start = system_clock::now();
		QuickSort(toCreate, copy_vector);
		timetaken = duration_cast<nanoseconds>(system_clock::now() - start).count();

		IsSorted(toCreate, correctly_sorted_vector, copy_vector);

		copy_vector = original_vector;

		out << "Time taken for Vector: " << std::setprecision(1) << timetaken / 1000.0 << " us." << endl;

		delete[] original_carray;
		delete[] correctly_sorted_carray;
		delete[] copy_carray;
	}
	return 0;
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void BubbleSort(Array<T>& data)
{
	int n = data.getLength(); //n times
	for (int i = 0; i < n; i++)
	{
		for (int y = 0; y < (n - i - 1); y++)
		{
			if (data[y] > data[y + 1])
			{
				//swap
				T temp = data[y];
				data[y] = data[y + 1];
				data[y + 1] = temp;
			}
		}
	}

}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void BubbleSort(int n, int * data)
{
	for (int i = 0; i < n; i++)
	{
		for (int y = 0; y < (n - i - 1); y++)
		{
			if (data[y] > data[y + 1])
			{
				//swap
				int temp = data[y];
				data[y] = data[y + 1];
				data[y + 1] = temp;
			}
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void BubbleSort(int n, vector<T>& data)
{
	for (int i = 0; i < n; i++)
	{
		for (int y = 0; y < (n - i - 1); y++)
		{
			if (data[y] > data[y + 1])
			{
				//swap
				T temp = data[y];
				data[y] = data[y + 1];
				data[y + 1] = temp;
			}
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void FlagBubbleSort(Array<T>& data)
{
	int n = data.getLength(); //n times
	bool sorted = false;
	for (int i = 0; (i < n) &&(sorted == false); i++)
	{
		sorted = true;
		for (int y = 0; y < (n - i - 1); y++)
		{
			if (data[y] > data[y + 1])
			{
				sorted = false;
				//swap
				T temp = data[y];
				data[y] = data[y + 1];
				data[y + 1] = temp;
			}
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void FlagBubbleSort(int n, int * data)
{
	bool sorted = false;
	for (int i = 0; (i < n) && (sorted == false); i++)
	{
		sorted = true;
		for (int y = 0; y < (n - i - 1); y++)
		{
			if (data[y] > data[y + 1])
			{
				sorted = false;
				//swap
				int temp = data[y];
				data[y] = data[y + 1];
				data[y + 1] = temp;
			}
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void FlagBubbleSort(int n, vector<T>& data)
{
	bool sorted = false;
	for (int i = 0; (i < n) && (sorted == false); i++)
	{
		sorted = true;
		for (int y = 0; y < (n - i - 1); y++)
		{
			if (data[y] > data[y + 1])
			{
				sorted = false;
				//swap
				T temp = data[y];
				data[y] = data[y + 1];
				data[y + 1] = temp;
			}
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void SelectionSort(Array<T>& data)
{
	int n = data.getLength(); //n times
	int i = 0, j = 0, min = 0;
	T tmp;
	for (j = 0; j < (n - 1); j++)
	{
		min = j;

		for (i = j + 1; i < n; i++)
		{
			if (data[i] < data[min])
			{
				min = i;
			}
		}
		if (min != j)
		{
			tmp = data[j];
			data[j] = data[min];
			data[min] = tmp;
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void SelectionSort(int n, int * data)
{
	int i = 0, j = 0;
	for (j = 0; j < n - 1; j++)
	{
		int min = j;

		for (i = j + 1; i < n; i++)
		{
			if (data[i] < data[min])
			{
				min = i;
			}
		}
		if (min != j)
		{
			int tmp = data[j];
			data[j] = data[min];
			data[min] = tmp;
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void SelectionSort(int n, vector<T>& data)
{
	int i = 0, j = 0;
	for (j = 0; j < n - 1; j++)
	{
		int min = j;

		for (i = j + 1; i < n; i++)
		{
			if (data[i] < data[min])
			{
				min = i;
			}
		}
		if (min != j)
		{
			T tmp = data[j];
			data[j] = data[min];
			data[min] = tmp;
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void InsertionSort(Array<T>& data)
{
	int n = data.getLength(); //n times
	for (int i = 1; i < n; i++)
	{
		T temp = data[i];
		int j = 0;
		for (j = i; (j > 0) && (temp < data[j-1]); j--)
		{
			data[j] = data[j - 1];
		}
		data[j] = temp;
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void InsertionSort(int n, int * data)
{
	for (int i = 1; i < n; i++)
	{
		int temp = data[i];
		int j = 0;
		for (j = i; (j > 0) && (temp < data[j - 1]); j--)
		{
			data[j] = data[j - 1];
		}
		data[j] = temp;
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void InsertionSort(int n, vector<T>& data)
{
	for (int i = 1; i < n; i++)
	{
		T temp = data[i];
		int j = 0;
		for (j = i; (j > 0) && (temp < data[j - 1]); j--)
		{
			data[j] = data[j - 1];
		}
		data[j] = temp;
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void ShellSort(Array<T>& data)
{
	int n = data.getLength(); //n times
	Array<int> incriments;
	for (int h = 1, i = 0; h < n; i++)
	{
		incriments.setLength(incriments.getLength() + 1);
		incriments[i] = h;
		h = 3 * h + 1;
	}
	int numOfIncriments = incriments.getLength();
	int h = 0;
	for (int i = 0; i < numOfIncriments; i++)
	{
		h = incriments[i];
		//Loop on the number of subarrays for this pass
		for (int hCnt = h; hCnt < (2 * h); hCnt++)
		{
			//Do an insertion sort
			for (int j = hCnt; j < n; )
			{
				T tmp = data[j];
				int k = j;
				while (k - h >= 0 && tmp < data[(k-h)])
				{
					data[k] = data[k - h];
					k = k - h;
				}
				data[k] = tmp;
				j = j + h;
			}
		}
	}

}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void ShellSort(int n, int * data)
{
	Array<int> incriments;
	for (int h = 1, i = 0; h < n; i++)
	{
		incriments.setLength(incriments.getLength() + 1);
		incriments[i] = h;
		h = 3 * h + 1;
	}
	int numOfIncriments = incriments.getLength();
	int h = 0;
	for (int i = 0; i < numOfIncriments; i++)
	{
		h = incriments[i];
		//Loop on the number of subarrays for this pass
		for (int hCnt = h; hCnt < (2 * h); hCnt++)
		{
			//Do an insertion sort
			for (int j = hCnt; j < n; )
			{
				int tmp = data[j];
				int k = j;
				while (k - h >= 0 && tmp < data[(k - h)])
				{
					data[k] = data[k - h];
					k = k - h;
				}
				data[k] = tmp;
				j = j + h;
			}
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void ShellSort(int n, vector<T>& data)
{
	Array<int> incriments;
	for (int h = 1, i = 0; h < n; i++)
	{
		incriments.setLength(incriments.getLength() + 1);
		incriments[i] = h;
		h = 3 * h + 1;
	}
	int numOfIncriments = incriments.getLength();
	int h = 0;
	for (int i = 0; i < numOfIncriments; i++)
	{
		h = incriments[i];
		//Loop on the number of subarrays for this pass
		for (int hCnt = h; hCnt < (2 * h); hCnt++)
		{
			//Do an insertion sort
			for (int j = hCnt; j < n; )
			{
				T tmp = data[j];
				int k = j;
				while (k - h >= 0 && tmp < data[(k - h)])
				{
					data[k] = data[k - h];
					k = k - h;
				}
				data[k] = tmp;
				j = j + h;
			}
		}
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void HeapSort(Array<T>& data)
{
	int n = data.getLength(); //n times

	for (int k = (n / 2); k >= 0; k--)
		HeapSortMoveDown(data, k, n - 1);

	for (int i = n - 1; i != 0; --i)
	{
		//swap data[i] and data[0]
		T tmp = data[i];
		data[i] = data[0];
		data[0] = tmp;

		HeapSortMoveDown(data, 0, i - 1);
	}

}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void HeapSortMoveDown(Array<T>& data, int firstindex, int lastindex)
{
	int largest = firstindex * 2 + 1;
	while (largest <= lastindex)
	{
		if (largest < lastindex && data[largest] < data[largest + 1])
			largest++;
		if (data[firstindex] < data[largest])
		{
			//swap data[child] and data[firstindex]
			T tmp = data[largest];
			data[largest] = data[firstindex];
			data[firstindex] = tmp;
		}
		firstindex = largest;
		largest = 2 * firstindex + 1;
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void HeapSort(int n, int * data)
{

	for (int k = (n / 2); k >= 0; k--)
		HeapSortMoveDown(data, k, n - 1);

	for (int i = n - 1; i != 0; --i)
	{
		//swap data[i] and data[0]
		int tmp = data[i];
		data[i] = data[0];
		data[0] = tmp;

		HeapSortMoveDown(data, 0, i - 1);
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void HeapSortMoveDown(int * data, int firstindex, int lastindex)
{
	int largest = firstindex * 2 + 1;
	while (largest <= lastindex)
	{
		if (largest < lastindex && data[largest] < data[largest + 1])
			largest++;
		if (data[firstindex] < data[largest])
		{
			//swap data[child] and data[firstindex]
			int tmp = data[largest];
			data[largest] = data[firstindex];
			data[firstindex] = tmp;
		}
		firstindex = largest;
		largest = 2 * firstindex + 1;
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void HeapSort(int n, vector<T>& data)
{

	for (int k = (n / 2); k >= 0; k--)
		HeapSortMoveDown(data, k, n - 1);

	for (int i = n - 1; i != 0; --i)
	{
		//swap data[i] and data[0]
		T tmp = data[i];
		data[i] = data[0];
		data[0] = tmp;

		HeapSortMoveDown(data, 0, i - 1);
	}

}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void HeapSortMoveDown(vector<T>& data, int firstindex, int lastindex)
{
	int largest = firstindex * 2 + 1;
	while (largest <= lastindex)
	{
		if (largest < lastindex && data[largest] < data[largest + 1])
			largest++;
		if (data[firstindex] < data[largest])
		{
			//swap data[child] and data[firstindex]
			T tmp = data[largest];
			data[largest] = data[firstindex];
			data[firstindex] = tmp;
		}
		firstindex = largest;
		largest = 2 * firstindex + 1;
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void MergeSort(Array<T>& data)
{
	int n = data.getLength();
	MergeSort(data, 0, (n - 1));
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void MergeSort(Array<T>& data, int left, int right)
{ //right == n, this is recursive and splits the array until it cannot be split no more.
	if (left < right)
	{
		int mid = (left + right) / 2;

		MergeSort(data, left, mid); //left array
		MergeSort(data, mid + 1, right); //right array
		Merge(data, left, mid, right); //Merges every one after splits.
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void Merge(Array<T>& data, int low, int mid, int high)
{
	Array<T> temp(high-low + 1);

	int left = low;
	int right = mid + 1;
	int cur = 0;
	// Merges both arrays into temp array.
	while (left <= mid && right <= high)
	{
		if (data[left] <= data[right])
			temp[cur++] = data[left++];
		else
			temp[cur++] = data[right++];
	}

	if (left > mid)
		for (int i = right; i <= high; i++)
			temp[cur++] = data[i];
	else
		for (int i = left; i <= mid; i++)
			temp[cur++] = data[i];

	for (int i = 0; i <= high - low; i++)
		data[i + low] = temp[i];
	

}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void MergeSort(vector<T>& data, int n)
{
	MergeSort(data, 0, (n - 1));
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void MergeSort(vector<T>& data, int left, int right)
{
	if (left < right)
	{
		int mid = (left + right) / 2;

		MergeSort(data, left, mid); //left array
		MergeSort(data, mid + 1, right); //right array
		Merge(data, left, mid, right); //Merges every one after splits.
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void Merge(vector<T>& data, int low, int mid, int high)
{
	vector<T> temp(high - low + 1);

	int left = low;
	int right = mid + 1;
	int cur = 0;
	// Merges both arrays into temp array.
	while (left <= mid && right <= high)
	{
		if (data[left] <= data[right])
			temp[cur++] = data[left++];
		else
			temp[cur++] = data[right++];
	}

	if (left > mid)
		for (int i = right; i <= high; i++)
			temp[cur++] = data[i];
	else
		for (int i = left; i <= mid; i++)
			temp[cur++] = data[i];

	for (int i = 0; i <= high - low; i++)
		data[i + low] = temp[i];


}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void MergeSort(int * data, int n)
{
	MergeSort(data, 0, (n - 1));
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void MergeSort(int * data, int left, int right)
{
	if (left < right)
	{
		int mid = (left + right) / 2;

		MergeSort(data, left, mid); //left array
		MergeSort(data, mid + 1, right); //right array
		Merge(data, left, mid, right); //Merges every one after splits.
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void Merge(int * data, int low, int mid, int high)
{
	int * temp = new int[high - low + 1];

	int left = low;
	int right = mid + 1;
	int cur = 0;
	// Merges both arrays into temp array.
	while (left <= mid && right <= high)
	{
		if (data[left] <= data[right])
			temp[cur++] = data[left++];
		else
			temp[cur++] = data[right++];
	}

	if (left > mid)
		for (int i = right; i <= high; i++)
			temp[cur++] = data[i];
	else
		for (int i = left; i <= mid; i++)
			temp[cur++] = data[i];

	for (int i = 0; i <= high - low; i++)
		data[i + low] = temp[i];


	delete[] temp;
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void QuickSort(Array<T>& data)
{
	int n = data.getLength();
	QuickSort(data, 0, n - 1);
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void QuickSort(Array<T>& data, int leftIndex, int rightIndex)
{
	int left = 0,
		right = 0,
		pivot = 0;

	if (leftIndex < rightIndex)
	{
		left = leftIndex;
		right = rightIndex;
		pivot = data[(leftIndex + rightIndex) / 2];

		while (left <= right)
		{
			while (data[left] < pivot)
				++left;
			while (data[right] > pivot)
				--right;
			if (left <= right)
			{
				//swap data[left] and data[right]
				T tmp = data[left];
				data[left] = data[right];
				data[right] = tmp;
				++left;
				--right;
			}
		}
		QuickSort(data, leftIndex, right);
		QuickSort(data, left, rightIndex);
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void QuickSort(int n, int * data)
{
	QuickSort(data, 0, n - 1);
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
void QuickSort(int * data, int leftIndex, int rightIndex)
{
	int left = 0,
		right = 0,
		pivot = 0;

	if (leftIndex < rightIndex)
	{
		left = leftIndex;
		right = rightIndex;
		pivot = data[(leftIndex + rightIndex) / 2];

		while (left <= right)
		{
			while (data[left] < pivot)
				++left;
			while (data[right] > pivot)
				--right;
			if (left <= right)
			{
				//swap data[left] and data[right]
				int tmp = data[left];
				data[left] = data[right];
				data[right] = tmp;
				++left;
				--right;
			}
		}
		QuickSort(data, leftIndex, right);
		QuickSort(data, left, rightIndex);
	}
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void QuickSort(int n, vector<T>& data)
{
	QuickSort(data, 0, n - 1);
}

/**********************************************************************
* Purpose: Sort incoming data.
*
* Precondition:
*	   Passed in correct array
*
* Postcondition:
*      Array is sorted.
*
************************************************************************/
template<typename T>
void QuickSort(vector<T>& data, int leftIndex, int rightIndex)
{
	int left = 0,
		right = 0,
		pivot = 0;

	if (leftIndex < rightIndex)
	{
		left = leftIndex;
		right = rightIndex;
		pivot = data[(leftIndex + rightIndex) / 2];

		while (left <= right)
		{
			while (data[left] < pivot)
				++left;
			while (data[right] > pivot)
				--right;
			if (left <= right)
			{
				//swap data[left] and data[right]
				T tmp = data[left];
				data[left] = data[right];
				data[right] = tmp;
				++left;
				--right;
			}
		}
		QuickSort(data, leftIndex, right);
		QuickSort(data, left, rightIndex);
	}
}

/**********************************************************************
* Purpose: Fill array with randomized data.
*
* Precondition:
*	   Valid incoming data members.
*
* Postcondition:
*      Array is filled (at toCreate size), with random data.
*
************************************************************************/
void CreateRandom(int toCreate, Array<int> & data)
{
	data.setLength(toCreate);
	srand(time(nullptr));
	for (int i = 0; i < toCreate; i++)
	{
		data[i] = rand() % 2147483647; //largest signed int value
	}

}

/**********************************************************************
* Purpose: Determine if array is sorted.
*
* Precondition:
*	   Passed in n, original_array, and copy_array.
*
* Postcondition:
*      If correctly sorted, nothing. If not, displays the error to 
*	   console.
*
************************************************************************/
template <typename T>
void IsSorted(int n, T& original_array, T& copy_array)
{
	bool good = true;
	for (int i = 0; i < n; i++)
	{
		if (copy_array[i] != original_array[i])
			good = false;
	}

	if (good == false)
	{
		cout << "[WARNING] Sort failed! Dumping array's: " << endl << endl;
		cout << "Correctly sorted: " << endl;
		for (int i = 0; i < n; i++)
		{
			cout << original_array[i] << '\t';
		}

		cout << endl;
		cout << "Resulted sort: " << endl;
		for (int i = 0; i < n; i++)
		{
			cout << copy_array[i] << '\t';
		}
		cout << endl << endl;
	}
}

