No argument defined, defaulting to 1000
************************************Bubble Sort*********************************
Time taken for Array:  6081.7 us.
Time taken for CArray: 777.5 us.
Time taken for Vector: 831.8 us.
************************************Flag Bubble Sort****************************
Time taken for Array:  6320.3 us.
Time taken for CArray: 749.3 us.
Time taken for Vector: 1558.8 us.
************************************Insertion Sort******************************
Time taken for Array:  1708.9 us.
Time taken for CArray: 146.7 us.
Time taken for Vector: 169.7 us.
************************************Selection Sort******************************
Time taken for Array:  2718.7 us.
Time taken for CArray: 396.0 us.
Time taken for Vector: 377.2 us.
************************************Shell Sort**********************************
Time taken for Array:  1985.6 us.
Time taken for CArray: 282.7 us.
Time taken for Vector: 249.7 us.
************************************Heap Sort***********************************
Time taken for Array:  175.8 us.
Time taken for CArray: 52.2 us.
Time taken for Vector: 62.8 us.
************************************Merge Sort**********************************
Time taken for Array:  303.7 us.
Time taken for CArray: 139.0 us.
Time taken for Vector: 174.9 us.
************************************Quick Sort**********************************
Time taken for Array:  115.0 us.
Time taken for CArray: 49.2 us.
Time taken for Vector: 54.7 us.
