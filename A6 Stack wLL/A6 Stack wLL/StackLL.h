#ifndef StackLL_H
#define StackLL_H

#include "doublelinkedlist.h"
#include "Exception.h"

template <typename T>
class StackLL
{
public:
	StackLL();
	StackLL(const StackLL & copy);
	~StackLL();
	StackLL & operator=(const StackLL & copy);
	void Push(T pushed);
	T Pop();
	T Peek();
	int Size() const;
	bool isEmpty() const;

private:
	DoubleLinkedList<T> m_stack;
	int m_pointer;
};

template<typename T>
inline StackLL<T>::StackLL()
	:m_pointer(0)
{ }

template<typename T>
inline StackLL<T>::StackLL(const StackLL & copy)
	:m_stack(copy.m_stack), m_pointer(copy.m_pointer)
{ }

template<typename T>
inline StackLL<T>::~StackLL()
{
	m_stack.Purge();
	m_pointer = 0;
}

template<typename T>
StackLL<T> & StackLL<T>::operator=(const StackLL & copy)
{
	m_stack = copy.m_stack;
	m_pointer = copy.m_pointer;
	return *this;
}

template<typename T>
inline void StackLL<T>::Push(T pushed)
{
		//m_stack[m_pointer++] = pushed;
	m_stack.Append(pushed);
	m_pointer++;
}

template<typename T>
inline T StackLL<T>::Pop()
{
	T out;
	if (m_pointer > 0)
	{
		out = m_stack.Last();
		m_stack.Extract(out);
		m_pointer--;
	}
	else
		throw Exception("Stack Underflow Exception");
	return out;
}

template<typename T>
inline T StackLL<T>::Peek()
{
	T out;
	if (m_pointer > 0)
		out = m_stack.Last();
	else
		throw Exception("Stack Underflow Exception");
	return out;
}

template<typename T>
inline int StackLL<T>::Size() const
{
	return m_pointer;
}

template<typename T>
inline bool StackLL<T>::isEmpty() const
{
	return m_pointer == 0;
}

#endif