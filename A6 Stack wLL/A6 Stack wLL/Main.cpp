#include <iostream>
#include <string>
#include "StackLL.h"

using std::cout;
using std::endl;
using std::string;

int main()
{
	//Creatings stacks testing both constructors
	cout << "Creating Stacks" << endl << endl;
	StackLL<int> iTest;
	StackLL<string> sTest;

	//Filling all the stacks.
	cout << "Filling stacks" << endl << endl;
	for (int i = 0; i < 100; i++)
	{
		iTest.Push(i);
		sTest.Push("Test100");
	}

	//Testing Peek
	cout << "Peeking last values" << endl << endl;
	cout << '\t' << sTest.Peek() << endl;
	cout << endl;

	//Testing Size
	if (sTest.Size() == 100)
		cout << "Size test passed" << endl << endl;
	else
		cout << "Size test failed, value= " << sTest.Size()  << endl << endl;

	//Testing popping out all values into cout;
	cout << "Popping out all values" << endl << endl;
	for (int i = 0; i < 100; i++)
	{
		sTest.Pop();
		iTest.Pop();
	}

	//Testing isEmpty
	if (sTest.isEmpty() == true)
		cout << "isEmpty test passed" << endl << endl;
	else
		cout << "isEmpty test failed" << endl << endl;
	//Testing Stack Underflow
	cout << "Did Underflow test pass?" << endl;
	try
	{
		sTest.Pop();
	}
	catch (Exception msg)
	{
		cout << "\tYes" << endl << endl;
	}
	//Testing Copy Constructor
	iTest.Push(1);
	StackLL<int> iTest3(iTest);
	if (iTest3.Peek() == 1)
		cout << "Copy Constructor test passed" << endl << endl;
	else
		cout << "Copy Constructor test failed" << endl << endl;
	//Testing OP=
	StackLL<int> iTest4;
	iTest4.Push(2);
	iTest3 = iTest4;
	if (iTest3.Peek() == 2)
		cout << "Operator= test passed" << endl << endl;
	else
		cout << "Operator= test failed" << endl << endl;
	return 0;
}