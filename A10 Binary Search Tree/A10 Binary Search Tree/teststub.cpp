#include "BSTree.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

void Print(const int & in);
void PrintStr(const string & in);

int main()
{
	BSTree<int> test;
	test.Insert(1);
	test.Insert(2);
	test.Insert(0);
	test.Insert(5);
	test.Insert(10);
	test.Insert(15);
	test.Insert(12);
	test.Insert(4);
	test.Insert(3);
	test.Insert(2);
	cout << "InOrder test" << endl;
	test.InOrder(Print);
	cout << "PreOrder test" << endl;
	test.PreOrder(Print);
	cout << "PostOrder test" << endl;
	test.PostOrder(Print);
	cout << "Breadth-First test" << endl;
	test.BreadthFirst(Print);
	cout << "Heighth = " << test.Height() << endl;
	cout << "Deleting 5" << endl;
	test.Delete(5);
	cout << "Bread-Firsting test again" << endl;
	test.BreadthFirst(Print);
	cout << "Heighth = " << test.Height() << endl;
	cout << "Copying into test2" << endl;
	BSTree<int> test2 = test;
	cout << "Breadth-Firsting test2" << endl;
	test2.BreadthFirst(Print);
	cout << "Heighth = " << test2.Height() << endl;
	cout << "Deleting 1" << endl;
	test2.Delete(1);
	cout << "Testing Deleting something that doesnt exist" << endl;
	test2.Delete(42);
	cout << "Breadth-Firsting test2" << endl;
	test2.BreadthFirst(Print);
	cout << "Heighth = " << test2.Height() << endl;
	cout << "Deleting 12, 15, 4" << endl;
	test2.Delete(12);
	test2.Delete(15);
	test2.Delete(4);
	cout << "Breadth-Firsting test2" << endl;
	test2.BreadthFirst(Print);
	cout << "Heighth = " << test2.Height() << endl;
	cout << "Manually deleting the rest." << endl;
	test2.Delete(10);
	test2.Delete(2);
	test2.Delete(0);
	test2.Delete(2);
	test2.Delete(3);
	cout << "Testing deleting something that doesnt exist." << endl;
	test2.Delete(4);
	test2.BreadthFirst(Print);
	try 
	{
		cout << "Heighth = " << test2.Height() << endl;
	}
	catch (Exception msg)
	{
		cout << msg << endl;
	}

	//Small test with strings
	BSTree<string> strtest;
	strtest.Insert("Hi");
	strtest.Insert("Hello");
	strtest.Insert("Hello");
	BSTree<string> strcpytest;
	strcpytest = strtest;
	BSTree<string> stroptest = strtest;
	strtest.Delete("Hi");
	strtest.Height();
	strcpytest.Delete("Hi");
	strcpytest.InOrder(PrintStr);
	strcpytest.PreOrder(PrintStr);
	strcpytest.PostOrder(PrintStr);
	strcpytest.BreadthFirst(PrintStr);
	return 0;
}

void Print(const int & in)
{
	cout << in << endl;
}

void PrintStr(const string & in)
{
	cout << in << endl;
}