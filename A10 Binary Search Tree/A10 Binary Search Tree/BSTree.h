#ifndef BSTREE_H
#define BSTREE_H

#include "TNode.h"
#include "QueueLL.h"

template <typename T>
class BSTree
{
public:
	//Canonical functions
	BSTree();
	BSTree(const BSTree<T> & copy);
	BSTree<T> & operator=(const BSTree<T> & copy);
	~BSTree();
	//Public member functions
	void Insert(const T & in);
	void Delete(const T & in);
	void Purge();
	int Height(); //Determine the height of the tree.
	//Traversal Functions
	void InOrder(void (visit)(const T & visiting));
	void PreOrder(void (visit)(const T & visiting));
	void PostOrder(void (visit)(const T & visiting));
	void BreadthFirst(void (visit)(const T & visiting));

private:
	TNode<T> * m_root;
	void Insert(const T & in, TNode<T> *& root);
	void Delete(const T & in, TNode<T> *& root);
	void Purge(TNode<T> *& root);
	int Height(TNode<T> * root);
	void InOrder(void (visit)(const T & visiting), TNode<T> * root);
	void PreOrder(void (visit)(const T & visiting), TNode<T> * root);
	void PostOrder(void (visit)(const T & visiting), TNode<T> * root);
};

template <typename T>
BSTree<T>::BSTree()
	:m_root(nullptr)
{ }
template <typename T>
BSTree<T>::BSTree(const BSTree<T>& copy)
	:m_root(nullptr)
{
	*this = copy;
}
template <typename T>
BSTree<T>& BSTree<T>::operator=(const BSTree<T>& copy)
{
	if (this != &copy)
	{
		QueueLL<TNode<T> *> visitQueue;
		TNode<T> * tmp = nullptr;
		if (copy.m_root != nullptr)
		{
			Purge();
			visitQueue.Enqueue(copy.m_root);
			while (!visitQueue.isEmpty())
			{
				tmp = visitQueue.Dequeue();
				if (tmp->m_L != nullptr)
					visitQueue.Enqueue(tmp->m_L);
				if (tmp->m_R != nullptr)
					visitQueue.Enqueue(tmp->m_R);
				Insert(tmp->m_data);
			}
		}
	}
	return *this;
}
template <typename T>
BSTree<T>::~BSTree()
{
	Purge();
}
template<typename T>
inline void BSTree<T>::Insert(const T & in, TNode<T>*& root)
{
	if (root == nullptr)
	{
		root = new TNode<T>(in);
	}
	else
	{
		if (in <= root->m_data)
			Insert(in, root->m_L);
		else
			Insert(in, root->m_R);
	}
}
template<typename T>
inline void BSTree<T>::Delete(const T & in, TNode<T>*& root)
{
	if (root != nullptr)
	{
		if (root->m_data == in)
		{
			//delete
			//if no children, just delete and set it's pointer to 0.
			if (root->m_L == nullptr && root->m_R == nullptr)
			{
				delete root;
				root = nullptr;
			}
			//if it has one child.
			else if ((root->m_L == nullptr && root->m_R != nullptr) || (root->m_L != nullptr) && root->m_R == nullptr)
			{
				if (root->m_L == nullptr)
				{
					TNode<T> * tmp = root;
					root = root->m_R;
					delete tmp; //tmp ptr dies after the } one line down.
				}
				else
				{
					TNode<T> * tmp = root;
					root = root->m_L;
					delete tmp; //tmp ptr dies after the } one line down.
				}
			}
			//has 2 children.
			else
			{
				TNode<T> *toRemove = root;
				TNode<T> *tmpchild = root->m_R;
				TNode<T> *tmpchildbefore = root;
				while (tmpchild->m_L != nullptr) 
				{
					tmpchildbefore = tmpchild;
					tmpchild = tmpchild->m_L;
				}
				if (tmpchildbefore == root)
				{
					tmpchild->m_L = root->m_L;
					root = tmpchild;
				}
				else
				{
					tmpchildbefore->m_L = tmpchild->m_R;
					tmpchild->m_L = root->m_L;
					tmpchild->m_R = root->m_R;
					root = tmpchild;
				}
				delete toRemove;
				toRemove = nullptr;
			}
		}
		else if (in < root->m_data)
			Delete(in, root->m_L);
		else
			Delete(in, root->m_R);
	}
}
template<typename T>
inline void BSTree<T>::Insert(const T & in)
{
	Insert(in, m_root);
}
template<typename T>
inline void BSTree<T>::Delete(const T & in)
{
	Delete(in, m_root);
}
template<typename T>
inline void BSTree<T>::Purge()
{
	Purge(m_root);
}
template<typename T>
inline void BSTree<T>::Purge(TNode<T> *& root)
{
	if (root != nullptr)
	{
		Purge(root->m_L);
		Purge(root->m_R);
		delete root;
		root = nullptr;
	}
}
template<typename T>
inline int BSTree<T>::Height(TNode<T>* root)
{
	int out = 0;
	if (root != nullptr)
	{
		int left = Height(root->m_L);
		int right = Height(root->m_R);
		if (left > right)
			out = left + 1;
		else
			out = right + 1;
	}
	return out;
}
template<typename T>
inline void BSTree<T>::InOrder(void(visit)(const T &visiting), TNode<T>* root)
{
	if (root != nullptr)
	{
		InOrder(visit, root->m_L);
		visit(root->m_data);
		InOrder(visit, root->m_R);
	}
}
template<typename T>
inline void BSTree<T>::PreOrder(void(visit)(const T &visiting), TNode<T>* root)
{
	if (root != nullptr)
	{
		visit(root->m_data);
		InOrder(visit, root->m_L);
		InOrder(visit, root->m_R);
	}
}
template<typename T>
inline void BSTree<T>::PostOrder(void(visit)(const T &visiting), TNode<T>* root)
{
	if (root != nullptr)
	{
		visit(root->m_data);
		InOrder(visit, root->m_L);
		InOrder(visit, root->m_R);
	}
}
template<typename T>
inline int BSTree<T>::Height()
{
	if (m_root == nullptr)
		throw Exception("No existing tree!");
	return Height(m_root);
}
template<typename T>
inline void BSTree<T>::InOrder(void(visit)(const T &visiting))
{
	if (visit != nullptr)
		InOrder(visit, m_root);
}
template<typename T>
inline void BSTree<T>::PreOrder(void(visit)(const T &visiting))
{
	if (visit != nullptr)
		PreOrder(visit, m_root);
}
template<typename T>
inline void BSTree<T>::PostOrder(void(visit)(const T &visiting))
{
	if (visit != nullptr)
		PostOrder(visit, m_root);
}
template<typename T>
inline void BSTree<T>::BreadthFirst(void(visit)(const T &visiting))
{
	QueueLL<TNode<T> *> visitQueue;
	TNode<T> * tmp = nullptr;
	if (m_root != nullptr && visit != nullptr)
	{
		visitQueue.Enqueue(m_root);
		while (!visitQueue.isEmpty())
		{
			tmp = visitQueue.Dequeue();
			if (tmp->m_L != nullptr)
				visitQueue.Enqueue(tmp->m_L);
			if (tmp->m_R != nullptr)
				visitQueue.Enqueue(tmp->m_R);
			visit(tmp->m_data);
		}
	}
}
#endif
