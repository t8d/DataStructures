#ifndef TNODE_H
#define TNODE_H

template <typename T>
class TNode
{
private:
	//Canonical functions
	TNode(const T& in); //required Constructor
	TNode(const TNode & copy);
	TNode & operator=(const TNode & copy);
	~TNode();

	//Variables
	T m_data;
	TNode * m_L;
	TNode * m_R;
	//Friend class, private class to all but my BSTree.
	template <typename T>
	friend class BSTree; //will add other trees when needed.
};

template <typename T>
TNode<T>::TNode(const T& in)
	:m_data(in), m_L(nullptr), m_R(nullptr)
{ }

template <typename T>
TNode<T>::TNode(const TNode & copy)
	:m_data(copy.m_data), m_L(nullptr), m_R(nullptr)
{ }

template <typename T>
TNode<T>& TNode<T>::operator=(const TNode & copy)
{
	if (this != &copy)
	{
		m_data = copy.m_data;
		m_L = nullptr;
		m_R = nullptr;
	}
	return *this;
}

template <typename T>
TNode<T>::~TNode()
{ }
#endif
