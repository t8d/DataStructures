#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <iostream>
using std::ostream;
using std::endl;
using std::cout;
using std::strcpy;
using std::strlen;

/************************************************************************
* Class: Exception
*
* Purpose: This class handles exceptions thrown by the other classes.
*
* Manager functions:
* Exception ( )
* Exception (char * msg)
* Exception (const Exception & copy)
* ~Exception()
* operator = (const Exception & copy)
*
* Methods:
* getMessage(): Returns m_msg
* setMessage(char * msg): set's m_msg to passed char *.
*************************************************************************/

class Exception
{
private:
	char * m_msg;
public:
	Exception();
	Exception(const char * msg);
	Exception(Exception & copy);
	~Exception();
	Exception & operator= (const Exception & copy);
	const char * getMessage() const;
	void setMessage(const char * msg);
	friend ostream & operator<<(ostream & stream, const Exception & except);
};

#endif
