#include "Exception.h"

Exception::Exception()
	:m_msg(nullptr)
{ }

Exception::Exception(const char * msg)
{
	m_msg = new char[strlen(msg) + 1];
	strcpy(m_msg, msg);
}

Exception::Exception(Exception & copy)
{
	m_msg = new char[strlen(copy.m_msg) + 1];
	strcpy(m_msg, copy.m_msg);
}

Exception::~Exception()
{
	delete [] m_msg;
}

Exception & Exception::operator=(const Exception & copy)
{
	if (this != &copy)
	{
		delete[] m_msg;
		m_msg = new char[strlen(copy.m_msg) + 1];
		strcpy(m_msg, copy.m_msg);
	}
	return *this;
}

const char * Exception::getMessage() const
{
	return m_msg;
}

void Exception::setMessage(const char * msg)
{
	delete[] m_msg;
	m_msg = new char[strlen(msg) + 1];
	strcpy(m_msg, msg);
}

ostream & operator<<(ostream & stream, const Exception & except)
{
	if (except.m_msg != nullptr)
		stream << except.m_msg;
	return stream;
}
