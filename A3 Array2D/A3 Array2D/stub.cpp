#include "Array2D.h"
#include <string>
using std::string;
#define _CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC

#include <string>
using std::string;

//Function declarations.
void TestConstructors();
void TestOpEq();
void TestAccessors();
void TestMutators();
void TestSelectOperators();
void CreateDisplayTable();

int main()
{
	//Memory leak checker
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	//Not testing Exception.h or Array.h, since they have been tested in Assignment 1's stub file.

	TestConstructors();
	cout << endl;
	TestOpEq();
	cout << endl;
	TestAccessors();
	cout << endl;
	TestMutators();
	cout << endl;
	TestSelectOperators();
	cout << endl;
	CreateDisplayTable();
	cout << endl;
	
	return 0;
}

void TestConstructors()
{
	cout << "Array2D constructor test commencing" << endl;
	try
	{
		Array2D<int> test;
		Array2D<string> test2(2, 4);
		test2[0][0] = "Hello!";
		Array2D<string> test3(test2);

		if (test3[0][0] == test2[0][0])
			cout << "Array2D Constructor test passed." << endl;
		else
			cout << "Array2D Constructor test failed." << endl;

	}
	catch (...)
	{
		cout << "Array2D Constructor test failed." << endl;
	}

	cout << "Row Constructor test commencing" << endl;
	try
	{
		Array2D<int> test(5, 5);
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				test[i][j] = (i + j);
			}
		}
		Row<int> test2(test, 4);
		if (test2[4] == 8)
			cout << "Row Constructor test passed." << endl;
		else
			cout << "Row Construtor test failed." << endl;
	}
	catch (...)
	{
		cout << "Row Constructors test failed." << endl;
	}
}

void TestOpEq()
{
	cout << "Testing OP= for Array2D" << endl;
	try
	{
		Array2D<int> test(5, 5);
		test[4][4] = 4;
		Array2D<int> test2(5, 5);
		test2[4][4] = 8;
		test2 = test;
		if (test2[4][4] == 4)
			cout << "OP= for Array2D test passed." << endl;
		else
			cout << "OP= for Array2D test failed." << endl;
	}
	catch (...)
	{
		cout << "OP= for Array2D test failed." << endl;
	}
}

void TestAccessors()
{
	cout << "Testing Array2D Accessors" << endl;
	try
	{
		Array2D<int> test(20, 4);
		bool passed = true;
		if (test.getRow() != 20)
			passed = false;
		if (test.getColumn() != 4)
			passed = false;

		if (passed)
			cout << "Array2D Accessor test passed." << endl;
	}
	catch (...)
	{
		cout << "Array2D Accessor test failed." << endl;
	}
}

void TestMutators()
{
	cout << "Testing Array2D Mutators" << endl;
	try
	{
		Array2D<int> test(5, 5);
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				test[i][j] = (i * j);
			}
		}
		test.setColumn(4);
		test.setRow(4);
		try
		{
			test[4][4] = 16;
		}
		catch (Exception msg)
		{
			cout << "Testing out of bounds for new boundries" << endl;
			cout << msg << endl;
		}
		if (test[3][3] == 0)
			cout << "Array2D Mutator test 1 passed." << endl;
		else
			cout << "Array2D Mutator test 1 failed (accessor[] test failed)" << endl;

		cout << "Testing setting row and col to negative values" << endl;
		test.setRow(-5);
		test.setColumn(-5);
		bool passed = (test.getColumn() == 0 && test.getRow() == 0);
		if (passed)
			cout << "Array2D Mutator test 2 passed." << endl;
		else
			cout << "Array2D Mutator test 2 failed." << endl;

	}
	catch (...)
	{
		cout << "Array2D Mutator test failed." << endl;
	}
}

void TestSelectOperators()
{
	cout << "Array2D Operator[] test (implies Row[] test as well)" << endl;
	try
	{
		cout << "Testing Row out of bounds" << endl;
		Array2D<int> test(5, 5);
		test[6][4] = 0;
	}
	catch (Exception msg)
	{
		cout << msg << endl;
	}
	try
	{
		cout << "Testing Column out of bounds" << endl;
		Array2D<int> test(5, 5);
		test[4][6] = 0;
	}
	catch (Exception msg)
	{
		cout << msg << endl;
	}
	try
	{
		cout << "Testing negative row index." << endl;
		Array2D<int> test(5, 5);
		test[-4][4] = 0;
	}
	catch (Exception msg)
	{
		cout << msg << endl;
	}
	try
	{
		cout << "Testing negative column index." << endl;
		Array2D<int> test(5, 5);
		test[4][-4] = 0;
	}
	catch (Exception msg)
	{
		cout << msg << endl;
	}
}

void CreateDisplayTable()
{
	cout << "Creating a 10x10 table, populating it, and accessing [3][3]" << endl << endl;;
	Array2D<int> test(10, 10);
	int numero = 0;
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			test[i][j] = ++numero;
			cout << numero << '\t';;
		}
		cout << endl;
	}
	cout << endl << "test[3][3] = " << test[3][3] << endl << endl;
	cout << "Setting Col and Row to 4, and displaying it." << endl;
	cout << "Notice, it wont chop columns or rows, it reorganizes them by element order." << endl;
	test.setColumn(4);
	test.setRow(4);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << test[i][j] << '\t';;
		}
		cout << endl;
	}
}

