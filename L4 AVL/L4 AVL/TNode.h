/************************************************************************
* Class: TNode
*
* Purpose: This class is the Tree Node class that carries two TNode ptr's.
*
* Manager functions:
*	All the canonical functions. - 0 arg constructor.
*	TNode(T in) 1 arg constructor, setting it's data upon creation.
* Methods:
*	None.
* ...
*************************************************************************/

#ifndef TNODE_H
#define TNODE_H

//Small enum to make life easy, for the balance factor used by AVL Tree.
enum Balance
{ LH, EH, RH };

template <typename T>
class TNode
{
private:
	//Canonical functions
	TNode(const T& in); //required Constructor
	TNode(const TNode & copy);
	TNode & operator=(const TNode & copy);
	~TNode();

	//Variables
	T m_data;
	TNode * m_L;
	TNode * m_R;
	Balance bal;
	//Friend class, private class to all but my Tree classes..
	template <typename U>
	friend class BSTree; //will add other trees when needed.
	template <typename V>
	friend class BTree;
	template <typename Q>
	friend class AVLTree;
};

//1 arg constructor. Copying T into m_data.
template <typename T>
TNode<T>::TNode(const T& in)
	:m_data(in), m_L(nullptr), m_R(nullptr), bal(EH)
{ }

//Copy constructor.
template <typename T>
TNode<T>::TNode(const TNode & copy)
	:m_data(copy.m_data), m_L(nullptr), m_R(nullptr), bal(copy.bal)
{ }

//OP= copier function.
template <typename T>
TNode<T>& TNode<T>::operator=(const TNode & copy)
{
	if (this != &copy)
	{
		m_data = copy.m_data;
		m_L = nullptr;
		m_R = nullptr;
		bal = copy.bal;
	}
	return *this;
}

//Destructor.
template <typename T>
TNode<T>::~TNode()
{ }
#endif
