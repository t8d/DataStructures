/************************************************************************
* Class: BSTree
*
* Purpose: This class creates a Binary Search Tree.
*
* Manager functions:
*	All the canonical functions. Same stuff as always.
*													 
* Methods:
*	Insert() Inserts item to tree.
*	Delete() Deletes item from tree (Keeps binary search tree integrity).
* ...
*************************************************************************/
#ifndef BSTREE_H
#define BSTREE_H

#include "BTree.h"

template <typename T>
class BSTree : public BTree<T>
{
public:
	//Canonical functions
	BSTree();
	BSTree(const BSTree<T> & copy);
	BSTree<T> & operator=(const BSTree<T> & copy);
	~BSTree();
	//Public member functions
	void Insert(const T & in) override;
	void Delete(const T & in) override;
	

private:
	
	void Insert(const T & in, TNode<T> *& root);
	void Delete(const T & in, TNode<T> *& root);	

};

//default constructor. Nothing special.
template <typename T>
BSTree<T>::BSTree()
	:BTree()
{ }

//Copy constructor.. It actually just calls the OP= to handle it.
template <typename T>
BSTree<T>::BSTree(const BSTree<T>& copy)
	:BSTree()
{
	*this = copy;
}

//OP= copies passed tree into itself.
template <typename T>
BSTree<T>& BSTree<T>::operator=(const BSTree<T>& copy)
{
	if (this != &copy)
	{
		QueueLL<TNode<T> *> visitQueue;
		TNode<T> * tmp = nullptr;
		if (copy.m_root != nullptr)
		{
			BTree<T>::Purge();
			visitQueue.Enqueue(copy.m_root);
			while (!visitQueue.isEmpty())
			{
				tmp = visitQueue.Dequeue();
				if (tmp->m_L != nullptr)
					visitQueue.Enqueue(tmp->m_L);
				if (tmp->m_R != nullptr)
					visitQueue.Enqueue(tmp->m_R);
				Insert(tmp->m_data);
			}
		}
	}
	return *this;
}

//Destructor.. Nothing to see here.
template <typename T>
BSTree<T>::~BSTree()
{ }

/**********************************************************************
* Purpose: Insert item into tree (in order).
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Item is inserted and tree is still keeping it's BST integrity.
*
************************************************************************/
template<typename T>
void BSTree<T>::Insert(const T & in, TNode<T>*& root)
{
	if (root == nullptr)
	{
		root = new TNode<T>(in);
	}
	else
	{
		if (in <= root->m_data)
			Insert(in, root->m_L);
		else
			Insert(in, root->m_R);
	}
}

/**********************************************************************
* Purpose: Delete item from tree.
*
* Precondition:
*     Item to delete previously exists in tree.
*
* Postcondition:
*      Item is deleted and tree is still keeping it's BST integrity.
*
************************************************************************/
template<typename T>
inline void BSTree<T>::Delete(const T & in, TNode<T>*& root)
{
	if (root != nullptr)
	{
		if (root->m_data == in)
		{
			//delete
			//if no children, just delete and set it's pointer to 0.
			if (root->m_L == nullptr && root->m_R == nullptr)
			{
				delete root;
				root = nullptr;
			}
			//if it has one child.
			else if ((root->m_L == nullptr && root->m_R != nullptr) || (root->m_L != nullptr) && root->m_R == nullptr)
			{
				if (root->m_L == nullptr)
				{
					TNode<T> * tmp = root;
					root = root->m_R;
					delete tmp; //tmp ptr dies after the } one line down.
				}
				else
				{
					TNode<T> * tmp = root;
					root = root->m_L;
					delete tmp; //tmp ptr dies after the } one line down.
				}
			}
			//has 2 children.
			else
			{
				TNode<T> *toRemove = root;
				TNode<T> *tmpchild = root->m_R;
				TNode<T> *tmpchildbefore = root;
				while (tmpchild->m_L != nullptr) 
				{
					tmpchildbefore = tmpchild;
					tmpchild = tmpchild->m_L;
				}
				if (tmpchildbefore == root)
				{
					tmpchild->m_L = root->m_L;
					root = tmpchild;
				}
				else
				{
					tmpchildbefore->m_L = tmpchild->m_R;
					tmpchild->m_L = root->m_L;
					tmpchild->m_R = root->m_R;
					root = tmpchild;
				}
				delete toRemove;
				// ReSharper disable once CppAssignedValueIsNeverUsed
				toRemove = nullptr;
			}
		}
		else if (in < root->m_data)
			Delete(in, root->m_L);
		else
			Delete(in, root->m_R);
	}
}

/**********************************************************************
* Purpose: Fake Insert. It is client facing, and actually just calls the
*	real insert that needed to be passed m_root.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Item is inserted and tree is still keeping it's BST integrity.
*
************************************************************************/
template<typename T>
inline void BSTree<T>::Insert(const T & in)
{
	Insert(in, BTree<T>::m_root);
}

/**********************************************************************
* Purpose: Fake Delete. It is client facing, and actually just calls the
*	real delete that needed to be passed m_root.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Item is deleted and tree is still keeping it's BST integrity.
*
************************************************************************/
template<typename T>
inline void BSTree<T>::Delete(const T & in)
{
	Delete(in, BTree<T>::m_root);
}

#endif
