/*************************************************************
*
* Lab/Assignment: Lab 4 � AVL Tree
*	By Tommy Miller
*
* Overview: This program contains the AVL Tree data structure
*	created for portability and simplicity to client.
*
* Input:
*	No user input, this is a test stub.
*
* Output:
*	Correctly functioning AVL Tree. (Derived from Binary Tree)
*
*
* Note: The AVL Tree algorithm is from "Data Structures: A
*	Pseudocode Approach with C++". Not going to claim it as my
*	own.. That, and why would someone call the AVL Tree
*	algorithm as their own?.. Georgy Adelson-Velsky and 
*	Evgenii Landis created it.. Hence the name AVL.
************************************************************/
#include "AVLTree.h"
#include "BSTree.h"
#include <iostream>
#define CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC

using std::cout;
using std::endl;

void Print(const int & in);

//Test stub. It creates AVL Tree Objects, fills them, output determines if it is working correctly.
int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	BSTree<int> totalBS; //make sure it still compiles.
	AVLTree<int> test;
	cout << "Testing Insert" << endl;
	test.Insert(8);
	test.Insert(9);
	test.Insert(10);
	test.Insert(2);
	test.Insert(1);
	test.Insert(5);
	test.Insert(3);
	test.Insert(6);
	test.Insert(4);
	test.Insert(7);
	test.Insert(11);
	test.BreadthFirst(Print);
	cout << endl;
	cout << "Testing OP= and Copy Constructor" << endl;
	AVLTree<int> test2 = test;
	AVLTree<int> test3;
	test3 = test2;
	test2.BreadthFirst(Print);
	cout << endl;
	test3.BreadthFirst(Print);
	cout << endl;
	cout << "Testing Deletion" << endl;
	test.Delete(5);
	test.Delete(3);
	test.Delete(4);
	test.Delete(2);
	test.Delete(8);
	test.Delete(6);
	test.Delete(10);
	test.Delete(11);
	test.Delete(1);
	test.Delete(7);
	test.Delete(9);
	cout << "Line below should be empty!" << endl << endl;
	test.BreadthFirst(Print);
	cout << "Line above this!" << endl;
	// No memory leaks here! ( } calls the destructor, murdering all the data).
}

//Print function to pass to BreadthFirst Iterator.
void Print(const int & in)
{
	cout << in << ' ';
}
