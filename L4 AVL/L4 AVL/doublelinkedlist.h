/************************************************************************
* Class: DoubleLinkedList
*
* Purpose: Data Structure for a Doubly Linked List.
*
* Manager functions:
*  *All canonical functions
*
* Methods:
*	getHead()
*	getTail()
*
*	disregard the test functions, didn't bother removing them..
*	isEmpty() returns if linkedlist is empty.
*	First() returns the reference to the first object in list.
*	Last() returns the reference to the last object in list.
*	Prepend(T in) Prepends list with in
*	Append(T in) Appends list with in.
*	Purge() Deletes the entire list.
*	Extract(T this) Finds and deletes "this".
*	InsertAfter(T in, T compare) Inserts after compare element
*	InsertBefore(T in, T before) Inserts before compare element
*************************************************************************/

#ifndef DOUBLELINKEDLIST_H
#define DOUBLELINKEDLIST_H

#include "node.h"

//for printfoward and printbackward
#include <iostream>
using std::cout;
using std::endl;

template <typename T>
class DoubleLinkedList
{
public:
	//Canonical Methods
	DoubleLinkedList();
	DoubleLinkedList(const DoubleLinkedList & copy);
	~DoubleLinkedList();
	DoubleLinkedList & operator=(const DoubleLinkedList & copy);

	//Accessors
	Node<T> * getHead() const;
	Node<T> * getTail() const;

	//Methods for testing
	void PrintForwards() const;
	void PrintBackwards() const;

	//Methods
	bool isEmpty() const;
	T & First() const;
	T & Last() const;
	void Prepend(const T & data);
	void Append(const T & data);
	void Purge();
	void Extract(T toDelete);
	void InsertAfter(const T & new_data, const T & existing_data);
	void InsertBefore(const T & new_data, const T & existing_data);


private:
	Node<T> * m_head;
	Node<T> * m_tail;
};


//Default constructor.. nothing special here.
template<typename T>
inline DoubleLinkedList<T>::DoubleLinkedList()
	:m_head(nullptr), m_tail(nullptr)
{ }

//default copy constructor.. nothing special here.
template<typename T>
inline DoubleLinkedList<T>::DoubleLinkedList(const DoubleLinkedList<T> & copy)
	: m_head(nullptr), m_tail(nullptr)
{
	*this = copy;
}

//destructor.. nothing special here.
template<typename T>
inline DoubleLinkedList<T>::~DoubleLinkedList()
{
	Purge();//murders data before the carrier objects pending death.
}

//OP=.. Copies object, just as you'd expect.
template<typename T>
inline DoubleLinkedList<T> & DoubleLinkedList<T>::operator=(const DoubleLinkedList & copy)
{
	if (this != &copy)
	{
		Purge();
		Node<T> * travel = copy.m_head;
		while (travel != nullptr)
		{
			Append(travel->m_data);
			travel = travel->m_next;
		}
	}
	return *this;
}

/**********************************************************************
* Purpose: Returns pointer to Head node.
*
* Precondition:
*     Data exists.
*
* Postcondition:
*     Head ptr is returned.
*
************************************************************************/
template<typename T>
inline Node<T> * DoubleLinkedList<T>::getHead() const
{
	return m_head;
}

/**********************************************************************
* Purpose: Returns pointer to Tail node.
*
* Precondition:
*     Data exists.
*
* Postcondition:
*     Tail ptr is returned.
*
************************************************************************/
template<typename T>
inline Node<T> * DoubleLinkedList<T>::getTail() const
{
	return m_tail;
}

/**********************************************************************
* Purpose: Prints the linkedlist's data out to console, in order.
*
* Precondition:
*     Data exists.
*
* Postcondition:
*     LinkedList data is displayed.
*
************************************************************************/
template<typename T>
inline void DoubleLinkedList<T>::PrintForwards() const
{
	Node<T> * travel = m_head;
	cout << "Printing contents of linked list in forwards order." << endl;
	while (travel != nullptr)
	{
		cout << travel->m_data << endl;
		travel = travel->m_next;
	}
}

/**********************************************************************
* Purpose: Prints the linkedlist's data out to console.. backwards.
*
* Precondition:
*     Data exists.
*
* Postcondition:
*     LinkedList data is displayed (backwards).
*
************************************************************************/
template<typename T>
inline void DoubleLinkedList<T>::PrintBackwards() const
{
	Node<T> * travel = m_tail;
	cout << "Printing contents of linked list in backwards order." << endl;
	while (travel != nullptr)
	{
		cout << travel->m_data << endl;
		travel = travel->m_prev;
	}
}

/**********************************************************************
* Purpose: Returns true if list is empty.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns true only if list is empty.
*
************************************************************************/
template<typename T>
inline bool DoubleLinkedList<T>::isEmpty() const
{
	bool Empty = true;
	if (m_head != nullptr)
		Empty = false;
	return Empty;
}

/**********************************************************************
* Purpose: Returns reference to first data element.
*
* Precondition:
*     Data exists.
*
* Postcondition:
*     Returns reference to first node's data, allowing for modifcation.
*
************************************************************************/
template<typename T>
inline T & DoubleLinkedList<T>::First() const
{
	if (isEmpty())
		throw "Empty List!";
	return m_head->m_data;
}

/**********************************************************************
* Purpose: Returns reference to the last data element.
*
* Precondition:
*     Data exists.
*
* Postcondition:
*     Returns reference to tail's node's data, allowing for modifcation.
*
************************************************************************/
template<typename T>
inline T & DoubleLinkedList<T>::Last() const
{
	if (isEmpty())
		throw "Empty List!";
	return m_tail->m_data;
}

/**********************************************************************
* Purpose: Prepends list with inserted data.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Data is inserted before everything in the list.
*
************************************************************************/
template<typename T>
inline void DoubleLinkedList<T>::Prepend(const T & data)
{
	//Node<T> * temp = new Node<T>(data);
	if (!isEmpty())
	{
		m_head->m_prev = new Node<T>(data);
		m_head->m_prev->m_next = m_head;
		m_head = m_head->m_prev;
	}
	else //Empty list
		m_head = m_tail = new Node<T>(data);
}

/**********************************************************************
* Purpose: Appends list with inserted data.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Data is inserted at the end of the list.
*
************************************************************************/
template<typename T>
inline void DoubleLinkedList<T>::Append(const T & data)
{
	Node<T> * temp = new Node<T>(data);
	if (!isEmpty())
	{
		m_tail->m_next = temp;
		temp->m_prev = m_tail;
		m_tail = temp;
	}
	else //Empty list
		m_head = m_tail = temp;
}


/**********************************************************************
* Purpose: Murders the list.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     All data in this container will be destroyed.
*
************************************************************************/
template<typename T>
inline void DoubleLinkedList<T>::Purge()
{
	if (!isEmpty())
	{
		Node<T> * travel = m_head;
		while (travel->m_next != nullptr)
		{
			travel = travel->m_next;
			delete travel->m_prev;
		}
		delete m_tail;
		m_head = nullptr;
		m_tail = nullptr;
	}
}

/**********************************************************************
* Purpose: Removes a node from list.
*
* Precondition:
*     Requested deleted element exists in list.
*
* Postcondition:
*     The requested node is deleted from list.
*
************************************************************************/
template<typename T>
inline void DoubleLinkedList<T>::Extract(T toDelete)
{
	if (!isEmpty())
	{
		Node<T> * travel = m_head;
		bool found = false;
		while (travel != nullptr && found == false)
		{
			if (travel->m_data == toDelete)
				found = true;
			else
				travel = travel->m_next;
		}
		if (found)
		{
			if (m_head == m_tail) //if the deleting item is the only item in list
			{
				Purge(); //Just murders the list.
			}
			else if (travel == m_head) //If deleting item is the m_head as well.
			{
				m_head = m_head->m_next;
				m_head->m_prev = nullptr;
				delete travel;
			}
			else if (travel == m_tail) //If deleting item is the m_tail as well.
			{
				m_tail = m_tail->m_prev;
				m_tail->m_next = nullptr;
				delete travel;
			}
			else { //Deleting item is middle of the list somewhere.
				travel->m_prev->m_next = travel->m_next;
				travel->m_next->m_prev = travel->m_prev;
				delete travel;
			}
		}

	}
}

/**********************************************************************
* Purpose: Adds in data after requested node.
*
* Precondition:
*     Comparison node is in the list.
*
* Postcondition:
*     Data is inserted after the compare node.
*
************************************************************************/
template<typename T>
inline void DoubleLinkedList<T>::InsertAfter(const T & new_data, const T & existing_data)
{
	if (!isEmpty())
	{
		Node<T> * temp = new Node<T>(new_data);
		if ((m_head == m_tail) && (m_head->m_data == existing_data)) //Only thing in list. New item needs to append to it.
		{
			m_tail = temp;
			m_head->m_next = m_tail;
			m_tail->m_prev = m_head;
		}
		else //needs to loop to find where to add.
		{
			if (m_head == m_tail)
			{
				delete temp;
				throw "Only item in list is not the same as passed item to compare against.";
			}
			else
			{
				Node<T> * travel = m_head;
				bool done = false;
				while (travel != nullptr && !done)
				{
					if (travel->m_data == existing_data)
						done = true;
					else
						travel = travel->m_next;
				}
				if (done)
				{
					if (travel != m_tail)
					{
						//Travel -- temp -- travel->m_next
						temp->m_next = travel->m_next;
						temp->m_prev = travel;
						travel->m_next->m_prev = temp;
						travel->m_next = temp;
					}
					else
					{
						//travel -- temp -- nullptr.
						temp->m_prev = travel;
						temp->m_next = nullptr;
						travel->m_next = temp;
						m_tail = temp;
					}

				}
				else
				{
					delete temp;
					throw "No matching item found!";
				}
			}
		}
	}
	else
	{
		throw "No list, so no data to compare with.";
	}
}

/**********************************************************************
* Purpose: Adds in data before requested node.
*
* Precondition:
*     Comparison node is in the list.
*
* Postcondition:
*     Data is inserted before the compare node.
*
************************************************************************/
template<typename T>
inline void DoubleLinkedList<T>::InsertBefore(const T & new_data, const T & existing_data)
{

	if (!isEmpty())
	{
		Node<T> * temp = new Node<T>(new_data);
		if ((m_head == m_tail) && (m_head->m_data == existing_data)) //Only thing in list. New item needs to prepend to it.
		{
			m_head = temp;
			m_head->m_next = m_tail;
			m_tail->m_prev = m_head;
		}
		else //needs to loop to find where to add
		{
			if (m_head == m_tail)
			{
				delete temp;
				throw "Only item in list is not the same as passed item to compare against.";
			}
			else
			{
				Node<T> * travel = m_head;
				bool done = false;
				while (travel != nullptr && !done)
				{
					if (travel->m_data == existing_data)
						done = true;
					else
						travel = travel->m_next;
				}
				if (done)
				{
					if (travel != m_head)
					{
						//Travel->m_prev --Temp --Travel
						temp->m_prev = travel->m_prev;
						temp->m_next = travel;
						travel->m_prev->m_next = temp;
						travel->m_prev = temp;
					}
					else
					{
						//Nullptr -- Temp -- Travel
						temp->m_next = travel;
						temp->m_prev = nullptr;
						travel->m_prev = temp;
						m_head = temp;
					}
				}
				else
				{
					delete temp;
					throw "No matching item found!";
				}
			}
		}
	}
	else
	{
		throw "No list, so no data to compare with.";
	}
}

#endif