/************************************************************************
* Class: AVLTree
*
* Purpose: This class creates a Binary Search Tree with AVL Balancing
*	algorithms.
*
* Manager functions:
*	All the canonical functions. They never change. (War never changes..)
*													 (get my reference?)
* Methods:
*	Insert() Inserts item to tree (triggering balancing on return stack).
*	Delete() Deletes item from tree (triggering balancing on return stack).
* ...
*************************************************************************/
#ifndef AVLTREE_H
#define AVLTREE_H

#include "BTree.h"

template <typename T>
class AVLTree : public BTree<T>
{
public:
	AVLTree();
	AVLTree(const AVLTree & copy);
	AVLTree & operator= (const AVLTree & copy);
	~AVLTree();
	void Insert(const T & in) override;
	void Delete(const T & in) override;

private:
	TNode<T>*& Insert(const T & in, TNode<T> *& root, bool & taller);
	TNode<T>* Delete(const T & out, TNode<T> *& root, bool & shorter, bool & success);
	TNode<T>*& LeftBalance(TNode<T> *& root, bool & taller);
	TNode<T>*& RightBalance(TNode<T> *& root, bool & taller);
	TNode<T>*& RotateRight(TNode<T> *& root);
	TNode<T>*& RotateLeft(TNode<T> *& root);
	TNode<T>*& DeleteRightBalance(TNode<T> *& root, bool & shorter);
	TNode<T>*& DeleteLeftBalance(TNode<T> *& root, bool & shorter);
};

//default constructor.
template<typename T>
inline AVLTree<T>::AVLTree()
	:BTree()
{ }

//Copy constructor.
template<typename T>
inline AVLTree<T>::AVLTree(const AVLTree & copy)
{
	*this = copy;
}

//OP= copyier function.
template<typename T>
inline AVLTree<T> & AVLTree<T>::operator=(const AVLTree & copy)
{
	if (this != &copy)
	{
		QueueLL<TNode<T> *> visitQueue;
		TNode<T> * tmp = nullptr;
		if (copy.m_root != nullptr)
		{
			BTree<T>::Purge();
			visitQueue.Enqueue(copy.m_root);
			while (!visitQueue.isEmpty())
			{
				tmp = visitQueue.Dequeue();
				if (tmp->m_L != nullptr)
					visitQueue.Enqueue(tmp->m_L);
				if (tmp->m_R != nullptr)
					visitQueue.Enqueue(tmp->m_R);
				Insert(tmp->m_data);
			}
		}
	}
	return *this;
}

//Destructor.
template<typename T>
inline AVLTree<T>::~AVLTree()
{ }

/**********************************************************************
* Purpose: This function basically calls the REAL insert function.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Item is added to tree, and rebalancing happened if it was needed.
*
************************************************************************/
template<typename T>
inline void AVLTree<T>::Insert(const T & in)
{
	bool taller = false;
	BTree<T>::m_root = Insert(in, BTree<T>::m_root, taller);
}

/**********************************************************************
* Purpose: This function basically calls the REAL delete function.
*
* Precondition:
*     Item looking to be deleted is in the tree.
*
* Postcondition:
*      Item is deleted from tree, and rebalancing happened if it was needed.
*
************************************************************************/
template<typename T>
inline void AVLTree<T>::Delete(const T & in)
{
	bool success = false;
	bool shorter = false;
	Delete(in, BTree<T>::m_root, shorter, success);
	if (success == false)
		throw Exception("Data not found!");
}

/**********************************************************************
* Purpose: This is the real Insert function. It inserts item into tree.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Item is added to tree, and rebalancing happened if it was needed.
*
************************************************************************/
template<typename T>
inline TNode<T>*& AVLTree<T>::Insert(const T & in, TNode<T>*& root, bool & taller)
{
	if (root == nullptr)
	{
		//insert at root
		root = new TNode<T>(in);
		taller = true;
	}
	else if (in <= root->m_data)
	{
		root->m_L = Insert(in, root->m_L, taller);
		if (taller)
		{
			//left subtree is taller
			if (root->bal == LH)
				root = LeftBalance(root, taller);
			else if (root->bal == EH)
				root->bal = LH;
			else
			{
				root->bal = EH;
				taller = false;
			}
		}
	}
	else
	{
		//new data >= root data.
		root->m_R = Insert(in, root->m_R, taller);
		if (taller)
		{
			//right subtree is taller.
			if (root->bal == LH)
			{
				root->bal = EH;
				taller = false;
			}
			else if (root->bal == EH)
			{
				root->bal = RH;
			}
			else
				root = RightBalance(root, taller);
		}
	}
	return root;
}

/**********************************************************************
* Purpose: This IS the real delete function.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Item is deleted from tree, and rebalancing happened if it was needed.
*
************************************************************************/
template<typename T>
inline TNode<T>* AVLTree<T>::Delete(const T & out, TNode<T>*& root, bool & shorter, bool & success)
{
	if (root == nullptr)
	{
		shorter = false;
		success = false;
	}
	else if (out < root->m_data)
	{
		root->m_L = Delete(out, root->m_L, shorter, success);
			if (shorter)
				root = DeleteRightBalance(root, shorter);
	}
	else if (out > root->m_data)
	{
		root->m_R = Delete(out, root->m_R, shorter, success);
		if (shorter)
			root = DeleteLeftBalance(root, shorter);
	}
	else
	{
		TNode<T> * deleteNode = root;
		if (root->m_R == nullptr)
		{
			TNode<T> * newRoot = root->m_L;
			success = true;
			shorter = true;
			delete deleteNode;
			deleteNode = nullptr;
			root = newRoot;
		}
		else if (root->m_L == nullptr)
		{
			TNode<T> * newRoot = root->m_R;
			success = true;
			shorter = true;
			delete deleteNode;
			deleteNode = nullptr;
			root = newRoot;
		}
		else
		{
			TNode<T> * exchPtr = root->m_R;
			while (exchPtr->m_L != nullptr)
				exchPtr = exchPtr->m_L;
			root->m_data = exchPtr->m_data;
			root->m_R = Delete(exchPtr->m_data, root->m_R, shorter, success);
			if (shorter)
				root = DeleteRightBalance(root, shorter);
			root = root;
		}
	}
	return root;
}

/**********************************************************************
* Purpose: Left Balance handles when a branch is too left heavy.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Tree is balanced and still keeping it's BST integrity.
*
************************************************************************/
template<typename T>
inline TNode<T>*& AVLTree<T>::LeftBalance(TNode<T>*& root, bool & taller)
{
	TNode<T> * leftTree = root->m_L;
	if (leftTree->bal == LH)
	{
		//case 1, left of left. single rotation right
		root->bal = EH;
		leftTree->bal = EH;
		root = RotateRight(root);
		taller = false;
	}
	else
	{
		TNode<T> * rightTree = leftTree->m_R;
		//adjust balance factors
		if (rightTree)
		{
			if (rightTree->bal == LH)
			{
				root->bal = RH;
				leftTree->bal = EH;
			}
			else if (rightTree->bal == EH)
			{
				leftTree->bal = EH;
			}
			else
			{
				root->bal = EH;
				leftTree->bal = LH;
			}
			rightTree->bal = EH;
			root->m_L = RotateLeft(leftTree);
			root = RotateRight(root);
			taller = false;
		}
	}
	return root;
}

/**********************************************************************
* Purpose: Right Balance handles when a branch is too right heavy.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Tree is balanced and still keeping it's BST integrity.
*
************************************************************************/
template<typename T>
inline TNode<T>*& AVLTree<T>::RightBalance(TNode<T>*& root, bool & taller) //fix for right instead of left.
{
	TNode<T> * rightTree = root->m_R;
	if (rightTree->bal == RH)
	{
		root->bal = EH;
		rightTree->bal = EH;
		root = RotateLeft(root);
		taller = false;
	}
	else
	{
		TNode<T> * leftTree = rightTree->m_L;
		if (leftTree)
		{
			//adjust balance factors
			if (leftTree->bal == RH)
			{
				root->bal = LH;
				rightTree->bal = EH;
			}
			else if (leftTree->bal == EH)
				rightTree->bal = EH;
			else
			{
				root->bal = EH;
				rightTree->bal = RH;
			}
			leftTree->bal = EH;
			root->m_R = RotateRight(rightTree);
			root = RotateLeft(root);
			taller = false;
		}
	}
	return root;
}

/**********************************************************************
* Purpose: Rotates the root to the right.
*
* Precondition:
*     this exists.
*
* Postcondition:
*     Tree rotated right.
*
************************************************************************/
template<typename T>
inline TNode<T>*& AVLTree<T>::RotateRight(TNode<T>*& root)
{
	TNode<T> * tmpPtr = root->m_L;
	root->m_L = tmpPtr->m_R;
	tmpPtr->m_R = root;
	return tmpPtr;
}

/**********************************************************************
* Purpose: Rotates the root to the left.
*
* Precondition:
*     this exists.
*
* Postcondition:
*     Tree rotated left.
*
************************************************************************/
template<typename T>
inline TNode<T>*& AVLTree<T>::RotateLeft(TNode<T>*& root)
{
	TNode<T> * tmpPtr = root->m_R;
	root->m_R = tmpPtr->m_L;
	tmpPtr->m_L = root;
	return tmpPtr;
}

/**********************************************************************
* Purpose: Delete Right Balance handles when a branch is too right heavy
*	after the deletion of a node.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Tree is balanced and still keeping it's BST integrity.
*
************************************************************************/
template<typename T>
inline TNode<T> *& AVLTree<T>::DeleteRightBalance(TNode<T>*& root, bool & shorter)
{
	if (root->bal == LH)
		root->bal = EH;
	else if (root->bal == EH)
	{
		root->bal = RH;
		shorter = false;
	}
	else
	{
		TNode<T> * rightTree = root->m_R;
		if (rightTree)
		{
			if (rightTree->bal == LH)
			{
				TNode<T> * leftTree = rightTree->m_L;
				if (leftTree->bal == LH)
				{
					rightTree->bal = RH;
					root->bal = EH;
				}
				else if (leftTree->bal == EH)
				{
					root->bal = EH;
					rightTree->bal = EH;
				}
				else
				{
					root->bal = LH;
					rightTree->bal = EH;
				}
				leftTree->bal = EH;
				root->m_R = RotateRight(rightTree);
				root = RotateLeft(root);
			}

			else
			{
				if (rightTree->bal != EH)
				{
					root->bal = EH;
					rightTree->bal = EH;
				}
				else
				{
					root->bal = RH;
					rightTree->bal = LH;
					shorter = false;
				}
				root = RotateLeft(root);
			}
		}
	}
	return root;
}

/**********************************************************************
* Purpose: Delete Left Balance handles when a branch is too left heavy
*	after the deletion of a node.
*
* Precondition:
*     this exists.
*
* Postcondition:
*      Tree is balanced and still keeping it's BST integrity.
*
************************************************************************/
template<typename T>
inline TNode<T>*& AVLTree<T>::DeleteLeftBalance(TNode<T>*& root, bool & shorter)
{
	if (root->bal == RH)
		root->bal = EH;
	else if (root->bal == EH)
	{
		root->bal = LH;
		shorter = false;
	}
	else
	{
		TNode<T> * leftTree = root->m_L;
		if (leftTree)
		{
			if (leftTree->bal == RH)
			{
				TNode<T> * rightTree = leftTree->m_R;
				if (rightTree->bal == RH)
				{
					leftTree->bal = LH;
					root->bal = EH;
				}
				else if (rightTree->bal == EH)
				{
					root->bal = EH;
					leftTree->bal = EH;
				}
				else
				{
					root->bal = RH;
					leftTree->bal = EH;
				}
				rightTree->bal = EH;
				root->m_L = RotateLeft(leftTree);
				root = RotateRight(root);
			}
			else
			{
				if (leftTree->bal != EH)
				{
					root->bal = EH;
					leftTree->bal = EH;
				}
				else
				{
					root->bal = LH;
					leftTree->bal = RH;
					shorter = false;
				}
				root = RotateRight(root);
			}
		}
	}
	return root;
}

#endif