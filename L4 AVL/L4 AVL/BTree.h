/************************************************************************
* Class: BTree //abstract base class
*
* Purpose: This class is the abstract base class for all types of trees
*	to inherit from.
*
* Manager functions:
*	All the canonical functions.
*
* Methods:
*	Insert() = 0 Inserts item to tree
*	Delete() = 0 Deletes item from tree
*	Purge() will purge the tree.
*	Height() will return the height of the tree.
*	//traversal functions
*		InOrder()
*		PreOrder()
*		PostOrder()
*		BreadthFirst()
*************************************************************************/
#ifndef BTREE
#define BTREE

#include "TNode.h"
#include "QueueLL.h"

template <typename T>
class BTree
{
public:
	//Canonical
	BTree();
	BTree(const BTree & copy);
	BTree & operator=(const BTree & copy);
	virtual ~BTree();
	//Traversal Functions
	virtual void InOrder(void (visit)(const T & visiting));
	virtual void PreOrder(void (visit)(const T & visiting));
	virtual void PostOrder(void (visit)(const T & visiting));
	virtual void BreadthFirst(void (visit)(const T & visiting));
	//Others
	virtual void Purge();
	virtual void Insert(const T & in) = 0;
	virtual void Delete(const T & in) = 0;
	virtual int  Height(); //Determine the height of the tree.
protected:
	virtual void InOrder(void (visit)(const T & visiting), TNode<T> * root);
	virtual void PreOrder(void (visit)(const T & visiting), TNode<T> * root);
	virtual void PostOrder(void (visit)(const T & visiting), TNode<T> * root);
	virtual void Purge(TNode<T> *& root);
	virtual int  Height(TNode<T> * root);
	TNode<T> * m_root;
};

//default constructor.
template<typename T>
inline BTree<T>::BTree()
	:m_root(nullptr)
{ }

//Copy constructor that really just calls the OP=.
template<typename T>
inline BTree<T>::BTree(const BTree & copy)
	: m_root(nullptr)
{
	*this = copy;
}

//Copies copy tree into itself.
template<typename T>
inline BTree<T> & BTree<T>::operator=(const BTree & copy)
{
	if (this != &copy)
	{
		QueueLL<TNode<T> *> visitQueue;
		TNode<T> * tmp = nullptr;
		if (copy.m_root != nullptr)
		{
			Purge();
			visitQueue.Enqueue(copy.m_root);
			while (!visitQueue.isEmpty())
			{
				tmp = visitQueue.Dequeue();
				if (tmp->m_L != nullptr)
					visitQueue.Enqueue(tmp->m_L);
				if (tmp->m_R != nullptr)
					visitQueue.Enqueue(tmp->m_R);
				Insert(tmp->m_data); //this line is dead somehow.
			}
		}
	}
	return *this;
}

//destructor.
template <typename T>
BTree<T>::~BTree()
{
	BTree<T>::Purge();
}

/**********************************************************************
* Purpose: InOrder Traversal.
*
* Precondition:
*     Tree has data.
*
* Postcondition:
*      Every item in the tree has been passed to visiting in InOrder.
*
************************************************************************/
template<typename T>
inline void BTree<T>::InOrder(void(visit)(const T &visiting), TNode<T>* root)
{
	if (root != nullptr)
	{
		InOrder(visit, root->m_L);
		visit(root->m_data);
		InOrder(visit, root->m_R);
	}
}

/**********************************************************************
* Purpose: PreOrder Traversal.
*
* Precondition:
*     Tree has data.
*
* Postcondition:
*      Every item in the tree has been passed to visiting in PreOrder.
*
************************************************************************/
template<typename T>
inline void BTree<T>::PreOrder(void(visit)(const T &visiting), TNode<T>* root)
{
	if (root != nullptr)
	{
		visit(root->m_data);
		InOrder(visit, root->m_L);
		InOrder(visit, root->m_R);
	}
}

/**********************************************************************
* Purpose: PostOrder Traversal.
*
* Precondition:
*     Tree has data.
*
* Postcondition:
*      Every item in the tree has been passed to visiting in PostOrder.
*
************************************************************************/
template<typename T>
inline void BTree<T>::PostOrder(void(visit)(const T &visiting), TNode<T>* root)
{
	if (root != nullptr)
	{
		visit(root->m_data);
		InOrder(visit, root->m_L);
		InOrder(visit, root->m_R);
	}
}

/**********************************************************************
* Purpose: Fake InOrder Traversal.
*		(Calls the real InOrder, passing in m_root.)
* Precondition:
*     Tree has data.
*
* Postcondition:
*      Every item in the tree has been passed to visiting in InOrder.
*
************************************************************************/
template<typename T>
inline void BTree<T>::InOrder(void(visit)(const T &visiting))
{
	if (visit != nullptr)
		InOrder(visit, m_root);
}

/**********************************************************************
* Purpose: Fake PreOrder Traversal.
*		(Calls the real PreOrder, passing in m_root.)
* Precondition:
*     Tree has data.
*
* Postcondition:
*      Every item in the tree has been passed to visiting in PreOrder.
*
************************************************************************/
template<typename T>
inline void BTree<T>::PreOrder(void(visit)(const T &visiting))
{
	if (visit != nullptr)
		PreOrder(visit, m_root);
}

/**********************************************************************
* Purpose: Fake PostOrder Traversal.
*		(Calls the real PostOrder, passing in m_root.)
* Precondition:
*     Tree has data.
*
* Postcondition:
*      Every item in the tree has been passed to visiting in PostOrder.
*
************************************************************************/
template<typename T>
inline void BTree<T>::PostOrder(void(visit)(const T &visiting))
{
	if (visit != nullptr)
		PostOrder(visit, m_root);
}

/**********************************************************************
* Purpose: Breadth First Traversal.
*
* Precondition:
*     Tree has data.
*
* Postcondition:
*      Every item in the tree has been passed to visiting in BreadFirst.
*
************************************************************************/
template<typename T>
inline void BTree<T>::BreadthFirst(void(visit)(const T &visiting))
{
	QueueLL<TNode<T> *> visitQueue;
	TNode<T> * tmp = nullptr;
	if (m_root != nullptr && visit != nullptr)
	{
		visitQueue.Enqueue(m_root);
		while (!visitQueue.isEmpty())
		{
			tmp = visitQueue.Dequeue();
			if (tmp->m_L != nullptr)
				visitQueue.Enqueue(tmp->m_L);
			if (tmp->m_R != nullptr)
				visitQueue.Enqueue(tmp->m_R);
			visit(tmp->m_data);
		}
	}
}

/**********************************************************************
* Purpose: Fake Purge. Just calls the real Purge passing in m_root.
*
* Precondition:
*     Tree has data.
*
* Postcondition:
*     Tree is deleted.
*
************************************************************************/
template<typename T>
inline void BTree<T>::Purge()
{
	Purge(m_root);
}

/**********************************************************************
* Purpose: Purge. To delete the entire tree.
*
* Precondition:
*     Tree has data.
*
* Postcondition:
*     Tree is deleted.
*
************************************************************************/
template<typename T>
inline void BTree<T>::Purge(TNode<T> *& root)
{
	if (root != nullptr)
	{
		Purge(root->m_L);
		Purge(root->m_R);
		delete root;
		root = nullptr;
	}
}

/**********************************************************************
* Purpose: Height. Returns the height of the tree.
*
* Precondition:
*     Tree has data.
*
* Postcondition:
*     Height of tree is returned.
*
************************************************************************/
template<typename T>
inline int BTree<T>::Height(TNode<T>* root)
{
	int out = 0;
	if (root != nullptr)
	{
		int left = Height(root->m_L);
		int right = Height(root->m_R);
		if (left > right)
			out = left + 1;
		else
			out = right + 1;
	}
	return out;
}

/**********************************************************************
* Purpose: Fake Height. Just calls the real Height passing in m_root.
*
* Precondition:
*     Tree has data. if it doesn't, an exception is thrown.
*
* Postcondition:
*     Height of the tree is returned.
*
************************************************************************/
template<typename T>
inline int BTree<T>::Height()
{
	if (m_root == nullptr)
		throw Exception("No existing tree!");
	return Height(m_root);
}

#endif