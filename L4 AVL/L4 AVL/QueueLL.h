/************************************************************************
* Class: QueueLL.h
*
* Purpose: LinkedList based Queue data structure.
*
* Manager functions:
*  *All canonical functions
*
* Methods:
* Enqueue(T in) Queue's in.
* Dequeue() Dequeue's the first in line.
* Front() Returns reference to first in line.
* Size() Returns size of Queue.
* isEmpty() Returns if queue is empty
*************************************************************************/

#ifndef QUEUELL_H
#define QUEUELL_H

#include "doublelinkedlist.h"
#include "Exception.h"

template <typename T>
class QueueLL
{
public:
	QueueLL();
	~QueueLL();
	QueueLL(const QueueLL & copy);
	QueueLL & operator=(const QueueLL & copy);
	void Enqueue(T in);
	T Dequeue();
	T & Front();
	int Size() const;
	bool isEmpty() const;
private:
	DoubleLinkedList<T> m_queue;
	int m_size;
};

//Regular default constructor.
template<typename T>
inline QueueLL<T>::QueueLL()
	:m_queue(), m_size(0)
{ }

//Regular ol' destructor
template<typename T>
inline QueueLL<T>::~QueueLL()
{
	m_queue.Purge();
	m_size = 0;
}
//Regular ol' copy constructor
template<typename T>
inline QueueLL<T>::QueueLL(const QueueLL & copy)
	:m_queue(copy.m_queue), m_size(copy.m_size)
{ }

//Regular ol' op= to copy data from copy, to this.
template<typename T>
inline QueueLL<T> & QueueLL<T>::operator=(const QueueLL & copy)
{
	if (this != &copy)
	{
		m_queue = copy.m_queue;
		m_size = copy.m_size;
	}
	return *this;
}

/**********************************************************************
* Purpose: Adds data to the queue
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Data is inserted to end of queue.
*
************************************************************************/
template<typename T>
inline void QueueLL<T>::Enqueue(T in)
{
	m_queue.Append(in);
	m_size++;
}

/**********************************************************************
* Purpose: Removes first in line from queue.
*
* Precondition:
*     Data exists.
*
* Postcondition:
*     First of the list is returned/popped out of Queue.
*
************************************************************************/
template<typename T>
inline T QueueLL<T>::Dequeue()
{
	if (isEmpty())
		throw Exception("Queue Underflow Exception");

	T out = m_queue.First();
	m_queue.Extract(out);

	m_size--;
	return out;
}

/**********************************************************************
* Purpose: Returns reference to front of queue.
*
* Precondition:
*     Data exists
*
* Postcondition:
*     Data at front of queue is returned, allowing modification.
*
************************************************************************/
template<typename T>
inline T & QueueLL<T>::Front()
{
	if (isEmpty())
		throw Exception("Queue Underflow Exception");

	return m_queue.First();
}

/**********************************************************************
* Purpose: Returns number of elements in queue.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Number of elements in queue is returned.
*
************************************************************************/
template<typename T>
inline int QueueLL<T>::Size() const
{
	return m_size;
}

/**********************************************************************
* Purpose: Returns true of queue is empty.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns true if queue is empty.
*
************************************************************************/
template<typename T>
inline bool QueueLL<T>::isEmpty() const
{
	return m_size == 0;
}

#endif