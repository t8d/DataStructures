/************************************************************************
* Class: Graph
*
* Purpose: Carry a graph of data. (and edge data)
*
* All canonical.
*
*	Manager:
*
*		InsertVertex() Inserts vertex to graph.
*		RemoveData() Removes vertex from graph.
*		InsertEdge() Inserts edge into graph.
*		RemoveEdge() Removes edge from graph.
*		IsEmpty() Returns true if graph is empty.
*
*	Traversals:
*
*		TraverseDepthFirst() Traversals graph depth-first
*		TraverseBreadthFirst() Traversals graph breadth-first
*
*	Getters:
*
*	GetVL() Returns vertex list.
*	GerVertexCount() Returns vertex count.
*
*	Helpers:
*
*	ResetProcessedState() Reset's all vertex's processed state to false.
*
*************************************************************************/

#ifndef GRAPH_H
#define GRAPH_H
#include <list>
#include "vertex.h"
#include <stack>
#include <queue>

using std::list;
using std::queue;
using std::stack;

template <typename T, typename U>
class Graph
{
public:
	//Canonical
	Graph();
	Graph(const Graph & copy);
	Graph & operator=(const Graph & copy);
	~Graph();

	//Manager
	void InsertVertex(const T & vData);
	void RemoveData(const T & vData);
	void InsertEdge(const T & startVx, const T & destVx, const U & ed, int weight);
	void RemoveEdge(const T & startVx, const T & destVx);
	bool IsEmpty() const;

	//Traversals
	void TraverseDepthFirst(void(*visit)(T vData));
	void TraverseBreadthFirst(void(*visit)(T vData));

	//Getters
	list<Vertex<T, U>> & GetVL();
	int GetVertexCount() const;

	//Helpers
	void ResetProcessedState();
private:
	list<Vertex<T, U>> m_vl; //vertex list.
	int m_vCount;
};

//Default constructor
template<typename T, typename U>
inline Graph<T, U>::Graph()
	:m_vCount(0)
{ }

//Copy constructor
template<typename T, typename U>
inline Graph<T, U>::Graph(const Graph & copy)
	:m_vCount(0)
{
	*this = copy; //passing work to the op=.
}

//OP=
template<typename T, typename U>
inline Graph<T, U> & Graph<T, U>::operator=(const Graph & copy)
{
	if (this != &copy)
	{
		//list<Vertex<T, U>> CopyVL = copy.m_vl;
		m_vl = copy.m_vl;
		m_vCount = copy.m_vCount;
		//go through every vertex in copy's vl, replicating each one's edges.

		for(const Vertex<T, U> & vertex : copy.m_vl)
		{
			for(const Edge<T,U> & edge : vertex.m_el)
			{
				InsertEdge(vertex.m_vd, edge.m_de->m_vd, edge.m_ed, edge.m_weight);
			}
		}

		/*for (typename list<Vertex<T, U>>::iterator iter = copy.m_vl.begin(); iter != copy.m_vl.end(); ++iter)
		{
			for (typename list<Edge<T, U>>::iterator iterE = iter->m_el.begin(); iterE != iter->m_el.end(); ++iterE)
			{
				InsertEdge(iter->m_vd, iterE->m_de->m_vd, iterE->m_ed, iterE->m_weight);
			}
		}*/
	}
	return *this;
}

//Destructor
template<typename T, typename U>
inline Graph<T, U>::~Graph()
{ }

/**********************************************************************
* Purpose: Inserts vertex into graph.
*
* Precondition:
*     vData is of correct type, and has comparison operators working.
*
* Postcondition:
*     Vertex with vData is inserted into graph.
*
************************************************************************/
template<typename T, typename U>
inline void Graph<T, U>::InsertVertex(const T & vData)
{
	bool skip = false;
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); iter != m_vl.end(); ++iter)
	{
		if (iter->m_vd == vData)
			skip = true;
	}
	if (skip == false)
	{
		m_vl.push_back(Vertex<T, U>(vData));
		m_vCount++;
	}
}

/**********************************************************************
* Purpose: Removes vertex from graph.
*
* Precondition:
*     Vertex currently exists in graph.
*
* Postcondition:
*     Removed vertex from graph.
*
************************************************************************/
template<typename T, typename U>
inline void Graph<T, U>::RemoveData(const T & vData)
{
	bool exit = false;
	//Find vData in list.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin();!exit && iter != m_vl.end();)
	{
		if ((*iter) == vData)
		{
			//Remove any edge pointing to vData.
			for (typename list<Vertex<T, U>>::iterator iter2 = m_vl.begin(); iter2 != m_vl.end(); ++iter2)
			{ //iterate through list to iterate through their edges.
				bool deleted = false;
				for (typename list<Edge<T, U>>::iterator iterE = (*iter2).m_el.begin(); !deleted && iterE != (*iter2).m_el.end();)
				{ 
					//find edges pointing to vData.
					if (*(*iterE).m_de == vData)
					{
						(*iter2).m_el.erase(iterE);
						deleted = true;
					}
					else
					{
						++iterE;
					}
				}
			}
			//now delete that vertex.
			m_vl.erase(iter);
			m_vCount--;
			exit = true;
		}
		else
		{
			++iter;
		}
	}
	
	
}

/**********************************************************************
* Purpose: Insert edge between two vertex's in graph.
*
* Precondition:
*     Both end vertex's exist.
*
* Postcondition:
*     Edge is inserted.
*
************************************************************************/
template<typename T, typename U>
inline void Graph<T, U>::InsertEdge(const T & startVx, const T & destVx, const U & ed, int weight)
{
	//I can probably combine a few of these iterators together for efficiency.
	if (startVx != destVx)
	{ 
		bool foundS = false; //found start
		bool foundD = false; //found destination
		bool connected = false; //is connected.
		for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); iter != m_vl.end(); ++iter)
		{ //make sure both vertex's exist.
			if (*iter == startVx)
				foundS = true;
			if (*iter == destVx)
				foundD = true;
		}
		if (foundS && foundD)
		{
			for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); iter != m_vl.end(); ++iter)
			{ //Iterate to find start vertex.
				if (*iter == startVx) //find start vertex.
				{
					for (typename list<Edge<T, U>>::iterator iterE = (*iter).m_el.begin(); iterE != (*iter).m_el.end(); ++iterE)
						if ((*iterE).m_de->m_vd == destVx && (*iterE).m_ed == ed) //determine if it's already connected.
							connected = true;
					for (typename list<Vertex<T, U>>::iterator iter2 = m_vl.begin(); iter2 != m_vl.end() && !connected; ++iter2)
					{  //iterator for destination vertex.
						if ((*iter2) == destVx) //find dest vertex.
						{
							//push edges to both vertexs to eachother.
							(*iter).m_el.push_back(Edge<T, U>(&*iter2, ed, weight));
							(*iter2).m_el.push_back(Edge<T, U>(&*iter, ed, weight));
						}
					}
				}
			}
		}
	}
}

/**********************************************************************
* Purpose: Removes edge from graph.
*
* Precondition:
*     Edge exists in graph.
*
* Postcondition:
*     Removed edge from graph.
*
************************************************************************/
template<typename T, typename U>
inline void Graph<T, U>::RemoveEdge(const T & startVx, const T & destVx)
{
	//iterate to find startvx.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); iter != m_vl.end(); ++iter)
	{
		if ((*iter) == startVx)
		{
			//iterate through Edge
			bool deleted = false;
			for (typename list<Edge<T, U>>::iterator iterE = (*iter).m_el.begin(); !deleted && iterE != (*iter).m_el.end();)
			{
				//find edges pointing to vData.
				if (*(*iterE).m_de == destVx)
				{
					//before erase, remove inside edge backwards.
					bool deleted2 = false;
					for (typename list<Edge<T, U>>::iterator iterE2 = (*iterE).m_de->m_el.begin(); !deleted2 && iterE2 != (*iterE).m_de->m_el.end();)
					{
						if (*(*iterE2).m_de == startVx)
						{
							(*iterE).m_de->m_el.erase(iterE2);
							deleted2 = true;
						}
						else
						{
							++iterE2;
						}
					}
					(*iter).m_el.erase(iterE);
					deleted = true;
				}
				else
				{
					++iterE;
				}
			}
		}
	}
}

/**********************************************************************
* Purpose: Returns true if graph is empty.
*
* Precondition:
*     None
*
* Postcondition:
*     Returns true if graph is empty.
*
************************************************************************/
template<typename T, typename U>
inline bool Graph<T, U>::IsEmpty() const
{
	return m_vCount <= 0;
}

/**********************************************************************
* Purpose: Traverses the graph DepthFirst
*
* Precondition:
*     Graph has vertexes.
*
* Postcondition:
*     Each vertex is visited in DepthFirst order.
*
************************************************************************/
template<typename T, typename U>
inline void Graph<T, U>::TraverseDepthFirst(void(*visit)(T vData))
{
	stack<Vertex<T, U> *> vStack;
	Vertex<T, U> * tempvertex = nullptr;
	vStack.push(&m_vl.front());
	while (!vStack.empty())
	{
		tempvertex = vStack.top();
		vStack.pop();
		tempvertex->m_proccessed = true;
		visit(tempvertex->m_vd);
		//push edges to stack.
		for (typename list<Edge<T, U>>::iterator iterE = tempvertex->m_el.begin(); iterE != tempvertex->m_el.end(); ++iterE)
		{
			if ((*iterE).m_de->m_proccessed == false)
				vStack.push((*iterE).m_de);
		}
	}
	//reset proccessed flags to false.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); m_vl.end() != iter; ++iter)
	{
		(*iter).m_proccessed = false;
	}
}

/**********************************************************************
* Purpose: Traverses the graph BreadthFirst
*
* Precondition:
*     Graph has vertexes.
*
* Postcondition:
*     Each vertex is visited in BreadthFirst order.
*
************************************************************************/
template<typename T, typename U>
inline void Graph<T, U>::TraverseBreadthFirst(void(*visit)(T vData))
{
	queue<Vertex<T, U> *> vQueue;
	Vertex<T, U> * tempvertex = nullptr;
	vQueue.push(&m_vl.front());
	while (!vQueue.empty())
	{
		tempvertex = vQueue.front();
		vQueue.pop();
		tempvertex->m_proccessed = true;
		visit(tempvertex->m_vd);
		//push edges to stack.
		for (typename list<Edge<T, U>>::iterator iterE = tempvertex->m_el.begin(); iterE != tempvertex->m_el.end(); ++iterE)
		{
			if ((*iterE).m_de->m_proccessed == false)
			{
				vQueue.emplace(iterE->m_de);
				iterE->m_de->m_proccessed = true;
			}
		}
	}
	//reset proccessed flags to false.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); m_vl.end() != iter; ++iter)
	{
		(*iter).m_proccessed = false;
	}
}

/**********************************************************************
* Purpose: Returns vertex list.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Vertex list is returned.
*
************************************************************************/
template<typename T, typename U>
inline list<Vertex<T, U>> & Graph<T, U>::GetVL()
{
	return m_vl;
}

/**********************************************************************
* Purpose: Returns vertex count.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns the number of vertex's in graph.
*
************************************************************************/
template<typename T, typename U>
inline int Graph<T, U>::GetVertexCount() const
{
	return m_vCount;
}

/**********************************************************************
* Purpose: Reset Processed State of all vertexes in graph.
*
* Precondition:
*     Graph has vertexes.
*
* Postcondition:
*     Each vertex's processed state is reset back to false.
*
************************************************************************/
template<typename T, typename U>
inline void Graph<T, U>::ResetProcessedState()
{
	//reset proccessed flags to false.
	for (typename list<Vertex<T, U>>::iterator iter = m_vl.begin(); m_vl.end() != iter; ++iter)
	{
		(*iter).m_proccessed = false;
	}
}

#endif
