/**************************************************************
*
* Lab/Assignment: Lab 5 � Dijkstra's Shortest Path
*		-Created by Tommy Miller
*
* Overview:
* This program takes a map as an input file, prompts users for
*	Destanation and Current location, and provides the user
*	with step-by-step instructions on which roads to take
*	with the shortest path.
*
* Input:
*	lab5.txt as the map input file.
*	From location:
*	To location:
*		(repeats those last two until "quit" is found);
*
* Output:
*	Shortest path between the two user-inputed locations. Along
*	with ETA based on 55mph for all roads but i5, and 65mph for
*	i5.
*
************************************************************/

//Includes
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stack>
#include "Graph.h"

//Using's
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::ifstream;
using std::vector;
using std::stack;

//Very helpful struct to keep all information that dijkstra's algorithm produces.
struct VertexData
{
	list<Edge<string, string>> ed;
	Vertex<string, string> begin;
	list<string> pathname;
	list<int> weight;
	string before;
	double ad; //accumulated distance.
	bool processed;
};

//Function declarations.
void print(string);
void ProcessLine(string line, Graph<string, string> & graph);
void GetShortestPath(Graph<string, string> & graph);
void Dijkstra(Graph<string, string> & graph, string startNode, string endNode);
void DisplayFindings(list<VertexData>, string startNode, string endNode);

/**********************************************************************
* Purpose: Read input file, process and insert it into graph, and
*	call function for user input.
*
* Precondition:
*     None. This is the start.
*
* Postcondition:
*     The program exits.
*
************************************************************************/
int main()
{
	Graph<string, string> graph;
	ifstream inFile("lab5.txt");
	if (inFile.is_open())
	{
		while (!inFile.eof())
		{ //separating by line.
			string line;
			std::getline(inFile, line);
			ProcessLine(line, graph);
		}
		graph.TraverseBreadthFirst(print);
		//done processing inFile. Now for user input.
		GetShortestPath(graph);
		inFile.close();
	}
	else
	{
		cout << "Failed to open file. Aborting." << endl;
	}
}

/**********************************************************************
* Purpose: Print an incoming string.
*
* Precondition:
*     str exists.
*
* Postcondition:
*     str is displayed to console.
*
************************************************************************/
void print(string str)
{
	cout << str << endl;
}

/**********************************************************************
* Purpose: Process line of input file.
*
* Precondition:
*     Arguments exist.
*
* Postcondition:
*     Data is inserted into graph.
*
************************************************************************/
void ProcessLine(string line, Graph<string, string> & graph)
{
	//this expects every line to have 3 commas.
	if (!line.empty())
	{
		string startNode;
		string endNode;
		string Path;
		int weight = 0;
		char * context = nullptr;
		char cline[256] = { '\0' };
		strcpy_s(cline, line.c_str());
		char * token = strtok_s(cline, ",\n", &context);
		startNode = token;
		token = strtok_s(nullptr, ",\n", &context);
		endNode = token;
		token = strtok_s(nullptr, ",\n", &context);
		Path = token;
		token = strtok_s(nullptr, ",\n,", &context);
		weight = atoi(token);
		graph.InsertVertex(startNode);
		graph.InsertVertex(endNode);
		graph.InsertEdge(startNode, endNode, Path, weight);
	}
}

/**********************************************************************
* Purpose: Ask user for both destination and source locations,
*	push the work off to Dijkstra's, and display it's findings.
*
* Precondition:
*     Graph has data.
*
* Postcondition:
*     Program is ready to exit.
*
************************************************************************/
void GetShortestPath(Graph<string, string>& graph)
{
	cout << "Map processed. Enter beginning and ending nodes." << endl;
	cout << "Type \"exit\" to exit program" << endl;
	string from;
	string to;
	while (from != "exit")
	{
		cout << "Starting Node: ";
		std::getline(cin, from);
		if (from != "exit")
		{
			cout << "Ending Node: ";
			std::getline(cin, to);
			try
			{
				Dijkstra(graph, from, to);
			}
			catch (char * msg)
			{
				cout << msg << endl;
			}
		}
	}
}

/**********************************************************************
* Purpose: Calculate shortest path in graph.
*
* Precondition:
*     All arguments exist, and startNode & endNode exist in graph.
*
* Postcondition:
*     Shortest path is calculated, called the display, and is done.
*
************************************************************************/
void Dijkstra(Graph<string, string>& graph, string startNode, string endNode)
{
	//init VertexData struct
	list<VertexData> vd; //tobechecked 
	list<VertexData> vdproc; //checked
	VertexData spoonfeed;

	for (Vertex<string, string> & vertex : graph.GetVL())
	{
		spoonfeed.processed = false;
		spoonfeed.begin = vertex;
		for (Edge<string, string> & edge : vertex.GetEL())
		{
			spoonfeed.ed.emplace_back(edge);
			spoonfeed.weight.emplace_back(edge.GetWeight());
			spoonfeed.pathname.emplace_back(edge.GetED());
			spoonfeed.ad = 999999999;
			spoonfeed.before = ""; //check
		}
		vd.emplace_back(spoonfeed);
		//clear spoonfeed for next loop.
		spoonfeed.ed.clear();
		spoonfeed.pathname.clear();
		spoonfeed.weight.clear();
	}
	//2 purposes: 1) end function if start wasn't found, and if it was, set it's ad to 0.
	{ //scoping to remove temp variables.
		bool found = false;
		for (list<VertexData>::iterator iter = vd.begin(); found == false && vd.end() != iter; ++iter)
		{
			if (iter->begin.GetVD() == startNode)
			{
				found = true;
				iter->ad = 0;
			}
		}
		if (found == false)
		{
			throw "Did not find begin vertex.";
		}
	}
	//now to check for the end data to exist.
	{
		bool found = false;
		for (list<VertexData>::iterator iter = vd.begin(); found == false && vd.end() != iter; ++iter)
		{
			if (iter->begin.GetVD() == endNode)
			{
				found = true;
			}
		}
		if (found == false)
		{
			throw "Did not find end vertex.";
		}
	}
	//now for the long (long) loop to finish the path
	bool done = false;
	int smaller = 0;
	while (!done) //while not done
	{
		for (list<VertexData>::iterator iter = vd.begin(); vd.end() != iter; ++iter)
		{
			if (iter->processed == false)
			{
				if (iter->ad < smaller)
				{
					smaller = iter->ad;
				}
			}
		}
		for (list<VertexData>::iterator iter = vd.begin(); vd.end() != iter; ++iter)
		{
			if (iter->ad == smaller)
			{
				vdproc.emplace_back(*iter);
				iter->processed = true;
			}
		}

				for (list<VertexData>::iterator iter = vdproc.begin(); vdproc.end() != iter; ++iter)
		{
			for (list<Edge<string, string>>::iterator iterE = iter->ed.begin(); iterE != iter->ed.end(); ++iterE)
			{
				for (list<VertexData>::iterator iter2 = vd.begin(); vd.end() != iter2; ++iter2)
				{
					if (iter2->begin.GetVD() == iterE->GetDE()->GetVD())
					{
						if (iter2->ad > iter->ad + iterE->GetWeight())
						{
							iter2->ad = (iter->ad + iterE->GetWeight());
							iter2->before = iter->begin.GetVD();
						}
					}
				}
			}
		}
		done = true;
		for (list<VertexData>::iterator iter = vd.begin(); vd.end() != iter; ++iter)
		{
			if (iter->processed == false)
				done = false;
		}
		smaller = 8675309; //Get the reference?
	}
	//vd is currently in backwards order. Reverse it to be able to display. (and into a stack)
	//for (list<VertexData>::iterator iter = vd.end(); vd.begin() != iter; --iter)
	//{
	//	inOrder.push(*(iter));
	//}
	//Display the findings
	DisplayFindings(vd, startNode, endNode);
	//cleanup
}

/**********************************************************************
* Purpose: Output findings from Dijkstra's formula.
*
* Precondition:
*     This was called from Dijkstra, and it didn't screw up.
*
* Postcondition:
*     Shortest path + time is displayed to console.
*
************************************************************************/
void DisplayFindings(list<VertexData> nonOrder, string startNode, string endNode)
{
	cout << "Starting at " << startNode << " traveling to " << endNode << endl;
	cout << "Begin:" << endl;
	stack<VertexData> inOrder;
	string before = endNode;
	int miles = 0;
	double time = 0;
	bool done = false;
	while (!done)
	{
		for (VertexData & iter : nonOrder)
		{ //create stack of path (since it's currently in reverse order)...
			if (iter.begin.GetVD() == before)
			{
				if (before != startNode)
					before = iter.before;
				inOrder.push(iter);
			}
		}
		if (before == startNode)
			done = true;
	}
	while (!inOrder.empty())
	{
		VertexData tmp = inOrder.top();
		inOrder.pop();
		if (tmp.before != "")
		{
			cout << tmp.begin.GetVD() << " from " << tmp.before << endl;
			cout << "\tDistance: " << tmp.ad - miles << " mi." << endl;;
			if (tmp.pathname.back() == "I-5")
			{
				double time = ((tmp.ad / 65) * 60) * 60;
				int hours = 0;
				int minutes = 0;
				while (time >= 60)
				{
					if (time >= 60)
					{
						minutes++;
						time -= 60;
					}
					if (minutes >= 60)
					{
						hours++;
						minutes -= 60;
					}
				}
				cout << "\tETA: \t";
				cout << hours <<" Hours, " << minutes << " Minutes, and " << time << " Seconds.";
			}
			else
			{
				double time = ((tmp.ad / 55) * 60) * 60;
				int hours = 0;
				int minutes = 0;
				while (time >= 60)
				{
					if (time >= 60)
					{
						minutes++;
						time -= 60;
					}
					if (minutes >= 60)
					{
						hours++;
						minutes -= 60;
					}
				}
				cout << "\tETA: \t";
				cout << hours << " Hours, " << minutes << " Minutes, and " << time << " Seconds.";
			}
			cout << endl << endl;
		}
		if (tmp.begin.GetVD() == endNode)
		{
		}
		miles = tmp.ad;
	}
}
