/************************************************************************
* Class: Edge
*
* Purpose: Carry edge data that is being carried by a Vertex.
*
* All canonical.
*	Edge(Vertex, U, weight) //creates edge with all passed in data.
*
*	operator==(Edge) compare the edges as a whole, and return true if match.
*
*	Getters:
*
*	GetDE() returns the Destination Edge.
*	GetWeight() returns weight of this edge.
*	GetED() returns the edge data.
*
*************************************************************************/

#ifndef EDGE_H
#define EDGE_H

template <typename T, typename U>
class Vertex;

template <typename T, typename U>
class Edge
{
public:
	Edge();
	Edge(Vertex<T, U> * de, U edge, int weight);
	Edge(const Edge & copy);
	Edge & operator=(const Edge & copy);
	~Edge();

	bool operator==(const Edge & compare) const;

	template <typename V, typename W>
	friend class Graph;

	//Getters

	Vertex<T, U> * GetDE();
	int GetWeight() const;
	U & GetED();
private:
	Vertex<T, U> * m_de; //destination edge
	int m_weight; //weight.
	U m_ed; //edge data
};

//default constructor
template<typename T, typename U>
inline Edge<T, U>::Edge()
	:m_de(nullptr), m_weight(0)
{ }

//4 arg constructor, putting them all directly into it's private variables.
template<typename T, typename U>
inline Edge<T, U>::Edge(Vertex<T, U>* de, U edge, int weight)
	:m_de(de), m_weight(weight), m_ed(edge)
{ }

//copy constructor.
template<typename T, typename U>
inline Edge<T, U>::Edge(const Edge & copy)
	: m_de(copy.m_de), m_weight(copy.m_weight), m_ed(copy.m_ed)
{ }

//OP=
template<typename T, typename U>
inline Edge<T, U>& Edge<T, U>::operator=(const Edge<T, U>& copy)
{
	if (this != &copy)
	{
		m_de = copy.m_de;
		m_weight = copy.m_weight;
		m_ed = copy.m_ed;
	}
	return *this;
}

//Destructor
template<typename T, typename U>
inline Edge<T, U>::~Edge()
{ }

/**********************************************************************
* Purpose: Compare this edge with compare edge.
*
* Precondition:
*     Both edges's exist.
*
* Postcondition:
*     Returns true if they both carry the same data.
*
************************************************************************/
template<typename T, typename U>
inline bool Edge<T, U>::operator==(const Edge<T, U>& compare) const
{
	return (m_weight == compare.m_weight && m_de == compare.m_de && m_ed == compare.m_ed);
}

/**********************************************************************
* Purpose: Getter for Destination Edge.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns the destination edge pointer.
*
************************************************************************/
template<typename T, typename U>
inline Vertex<T, U>* Edge<T, U>::GetDE()
{
	return m_de;
}

/**********************************************************************
* Purpose: Get Weight of Edge.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns the weight of this edge.
*
************************************************************************/
template<typename T, typename U>
inline int Edge<T, U>::GetWeight() const
{
	return m_weight;
}

/**********************************************************************
* Purpose: Returns the Edge Data.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns this's edge data.
*
************************************************************************/
template<typename T, typename U>
inline U & Edge<T, U>::GetED()
{
	return m_ed;
}

#endif