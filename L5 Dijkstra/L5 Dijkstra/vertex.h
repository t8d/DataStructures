/************************************************************************
* Class: Vertex
*
* Purpose: Carry vertex information (and edges for the vertex) for graph.
*
* All canonical.
*	Vertex(T vd) //creates vertex with vertexdata being base-classed init
*		to m_vd.
*
*	operator==(Vertex) compare the vertex as a whole, and return true if match.
*	operator==(T) compare T with m_vd. Return true if match.
*
*	Getters:
* 
*	GetVD() returns the vertex data.
*	GetEL() returns the edge list.
*	GetProcessed() gets it's processed boolean. (doesn't affect graph).
*	SetProcessed() sets it's processed boolean. (doesn't affect graph).
*
*************************************************************************/


#ifndef Vertex_H
#define Vertex_H
#include "Edge.h"
using std::list;
template <typename T, typename U>
class Vertex
{
public:
	//public (because xmemory) canonical methods
	Vertex();
	Vertex(T vd);
	Vertex(const Vertex & copy);
	Vertex & operator=(const Vertex & copy);
	~Vertex();

	bool operator==(const Vertex & compare) const;
	bool operator==(const T & compare) const;

	template <typename V, typename W>
	friend class Graph;

	//getters
	T & GetVD();
	list<Edge<T, U>> & GetEL();
	bool GetProcessed() const;
	void SetProcessed(); //and one setter
private:
	//data members
	T m_vd; //Vertex data.
	list<Edge<T, U>> m_el;
	bool m_proccessed;
};

//Default constructor.
template <typename T, typename U>
Vertex<T, U>::Vertex()
	:m_proccessed(false)
{ }

//1 arg constructor (setting m_vd to it).
template<typename T, typename U>
inline Vertex<T, U>::Vertex(T vd)
	:m_vd(vd), m_proccessed(false)
{ }

//Copy constructor. (shallow, for good reasons)
template<typename T, typename U>
inline Vertex<T, U>::Vertex(const Vertex & copy)
	:m_vd(copy.m_vd), m_el(copy.m_el), m_proccessed(copy.m_proccessed)
{ }

//Destructor
template <typename T, typename U>
Vertex<T, U>::~Vertex()
{ }

//OP=.
template<typename T, typename U>
inline Vertex<T, U> & Vertex<T, U>::operator=(const Vertex & copy)
{
	if (this != &copy)
	{
		m_vd = copy.m_vd;
		m_proccessed = copy.m_proccessed;
		//m_el = copy.m_el;
	}
	return *this;
}

/**********************************************************************
* Purpose: Compare this with Vertex
*
* Precondition:
*     This and compare exists.
*
* Postcondition:
*     Returns true if they carry the same data.
*
************************************************************************/
template<typename T, typename U>
inline bool Vertex<T, U>::operator==(const Vertex<T, U>& compare) const
{
	return m_vd == compare.m_vd;
}

/**********************************************************************
* Purpose: Compare this with T
*
* Precondition:
*     This and compare exists.
*
* Postcondition:
*     Returns true if they carry the same data.
*
************************************************************************/
template<typename T, typename U>
inline bool Vertex<T, U>::operator==(const T & compare) const
{
	return m_vd == compare;
}

/**********************************************************************
* Purpose: Return Vertex Data.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns m_vd.
*
************************************************************************/
template<typename T, typename U>
inline T & Vertex<T, U>::GetVD()
{
	return m_vd;
}

/**********************************************************************
* Purpose: Return Edge.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns m_el.
*
************************************************************************/
template<typename T, typename U>
inline list<Edge<T, U>> & Vertex<T, U>::GetEL()
{
	return m_el;
}

/**********************************************************************
* Purpose: Returns processed state.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns m_processed.
*
************************************************************************/
template<typename T, typename U>
inline bool Vertex<T, U>::GetProcessed() const
{
	return m_proccessed;
}

/**********************************************************************
* Purpose: Sets Processed State.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     sets m_vd to true. Graph contains a function that will reset all
*		vectors processed state to false. Use that if needed.
*
************************************************************************/
template<typename T, typename U>
inline void Vertex<T, U>::SetProcessed()
{
	m_proccessed = true;
}
#endif
