#ifndef NODE_H
#define NODE_H

/************************************************************************
* Class: Node
*
* Purpose: Carry data type T, a next Node pointer, and a previous Node pointer
* for use of my Doubly LinkedList Class.
*
* Manager functions:
* Node (const T & in) (requires data to carry)
* Node (const Node & copy)
* operator = (const Node & copy)
* ~Node()
*
* Methods:
* void SetData (const T & in)	Sets m_data to in.
* T& GetData()			Retruns m_data.
* void SetNext(Node * in)Sets m_next to in.
* Node * GetNext()		Returns m_next.
* void SetPrev(Node * in)Sets m_prev to in.
* Node * GetPrev()		Returns m_prev.
* ...
*************************************************************************/

template <typename T>
class Node
{
	Node(const T & in);
	Node(Node & copy);
	~Node();
	Node & operator=(const Node & copy);

	T m_data;
	void SetData(const T & in);
	T& GetData();

	Node * m_next;
	void SetNext(Node * in);
	Node * GetNext();

	Node * m_prev;
	void SetPrev(Node * in);
	Node * GetPrev();

	template <typename T>
	friend class DoubleLinkedList;
};

//Required regular constructor.
template <typename T>
Node<T>::Node(const T & in)
	:m_data(in), m_next(nullptr), m_prev(nullptr)
{ }

//Copy Constructor
template <typename T>
Node<T>::Node(Node & copy)
	:m_next(copy.m_next), m_prev(copy.m_prev), m_data(copy.m_data)
{ }

//Destructor
template <typename T>
Node<T>::~Node()
{
	m_next = nullptr;
	m_prev = nullptr;
}

//OP=
template <typename T>
Node<T> & Node<T>::operator=(const Node & copy)
{
	if (this != &copy)
	{
		m_prev = nullptr;
		m_next = nullptr;
		m_data = copy.m_data;
	}

	return *this;
}

//Getter for m_data (type T)
template <typename T>
T & Node<T>::GetData()
{
	return m_data;
}

//Setter for m_data (type T)
template <typename T>
void Node<T>::SetData(const T & in)
{
	m_data = in;
}

//Getter for m_next
template <typename T>
Node<T> * Node<T>::GetNext()
{
	return m_next;
}

//Setter for m_next
template <typename T>
void Node<T>::SetNext(Node * in)
{
	m_next = in;
}

//Getter for m_prev
template <typename T>
Node<T> * Node<T>::GetPrev()
{
	return m_prev;
}

//Setter for m_prev
template <typename T>
void Node<T>::SetPrev(Node* in)
{
	m_prev = in;
}

#endif