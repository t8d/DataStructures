#ifndef DOUBLELINKEDLIST_H
#define DOUBLELINKEDLIST_H

#include "node.h"

//for printfoward and printbackward
#include <iostream>
using std::cout;
using std::endl;

template <typename T>
class DoubleLinkedList
{
public:
	//Canonical Methods
	DoubleLinkedList();
	DoubleLinkedList(DoubleLinkedList & copy);
	~DoubleLinkedList();
	DoubleLinkedList & operator=(DoubleLinkedList & copy);

	//Accessors
	Node<T> * getHead() const;
	Node<T> * getTail() const;

	//Methods for testing
	void PrintForwards() const;
	void PrintBackwards() const;

	//Methods
	bool isEmpty() const;
	const T & First() const;
	const T & Last() const;
	void Prepend(const T & data);
	void Append(const T & data);
	void Purge();
	void Extract(T toDelete);
	void InsertAfter(const T & new_data, const T & existing_data);
	void InsertBefore(const T & new_data, const T & existing_data);


private:
	Node<T> * m_head;
	Node<T> * m_tail;
};

template<typename T>
inline DoubleLinkedList<T>::DoubleLinkedList()
	:m_head(nullptr), m_tail(nullptr)
{ }

template<typename T>
inline DoubleLinkedList<T>::DoubleLinkedList(DoubleLinkedList<T> & copy)
	:m_head(nullptr), m_tail(nullptr)
{
	*this = copy;
}

template<typename T>
inline DoubleLinkedList<T>::~DoubleLinkedList()
{
	Purge();
}

template<typename T>
inline DoubleLinkedList<T> & DoubleLinkedList<T>::operator=(DoubleLinkedList & copy)
{
	if (this != &copy)
	{
		Purge();
		Node<T> * travel = copy.m_head;
		while (travel != nullptr)
		{
			Append(travel->m_data);
			travel = travel->m_next;
		}
	}
	return *this;
}

template<typename T>
inline Node<T> * DoubleLinkedList<T>::getHead() const
{
	return m_head;
}

template<typename T>
inline Node<T> * DoubleLinkedList<T>::getTail() const
{
	return m_tail;
}

template<typename T>
inline void DoubleLinkedList<T>::PrintForwards() const
{
	Node<T> * travel = m_head;
	cout << "Printing contents of linked list in forwards order." << endl;
	while (travel != nullptr)
	{
		cout << travel->m_data << endl;
		travel = travel->m_next;
	}
}

template<typename T>
inline void DoubleLinkedList<T>::PrintBackwards() const
{
	Node<T> * travel = m_tail;
	cout << "Printing contents of linked list in backwards order." << endl;
	while (travel != nullptr)
	{
		cout << travel->m_data << endl;
		travel = travel->m_prev;
	}
}

template<typename T>
inline bool DoubleLinkedList<T>::isEmpty() const
{
	bool Empty = true;
	if (m_head != nullptr)
		Empty = false;
	return Empty;
}

template<typename T>
inline const T & DoubleLinkedList<T>::First() const
{
	if (isEmpty())
		throw "Empty List!";
	return m_head->m_data;
}

template<typename T>
inline const T & DoubleLinkedList<T>::Last() const
{
	if (isEmpty())
		throw "Empty List!";
	return m_tail->m_data;
}

template<typename T>
inline void DoubleLinkedList<T>::Prepend(const T & data)
{
	//Node<T> * temp = new Node<T>(data);
	if (!isEmpty())
	{   
		m_head->m_prev = new Node<T>(data);
		m_head->m_prev->m_next = m_head;
		m_head = m_head->m_prev;
	}
	else //Empty list
		m_head = m_tail = new Node<T>(data);
}

template<typename T>
inline void DoubleLinkedList<T>::Append(const T & data)
{
	Node<T> * temp = new Node<T>(data);
	if (!isEmpty()) 
	{ 
		m_tail->m_next = temp;
		temp->m_prev = m_tail;
		m_tail = temp;
	}
	else //Empty list
		m_head = m_tail = temp;
}

template<typename T>
inline void DoubleLinkedList<T>::Purge()
{
	if (!isEmpty())
	{
		Node<T> * travel = m_head;
		while (travel->m_next != nullptr)
		{
			travel = travel->m_next;
			delete travel->m_prev;
		}
		delete m_tail;
		m_head = nullptr;
		m_tail = nullptr;
	}
}

template<typename T>
inline void DoubleLinkedList<T>::Extract(T toDelete)
{
	if (!isEmpty())
	{
		Node<T> * travel = m_head;
		bool found = false;
		while (travel != nullptr && found == false)
		{
			if (travel->m_data == toDelete)
				found = true;
			else
				travel = travel->m_next;
		}
		if (found)
		{
			if (m_head == m_tail) //if the deleting item is the only item in list
			{
				Purge(); //Just murders the list.
			}
			else if (travel == m_head) //If deleting item is the m_head as well.
			{
				m_head = m_head->m_next;
				m_head->m_prev = nullptr;
				delete travel;
			}
			else if (travel == m_tail) //If deleting item is the m_tail as well.
			{
				m_tail = m_tail->m_prev;
				m_tail->m_next = nullptr;
				delete travel;
			}
			else { //Deleting item is middle of the list somewhere.
				travel->m_prev->m_next = travel->m_next;
				travel->m_next->m_prev = travel->m_prev;
				delete travel;
			}
		}

	}
}

template<typename T>
inline void DoubleLinkedList<T>::InsertAfter(const T & new_data, const T & existing_data)
{
	if (!isEmpty())
	{
		Node<T> * temp = new Node<T>(new_data);
		if ((m_head == m_tail) && (m_head->m_data == existing_data)) //Only thing in list. New item needs to append to it.
		{
			m_tail = temp;
			m_head->m_next = m_tail;
			m_tail->m_prev = m_head;
		}
		else //needs to loop to find where to add.
		{
			if (m_head == m_tail)
			{
				delete temp;
				throw "Only item in list is not the same as passed item to compare against.";
			}
			else
			{
				Node<T> * travel = m_head;
				bool done = false;
				while (travel != nullptr && !done)
				{
					if (travel->m_data == existing_data)
						done = true;
					else
						travel = travel->m_next;
				}
				if (done)
				{
					if (travel != m_tail)
					{
						//Travel -- temp -- travel->m_next
						temp->m_next = travel->m_next;
						temp->m_prev = travel;
						travel->m_next->m_prev = temp;
						travel->m_next = temp;
					}
					else
					{
						//travel -- temp -- nullptr.
						temp->m_prev = travel;
						temp->m_next = nullptr;
						travel->m_next = temp;
						m_tail = temp;
					}

				}
				else
				{
					delete temp;
					throw "No matching item found!";
				}
			}
		}
	}
	else 
	{
		throw "No list, so no data to compare with.";
	}
}

template<typename T>
inline void DoubleLinkedList<T>::InsertBefore(const T & new_data, const T & existing_data)
{

	if (!isEmpty())
	{
		Node<T> * temp = new Node<T>(new_data);
		if ((m_head == m_tail) && (m_head->m_data == existing_data)) //Only thing in list. New item needs to prepend to it.
		{
			m_head = temp;
			m_head->m_next = m_tail;
			m_tail->m_prev = m_head;
		}
		else //needs to loop to find where to add
		{
			if (m_head == m_tail)
			{
				delete temp;
				throw "Only item in list is not the same as passed item to compare against.";
			}
			else
			{
				Node<T> * travel = m_head;
				bool done = false;
				while (travel != nullptr && !done )
				{
					if (travel->m_data == existing_data)
						done = true;
					else
						travel = travel->m_next;
				}
				if (done)
				{
					if (travel != m_head)
					{
						//Travel->m_prev --Temp --Travel
						temp->m_prev = travel->m_prev;
						temp->m_next = travel;
						travel->m_prev->m_next = temp;
						travel->m_prev = temp;
					}
					else
					{
						//Nullptr -- Temp -- Travel
						temp->m_next = travel;
						temp->m_prev = nullptr;
						travel->m_prev = temp;
						m_head = temp;
					}
				}
				else
				{
					delete temp;
					throw "No matching item found!";
				}
			}
		}
	}
	else
	{
		throw "No list, so no data to compare with.";
	}
}

#endif