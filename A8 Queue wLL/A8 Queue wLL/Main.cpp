#include <iostream>
#include <string>
#include "QueueLL.h"

using std::cout;
using std::endl;
using std::string;

int main()
{
	//Creatings stacks testing both constructors
	cout << "Creating Queues" << endl << endl;
	QueueLL<int> iTest;
	QueueLL<string> sTest;

	//Filling all the stacks.
	cout << "Filling Queues" << endl << endl;
	for (int i = 0; i < 100; i++)
	{
		iTest.Enqueue(i);
		sTest.Enqueue("Test100");
	}

	//Testing Front
	cout << "Looking at front values" << endl << endl;
	cout << '\t' << sTest.Front() << endl;
	cout << endl;
	cout << "Changing front values" << endl << endl;
	sTest.Front() = "Hello";
	cout << '\t' << sTest.Front() << endl;
	cout << endl;

	//Testing Size
	if (sTest.Size() == 100)
		cout << "Size test passed" << endl << endl;
	else
		cout << "Size test failed, value= " << sTest.Size() << endl << endl;

	//Testing popping out all values into cout;
	cout << "Popping out all values" << endl << endl;
	for (int i = 0; i < 100; i++)
	{
		sTest.Dequeue();
		iTest.Dequeue();
	}

	//Testing isEmpty
	if (sTest.isEmpty() == true)
		cout << "isEmpty test passed" << endl << endl;
	else
		cout << "isEmpty test failed" << endl << endl;
	//Testing Stack Underflow
	cout << "Did Underflow test pass?" << endl;
	try
	{
		sTest.Dequeue();
	}
	catch (Exception msg)
	{
		cout << "\tYes" << endl << endl;
	}
	//Testing Copy Constructor
	iTest.Enqueue(1);
	QueueLL<int> iTest3(iTest);
	if (iTest3.Front() == 1)
		cout << "Copy Constructor test passed" << endl << endl;
	else
		cout << "Copy Constructor test failed" << endl << endl;
	//Testing OP=
	QueueLL<int> iTest4;
	iTest4.Enqueue(2);
	iTest3 = iTest4;
	if (iTest3.Front() == 2)
		cout << "Operator= test passed" << endl << endl;
	else
		cout << "Operator= test failed" << endl << endl;
	return 0;
}