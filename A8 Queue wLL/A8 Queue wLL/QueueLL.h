#ifndef QUEUELL_H
#define QUEUELL_H

#include "doublelinkedlist.h"
#include "Exception.h"

template <typename T>
class QueueLL
{
public:
	QueueLL();
	~QueueLL();
	QueueLL(const QueueLL & copy);
	QueueLL & operator=(const QueueLL & copy);
	void Enqueue(T in);
	T Dequeue();
	T & Front();
	int Size() const;
	bool isEmpty() const;
private:
	DoubleLinkedList<T> m_queue;
	int m_size;
};

template<typename T>
inline QueueLL<T>::QueueLL()
	:m_queue(), m_size(0)
{ }

template<typename T>
inline QueueLL<T>::~QueueLL()
{
	m_queue.Purge();
	m_size = 0;
}

template<typename T>
inline QueueLL<T>::QueueLL(const QueueLL & copy)
	:m_queue(copy.m_queue), m_size(copy.m_size)
{ }

template<typename T>
inline QueueLL<T> & QueueLL<T>::operator=(const QueueLL & copy)
{
	if (this != &copy)
	{
		m_queue = copy.m_queue;
		m_size = copy.m_size;
	}
	return *this;
}

template<typename T>
inline void QueueLL<T>::Enqueue(T in)
{
	m_queue.Append(in);
	m_size++;
}

template<typename T>
inline T QueueLL<T>::Dequeue()
{
	if (isEmpty())
		throw Exception("Queue Underflow Exception");

	T out = m_queue.First();
	m_queue.Extract(out);

	m_size--;
	return out;
}

template<typename T>
inline T & QueueLL<T>::Front()
{
	if (isEmpty())
		throw Exception("Queue Underflow Exception");

	return m_queue.First();
}

template<typename T>
inline int QueueLL<T>::Size() const
{
	return m_size;
}

template<typename T>
inline bool QueueLL<T>::isEmpty() const
{
	return m_size == 0;
}

#endif