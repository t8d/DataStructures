#include <iostream>
#include <string>
#include "QueueA.h"

using std::cout;
using std::endl;
using std::string;

int main()
{
	//Creatings stacks testing both constructors
	cout << "Creating Queues" << endl << endl;
	QueueA<int> iTest;
	QueueA<int> iTest2(200);
	QueueA<string> sTest;
	QueueA<string> sTest2(200);

	//Filling all the stacks.
	cout << "Filling Queues" << endl << endl;
	for (int i = 0; i < 100; i++)
	{
		iTest.Enqueue(i);
		sTest.Enqueue("Test100");
	}

	for (int i = 0; i < 200; i++)
	{
		iTest2.Enqueue(i);
		sTest2.Enqueue("Test200");
	}

	//Testing Front
	cout << "Looking at front values" << endl << endl;
	cout << '\t' << sTest.Front() << endl;
	cout << '\t' << sTest2.Front() << endl;
	cout << endl;
	cout << "Changing front values" << endl << endl;
	sTest.Front() = "Hello";
	sTest2.Front() = "Again!";
	cout << '\t' << sTest.Front() << endl;
	cout << '\t' << sTest2.Front() << endl;
	cout << endl;

	//Testing Size
	if (sTest2.Size() == 200)
		cout << "Size test passed" << endl << endl;
	else
		cout << "Size test failed, value= " << sTest2.Size() << endl << endl;

	//Testing Stack OverFlow
	cout << "Did Overflow test pass?" << endl;
	try
	{
		sTest2.Enqueue("Hi");
	}
	catch (Exception msg)
	{
		cout << "\tYes" << endl << endl;
	}

	//Testing popping out all values into cout;
	cout << "Popping out all values" << endl << endl;
	for (int i = 0; i < 100; i++)
	{
		sTest.Dequeue();
		iTest.Dequeue();
	}
	for (int i = 0; i < 200; i++)
	{
		sTest2.Dequeue();
		iTest2.Dequeue();
	}

	//Testing isEmpty
	if (sTest2.isEmpty() == true)
		cout << "isEmpty test passed" << endl << endl;
	else
		cout << "isEmpty test failed" << endl << endl;
	//Testing Stack Underflow
	cout << "Did Underflow test pass?" << endl;
	try
	{
		sTest2.Dequeue();
	}
	catch (Exception msg)
	{
		cout << "\tYes" << endl << endl;
	}
	//Testing Copy Constructor
	iTest.Enqueue(1);
	QueueA<int> iTest3(iTest);
	if (iTest3.Front() == 1)
		cout << "Copy Constructor test passed" << endl << endl;
	else
		cout << "Copy Constructor test failed" << endl << endl;
	//Testing OP=
	QueueA<int> iTest4;
	iTest4.Enqueue(2);
	iTest3 = iTest4;
	if (iTest3.Front() == 2)
		cout << "Operator= test passed" << endl << endl;
	else
		cout << "Operator= test failed" << endl << endl;
	return 0;
}