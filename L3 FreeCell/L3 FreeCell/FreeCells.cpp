#include "FreeCells.h"

//Default constructor? That has 4 free cells? Like in FreeCells? Cool!
FreeCells::FreeCells()
	:m_cells(4)
{ }

//Destructor man. Not a superhero.
FreeCells::~FreeCells()
{ }

//Copy constructor that makes a copy.
FreeCells::FreeCells(const FreeCells & copy)
	:m_cells(copy.m_cells)
{ }

//Op= that op='s.
FreeCells & FreeCells::operator=(const FreeCells & copy)
{
	if (this != &copy)
	{
		m_cells = copy.m_cells;
	}
	return *this;
}

/**********************************************************************
* Purpose: Insert into FreeCell at Index
*
* Precondition: this exists.
*
* Postcondition: Insert's card into FreeCell at Index
*
************************************************************************/
bool FreeCells::Insert(Card in, int index)
{
	bool returns = true;
	if (m_cells[index].IsEmptyCard())
	{
		m_cells[index] = in;
	}
	else
		returns = false;

	return returns;
}

/**********************************************************************
* Purpose: Retrieves FreeCell at Index
*
* Precondition: this exists.
*
* Postcondition: Retrieved FreeCell at Index
*
************************************************************************/
Card FreeCells::Retrieve(int col)
{
	Card output; //defaults to empty card.
	if (!m_cells[col].IsEmptyCard())
	{
		output = m_cells[col];
	}
	return output;
}

/**********************************************************************
* Purpose: Removes FreeCell at Index
*
* Precondition: this exists.
*
* Postcondition: Removed FreeCell at Index
*
************************************************************************/
void FreeCells::Remove(int col)
{
	if (!m_cells[col].IsEmptyCard())
	{
		m_cells[col] = Card(); //Replaces it with empty card.
	}
}

/**********************************************************************
* Purpose: Draws the FreeCell section of the game.
*
* Precondition: this exists.
*
* Postcondition: All relevent information is displayed to screen so user
*	knows what's going on.
*
************************************************************************/
void FreeCells::DrawFree(COORD & cursor, bool isSelected, int selectCol)
{
	for (int i = 0; i < 4; i++)
	{
		if (selectCol == i && isSelected)
			m_cells[i].DrawCard(cursor, true);
		else
			m_cells[i].DrawCard(cursor, false);
	}
}

/**********************************************************************
* Purpose: Returns the number of free freecells
*
* Precondition: this exists.
*
* Postcondition: The number of free freecells is returned.
*
************************************************************************/
int FreeCells::NumOfFreeCells()
{
	int output = 0;
	for (int i = 0; i < 4; i++)
	{
		if (m_cells[i].IsEmptyCard())
			++output;
	}
	return output;
}

