/************************************************************************
* Class: PlayArea.h
*
* Purpose: Carry and operate on the PlayArea Cells (for FreeCell)
*
* Manager functions:
*  *All canonical functions
*
* Methods:
*	DrawPlay(stuff) Draws the PlayArea for the game.
*	FillPlay() //initializes Play with new FreeCell Game.
*	GetStack(int row, int col) Returns the stack at said column, from row
*		downwards.
*	CanGetStack(int row, int col) Returns true if the stack is valid for pickup
*	RemoveStack(int row, int col) Removes stack at said col, from row downward.
*	PlaceStack(StackLL<Card> instack, int col) Places stack on top of said col.
*	SizeOfStack(int col) Returns how many cards the requested column has.
*	NumOfEmptyStacks() Returns the number of empty stacks.
*************************************************************************/

#ifndef PLAYAREA_H
#define PLAYAREA_H

#include "StackLL.h"
#include "Array.h"
#include "Deck.h"

class PlayArea
{
public:
	PlayArea();
	~PlayArea();
	PlayArea(const PlayArea & copy);
	PlayArea & operator=(const PlayArea & copy);
	void DrawPlay(COORD & cursor, bool selectInHere, int selectRow, int selectCol) const;
	void FillPlay();
	StackLL<Card> GetStack(int row, int col);
	bool CanGetStack(int row, int col);
	void RemoveStack(int row, int col);
	bool PlaceStack(StackLL<Card> inStack, int col);
	int SizeOfStack(int col);
	int NumOfEmptyStacks();
private:
	Array<StackLL<Card>> m_PA;
};

#endif