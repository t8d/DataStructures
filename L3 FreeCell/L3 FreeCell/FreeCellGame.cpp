#include "FreeCellGame.h"
#include <iostream>
#include <windows.h>
#include <conio.h>

//Defines for key codes
#define KSPACE 0x20

using std::cout;
using std::endl;

//A really long Default C'TOR? Maybe? Na...
FreeCellGame::FreeCellGame()
	:m_home(), m_free(), m_play(), m_exit(false), m_hasSelection(false),
	m_selectInHome(false), m_selectInFree(true), m_selectInPlay(false),
	m_selectionRow(0), m_selectionCol(0), m_toMoveRow(0), m_toMoveCol(0),
	m_toMoveInHome(false), m_toMoveInFree(false), m_toMoveInPlay(false)
{ }

//Destructor that's MUCH smaller than what you see above this.
FreeCellGame::~FreeCellGame()
{ }

//Easy ol Copy Constructor. (maybe kinda annoying)
FreeCellGame::FreeCellGame(const FreeCellGame & copy)
	:m_home(copy.m_home), m_free(copy.m_free), m_play(copy.m_play),
	m_exit(copy.m_exit), m_hasSelection(copy.m_hasSelection),
	m_selectInHome(copy.m_selectInHome), m_selectInFree(copy.m_selectInFree),
	m_selectInPlay(copy.m_selectInPlay), m_selectionRow(copy.m_selectionRow),
	m_selectionCol(copy.m_selectionCol), m_toMoveRow(copy.m_toMoveRow), 
	m_toMoveCol(copy.m_toMoveCol), m_toMoveInHome(copy.m_toMoveInHome),
	m_toMoveInFree(copy.m_toMoveInFree),m_toMoveInPlay(copy.m_toMoveInPlay)
{ }

//Annoying OP= to make a copy of game instance. If you'd ever WANT to..?
FreeCellGame & FreeCellGame::operator=(const FreeCellGame & copy)
{
	if (this != &copy)
	{
		m_home = copy.m_home;
		m_free = copy.m_free;
		m_play = copy.m_play;
		m_exit = copy.m_exit;
		m_hasSelection = copy.m_hasSelection;
		m_selectInHome = copy.m_selectInHome;
		m_selectInFree = copy.m_selectInFree;
		m_selectInPlay = copy.m_selectInPlay;
		m_selectionRow = copy.m_selectionRow;
		m_selectionCol = copy.m_selectionCol;
		m_toMoveRow = copy.m_toMoveRow;
		m_toMoveCol = copy.m_toMoveCol;
		m_toMoveInHome = copy.m_toMoveInHome;
		m_toMoveInFree = copy.m_toMoveInFree;
		m_toMoveInPlay = copy.m_toMoveInPlay;
	}
	return *this;
}

/**********************************************************************
* Purpose: Start the FreeCell Game
*
* Precondition: this exists.
*
* Postcondition: The game is started. When done, prompt to play again.
*
************************************************************************/
void FreeCellGame::Start()
{
	system("mode 120, 50");
	bool restart = true;
	while (restart)
	{
		COORD cursor;
		m_play.FillPlay();
		Game(cursor);
		ChangeColor(1);
		cout << "Restart? (y/n)" << endl;
		cursor.Y++;
		char r = _getch();
		while (r != 'y' && r != 'n')
		{
			cursor.X = 0;
			cursor.Y = 48;
			Cursor(cursor);
			cout << "Please type 'y' or 'n'" << endl;
			r = _getch();
		}
		switch(r)
		{
		case 'y':
			m_home = HomeCells();
			m_free = FreeCells();
			m_play = PlayArea();
			m_exit = false;
			system("cls");
			break;
		case 'n':
			restart = false;
			break;
		}
	}
}

/**********************************************************************
* Purpose: Loop the Refresh and Prompt functions until the game is won or
*	the exit flag was flagged.
*
* Precondition: this exists.
*
* Postcondition: This game instance is over.
*
************************************************************************/
void FreeCellGame::Game(COORD & cursor)
{

	while (m_exit == false && m_home.WonGame() == false)
	{
		Refresh(cursor);
		Prompt();
	}
	if (m_home.WonGame() == true)
	{
		cout << "You Win!" << endl;
	}
}

/**********************************************************************
* Purpose: Redraws all information to console.
*
* Precondition: this exists, and data is in good condition.
*
* Postcondition: The current game's cards are displayed, along with
*	the selector.
*
************************************************************************/
void FreeCellGame::Refresh(COORD & cursor)
{
	cursor.X = 0;
	cursor.Y = 0;
	Cursor(cursor);
	ChangeColor(2);
	cout << "-------------------------------------------------------Free Cell-------------------------------------------------------" << endl;
	cout << "----------------Free-------------------------------------------------------------------------------Home----------------" << endl;
	cout << endl << endl << endl << endl << endl << endl << endl;
	cout << "-----------------------------------------------------------------------------------------------------------------------" << endl;

	cursor.X = 0;
	cursor.Y = 2;
	Cursor(cursor);

	m_free.DrawFree(cursor, m_selectInFree, m_selectionCol);
	cursor.X = 82;
	Cursor(cursor);
	m_home.DrawHome(cursor, m_selectInHome, m_selectionCol - 4);
	cursor.X = 0;
	cursor.Y = 11;
	Cursor(cursor);
	m_play.DrawPlay(cursor, m_selectInPlay, m_selectionRow, m_selectionCol);


	cursor.X = 0;
	cursor.Y = 47;
	Cursor(cursor);
	ChangeColor(1);
	if (m_hasSelection)
		cout << "You have a selection. Move around with WASD and space again to attempt the move." << endl;
	else
		cout << "Move around with WASD, Select/Action/Unselect with space, exit with c.          " << endl;
}

/**********************************************************************
* Purpose: Really long prompt function that prompts user for command
*	and does a action based on it. (or no action if logical).
*
* Precondition: Game is in good shape.
*
* Postcondition: Action is completed based on command.
*
************************************************************************/
void FreeCellGame::Prompt()
{
	int command = 0;
	switch (command = _getch())
	{
	case 'w': //up
		if (m_selectInFree || m_selectInHome)
			;
		else if (m_selectInPlay)
		{
			if (m_selectionRow == 0)
			{
				if (m_selectionCol < 4)
					m_selectInFree = true;
				else
					m_selectInHome = true;
				m_selectInPlay = false;
			}
			else   
			{
				m_selectionRow--;
			}
		}
		break;
	case 's': //down
		if (m_selectInFree || m_selectInHome)
		{
			m_selectInFree = false;
			m_selectInHome = false;
			m_selectInPlay = true;
		}
		else
			if (m_selectInPlay && m_selectionRow < m_play.SizeOfStack(m_selectionCol) - 1)
				m_selectionRow++; 
		break;
	case 'a': //left
		if (m_selectionCol > 0)
		{
			if (m_selectInPlay && m_selectionCol >= 1)
			{
				if (m_play.SizeOfStack(m_selectionCol - 1) > m_selectionRow)
					m_selectionCol--;
				else
				{
					
					if (m_selectionRow > 0)
						m_selectionRow = m_play.SizeOfStack(m_selectionCol - 1) - 1;
					else
						m_selectionRow = 0; //workarounds
					m_selectionCol--;
				}
				if (m_selectionRow < 0)
					m_selectionRow = 0;
			}
			else
			{
				m_selectionCol--;
				if (m_selectInHome && m_selectionCol == 3)
				{
					m_selectInHome = false;
					m_selectInFree = true;
				}
			}
		}
		break;
	case 'd': //right
		if (m_selectionCol < 7)
		{
			if (m_selectInPlay && m_selectionCol <= 6)
			{
				if (m_play.SizeOfStack(m_selectionCol + 1) > m_selectionRow)
					m_selectionCol++;
				else
				{
					if (m_selectionCol < 7)
						m_selectionRow = m_play.SizeOfStack(m_selectionCol + 1) - 1;
					else
						m_selectionRow = 0; //work arounds
					m_selectionCol++;
				}
				if (m_selectionRow < 0)
					m_selectionRow = 0;
			}
			else
			{
				m_selectionCol++;
				if (m_selectInFree && m_selectionCol == 4)
				{
					m_selectInHome = true;
					m_selectInFree = false;
				}
			}
		}
		break;
 	case KSPACE:
		if (m_hasSelection)
		{ //action
			if ((m_selectionCol != m_toMoveCol || m_selectionRow != m_toMoveRow) || m_selectInHome != m_toMoveInHome
				|| m_selectInFree != m_toMoveInFree || m_selectInPlay != m_toMoveInPlay)
			{
				if (m_selectInHome)
				{
					if (m_toMoveInPlay)
					{
						if (m_play.GetStack(m_toMoveRow, m_toMoveCol).Size() == 1)
						{
							StackLL<Card> tmp = m_play.GetStack(m_toMoveRow, m_toMoveCol);
							bool worked = m_home.Insert(tmp.Pop());
							if (worked)
								m_play.RemoveStack(m_toMoveRow, m_toMoveCol);
						}
					}
					else if (m_toMoveInFree)
					{
						Card tmp = m_free.Retrieve(m_toMoveCol);
						bool worked = m_home.Insert(tmp);
						if (worked)
							m_free.Remove(m_toMoveCol);
					}
				}
				if (m_selectInFree)
				{
					if (m_toMoveInPlay)
					{
						if (m_play.GetStack(m_toMoveRow, m_toMoveCol).Size() == 1)
						{
							StackLL<Card> tmp = m_play.GetStack(m_toMoveRow, m_toMoveCol);
							bool worked = m_free.Insert(tmp.Pop(), m_selectionCol);
							if (worked)
								m_play.RemoveStack(m_toMoveRow, m_toMoveCol);
						}
					}
				}
				if (m_selectInPlay)
				{
					if (m_toMoveInPlay)
					{
						if (m_play.CanGetStack(m_toMoveRow, m_toMoveCol))
						{
							if (canSuperMove())
							{
								StackLL<Card> tmp = m_play.GetStack(m_toMoveRow, m_toMoveCol);
								bool worked = m_play.PlaceStack(tmp, m_selectionCol);
								if (worked)
									m_play.RemoveStack(m_toMoveRow, m_toMoveCol);
							}
						}
					}
					else if (m_toMoveInFree)
					{
						StackLL<Card> tmp;
						tmp.Push(m_free.Retrieve(m_toMoveCol));
						if (!tmp.Peek().IsEmptyCard())
						{
							bool worked = m_play.PlaceStack(tmp, m_selectionCol);
							if (worked)
								m_free.Remove(m_toMoveCol);
						}
					}
				}
			}

			//after action cleanup.
			m_hasSelection = false;
			m_toMoveInHome = false;
			m_toMoveInFree = false;
			m_toMoveInPlay = false;
			system("cls");
		}
		else
		{ //select.
			if (m_selectInHome)
				m_toMoveInHome = true;
			else if (m_selectInFree)
				m_toMoveInFree = true;
			else
				m_toMoveInPlay = true;
			m_hasSelection = true;
			m_toMoveCol = m_selectionCol;
			m_toMoveRow = m_selectionRow;
		}
		break;
	case 'c': //exit
		m_exit = true;
		break;
	case 'h': //quick move to home
		if (m_selectInFree)
		{
			Card tmp = m_free.Retrieve(m_selectionCol);
			bool worked = m_home.Insert(tmp);
			if (worked)
				m_free.Remove(m_selectionCol);
		}
		else if (m_selectInPlay)
		{
			if (m_play.GetStack(m_selectionRow, m_selectionCol).Size() == 1)
			{
				StackLL<Card> tmp = m_play.GetStack(m_selectionRow, m_selectionCol);
				bool worked = m_home.Insert(tmp.Pop());
				if (worked)
				{
					m_play.RemoveStack(m_selectionRow--, m_selectionCol);
					if (m_selectionRow < 0)
						m_selectionRow = 0;
				}
			}
		}
		system("cls");
		break;
	case 'f': //quick move to free
		if (m_selectInPlay)
		{
			if (m_play.GetStack(m_selectionRow, m_selectionCol).Size() == 1)
			{
				StackLL<Card> tmp = m_play.GetStack(m_selectionRow, m_selectionCol);
				bool worked = false;
				for (int i = 0; i < 4 && worked == false; i++)
				{
					worked = m_free.Insert(tmp.Peek(), i);
					if (worked)
					{
						m_play.RemoveStack(m_selectionRow--, m_selectionCol);
						if (m_selectionRow < 0)
							m_selectionRow = 0;
					}
				}
			}
		}
		system("cls");
		break;
	}
}


/***************************************************
* Purpose: Changes current color to passed number
*	index.
*
* Precondition: None.
*
* Postcondition: Sets console's current curser's
*	color to passed index's choice.
*
* Index:
* 1) Red
* 2) Green
* 3) Blue
* 4) Red + Green
* 5) Red + Blue
* 6) Green + Blue
* 0) White
***************************************************/
void FreeCellGame::ChangeColor(int number)
{
	HANDLE hStdout = nullptr;
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	switch (number)
	{
	case 1: //Red
		SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
		break;
	case 2: //Green
		SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		break;
	case 3: //Blue
		SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		break;
	case 4: //Red + Green
		SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		break;
	case 5: //Red + Blue
		SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		break;
	case 6: //Green + Blue
		SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		break;
	case 0: //White
		SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
		break;
	default:
		break;
	}
}

/**********************************************************************
* Purpose: Quick function to ask console to change cursor to "in"'s coords.
*
* Precondition: COORDS are inside of range of console.
*
* Postcondition: Either the coords are changed, or nothing happens, and returns
*	false.
*
************************************************************************/
bool FreeCellGame::Cursor(COORD & in)
{
	HANDLE hStdout = nullptr;

	bool success = false;

	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if (SetConsoleCursorPosition(hStdout, in))
		success = true;
	return success;
}

/**********************************************************************
* Purpose: Determines if player can do this supermove.
*
* Precondition: Data is in good shape, and this was called after 
*	CanGetStack returns true.
*
* Postcondition: Returned true if game logic allows for supermove.
*
************************************************************************/
bool FreeCellGame::canSuperMove()
{
	bool returns = true;
	
	StackLL<Card> testSubject = m_play.GetStack(m_toMoveRow, m_toMoveCol);
	int numOfEmptyStacks = m_play.NumOfEmptyStacks();
	int maxCards = m_free.NumOfFreeCells() + 1;

	//if the stack your moving into is one of the empty stacks, dont include it.
	if (m_play.GetStack(m_selectionRow, m_selectionCol).isEmpty())
		maxCards--;

	for (int i = 0; i < numOfEmptyStacks; i++)
	{
		maxCards *= 2;
	}
	
	if (testSubject.Size() > maxCards)
		returns = false;
	return returns;
}
