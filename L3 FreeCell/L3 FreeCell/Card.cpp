﻿#include "Card.h"
#include <iostream>
#include <io.h>
#include <fcntl.h>

using std::cout;
using std::endl;
using std::wcout;

//Not any fancy constructor. Default suit and number means nothing since it's an empty card.
Card::Card()
	:m_suit(Spades), m_number(Ace), m_EmptyCard(true)
{ }

//1 arg constructor. It inits the card with requested Suit and Number.
Card::Card(Suit in, CardNumber num)
	:m_suit(in), m_number(num), m_EmptyCard(false)
{ }

//Easiest destructor in the world.
Card::~Card()
{ }

//Just a regular copy constructor.
Card::Card(const Card & copy)
	:m_suit(copy.m_suit), m_number(copy.m_number), m_EmptyCard(copy.m_EmptyCard)
{ }

//An op= that does it's job of copying a Card into itself.
Card & Card::operator=(const Card & copy)
{
	if (this != &copy)
	{
		m_suit = copy.m_suit;
		m_number = copy.m_number;
		m_EmptyCard = copy.m_EmptyCard;
	}
	return *this;
}

/**********************************************************************
* Purpose: Returns true if both cards in context are the same.
*
* Precondition: This exists.
*
* Postcondition: Returns true if both cards in context are the same.
*
************************************************************************/
bool Card::operator==(const Card & compare) const
{
	bool returns = true;
	if (m_EmptyCard != compare.m_EmptyCard)
		returns = false;
	else if (m_suit != compare.m_suit)
		returns = false;
	else if (m_number != compare.m_number)
		returns = false;
	return returns;
}

/**********************************************************************
* Purpose: Returns this card's suit.
*
* Precondition: This exists.
*
* Postcondition: Returns this card's suit.
*
************************************************************************/
Suit Card::GetSuit() const
{
	return m_suit;
}

/**********************************************************************
* Purpose: Returns this card's number.
*
* Precondition: This exists.
*
* Postcondition: Returns this card's number.
*
************************************************************************/
CardNumber Card::GetNum() const
{
	return m_number;
}

/**********************************************************************
* Purpose: Returns true if this is an empty card.
*
* Precondition: This exists.
*
* Postcondition: Returns true if this is an empty card.
*
************************************************************************/
bool Card::IsEmptyCard() const
{
	return m_EmptyCard;
}

/**********************************************************************
* Purpose: Sets this card's suit to requested.
*
* Precondition: This exists.
*
* Postcondition: Sets this card's suit to requested.
*
************************************************************************/
void Card::SetSuit(Suit in)
{
	m_suit = in;
	m_EmptyCard = false;
}

/**********************************************************************
* Purpose: Sets this card's number to requested.
*
* Precondition: This exists.
*
* Postcondition: Sets this card's number to requested.
*
************************************************************************/
void Card::SetNum(CardNumber in)
{
	m_number = in;
	m_EmptyCard = false;
}

/**********************************************************************
* Purpose: Sets this card's empty state to requested.
*
* Precondition: This exists.
*
* Postcondition: Sets this card's empty state to requested.
*
************************************************************************/
void Card::SetEmptyCard(bool state)
{
	m_EmptyCard = state;
}

/**********************************************************************
* Purpose: Draws this card.
*
* Precondition: This exists.
*
* Postcondition: The card plus all known information is displayed to screen.
*
************************************************************************/
void Card::DrawCard(COORD & in, bool isSelected) const
{
	_setmode(_fileno(stdout), _O_U16TEXT);
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if (m_EmptyCard)
	{
		if (isSelected)
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		else
			SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
		wcout << " _____ ";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << "|     |";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << "|     |";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << "|     |";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << "|     |";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << "|     |";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << L" ‾‾‾‾‾";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
	}
	else
	{
		wchar_t * suit = nullptr;
		wchar_t * num = nullptr;

		switch (GetSuit())
		{
		case Hearts:
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
			suit = L"\u2665";
			break;
		case Diamonds:
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
			suit = L"\u2666";
			break;
		case Clubs:
			SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
			suit = L"\u2663";
			break;
		case Spades:
			SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
			suit = L"\u2660";
			break;
		}

		switch (GetNum())
		{
		case Ace:
			num = L"A";
			break;
		case Two:
			num = L"2";
			break;
		case Three:
			num = L"3";
			break;
		case Four:
			num = L"4";
			break;
		case Five:
			num = L"5";
			break;
		case Six:
			num = L"6";
			break;
		case Seven:
			num = L"7";
			break;
		case Eight:
			num = L"8";
			break;
		case Nine:
			num = L"9";
			break;
		case Ten:
			num = L"10";
			break;
		case Jack:
			num = L"J";
			break;
		case Queen:
			num = L"Q";
			break;
		case King:
			num = L"K";
			break;
		}

		if (isSelected)
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);

		SetConsoleCursorPosition(hStdout, in);
		wcout << " _____ ";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		//Top row of card.
		if (GetNum() == Ten)
			wcout << "|" << num << "  " << suit << "|";
		else
			wcout << "|" << num << "   " << suit << "|";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << "|     |";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		if (GetNum() == Ten)
			wcout << "| " << num << "  |";
		else
			wcout << "|  " << num << "  |";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << "|     |";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		if (GetNum() == Ten)
			wcout << "|" << suit << "  " << num << "|";
		else
			wcout << "|" << suit << "   " << num << "|";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);
		wcout << L" ‾‾‾‾‾";
		in.Y++;
		SetConsoleCursorPosition(hStdout, in);

	}

	in.Y -= 7;
	in.X += 10;
	SetConsoleCursorPosition(hStdout, in);

	_setmode(_fileno(stdout), _O_TEXT);
}
