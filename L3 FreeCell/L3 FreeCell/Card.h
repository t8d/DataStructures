/************************************************************************
* Class: Card.h
*
* Purpose: Card class, to be able to carry information like Suit, Number,
*	Draw function, and a little helpful boolean to be able to draw a fake
*   (emtpy) card.
*
* Manager functions:
*  *All canonical functions
*   Card(Suit in, CardNumber num)
*		Constructs cards setting attributes to passed values.
*
* Methods:
*	operator== (determines if passed card is equal to this card.
*	GetSuit() returns the card's suit.
*	GetNum() returns the card's num.
*	IsEmptyCard() returns if the card has been modified from it's
*		default values (that used the default constructor).
*	SetSuit(Suit in) Setter for the card's suit.
*	SetNum(CardNumber in) Setter for the card's number.
*	SetEmptyCard() //if, for some reason, you have the need for this..
*	DrawCard() Nice, convienent, draw card function. Supports drawing
*		empty card.
*************************************************************************/

#ifndef CARD_H
#define CARD_H

#include "Suit.h"
#include "EnumCardNumbers.h"
#include <windows.h>

class Card
{
public:
	Card(); 
	Card(Suit in, CardNumber num);
	~Card();
	Card(const Card & copy);
	Card & operator=(const Card & copy);
	bool operator==(const Card & compare) const;
	Suit GetSuit() const;
	CardNumber GetNum() const;
	bool IsEmptyCard() const;
	void SetSuit(Suit in); 
	void SetNum(CardNumber in); 
	void SetEmptyCard(bool state);
	void DrawCard(COORD & in, bool isSelected) const;


private:
	Suit m_suit;
	CardNumber m_number;
	bool m_EmptyCard;
};

#endif