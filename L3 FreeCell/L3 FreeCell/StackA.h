/************************************************************************
* Class: StackA.h
*
* Purpose: Array based Stack data structure.
*
* Manager functions:
*  *All canonical functions
*   StackA(int Size)
*		Constructor initializing size to passed value.
*
* Methods:
* Push(T in) Pushes in into stack.
* Pop() Pops T from the stack.
* Peek() Returns a copy of the end of stack.
* Size() Returns size of stack.
* isEmpty() Returns if stack is empty
* isFull() Returns if stack is full
*************************************************************************/

#ifndef STACKA_H
#define STACKA_H

#include "Array.h"

template <typename T>
class StackA
{
public:
	StackA();
	StackA(int stackSize);
	StackA(const StackA & copy);
	~StackA();
	StackA & operator=(const StackA & copy);
	void Push(T pushed);
	T Pop();
	T Peek();
	int Size() const;
	bool isEmpty() const;
	bool isFull() const;

private:
	Array<T> m_stack;
	int m_size;
	int m_pointer;
};

//Easy ol default constructor, with default size of 100
template<typename T>
inline StackA<T>::StackA()
	:m_stack(100), m_size(100), m_pointer(0)
{ }

//1 arg constructor for custom size of queue.
template<typename T>
inline StackA<T>::StackA(int stackSize)
	:m_stack(stackSize), m_size(stackSize), m_pointer(0)
{ }

//Copy contructor.. Easy as pie.
template<typename T>
inline StackA<T>::StackA(const StackA & copy)
	:m_stack(copy.m_stack), m_size(copy.m_size), m_pointer(copy.m_pointer)
{ }

//INSANE destructor..
template<typename T>
inline StackA<T>::~StackA()
{
	m_size = 0;
	m_stack.setLength(0);
	m_pointer = 0;
}

//Operator= is OP? Naaa.. It just copies data.
template<typename T>
StackA<T> & StackA<T>::operator=(const StackA & copy)
{
	m_size = copy.m_size;
	m_stack = copy.m_stack;
	m_pointer = copy.m_pointer;
	return *this;
}

/**********************************************************************
* Purpose: Push data to stack.
*
* Precondition: This exists, and stack is not full
*     
* Postcondition: Data is pushed to stack.
*      
************************************************************************/
template<typename T>
inline void StackA<T>::Push(T pushed)
{
	if (m_pointer < m_size)
		m_stack[m_pointer++] = pushed;
	else
		throw Exception("Stack Overflow Exception");
}

/**********************************************************************
* Purpose: Pop data from stack.
*
* Precondition: Stack has data,
*
* Postcondition: Data is popped from stack.
*
************************************************************************/
template<typename T>
inline T StackA<T>::Pop()
{
	T * out = nullptr;
	if (m_pointer > 0)
		out = &m_stack[--m_pointer];
	else
		throw Exception("Stack Underflow Exception");
	return *out;
}

/**********************************************************************
* Purpose: Peek data from stack.
*
* Precondition: Stack has data
*
* Postcondition: Copy of data is coppied from top of stack.
*
************************************************************************/
template<typename T>
inline T StackA<T>::Peek()
{
	T * out = nullptr;
	if (m_pointer > 0)
		out = &m_stack[m_pointer - 1];
	else
		throw Exception("Stack Underflow Exception");
	return *out;
}

/**********************************************************************
* Purpose: Returns size of Stack
*
* Precondition: This exists.
*
* Postcondition: Size of stack is returned.
*
************************************************************************/
template<typename T>
inline int StackA<T>::Size() const
{
	return m_pointer;
}

/**********************************************************************
* Purpose: Returns true if Stack is empty.
*
* Precondition: This exists.
*
* Postcondition: Returns true if Stack is empty.
*
************************************************************************/
template<typename T>
inline bool StackA<T>::isEmpty() const
{
	return m_pointer == 0;
}

/**********************************************************************
* Purpose: Returns true if Stack is full.
*
* Precondition: This exists.
*
* Postcondition: Returns true if Stack is full.
*
************************************************************************/
template<typename T>
inline bool StackA<T>::isFull() const
{
	return m_pointer == m_size;
}

#endif