#include "HomeCells.h"

//I guess this isn't the smallest default constructor out there, but it's close!
HomeCells::HomeCells()
	:m_home(4)
{ }

//Small destructor. (too small? IT MUST BE SMALLER!)
HomeCells::~HomeCells()
{ }

//Copy constructor.. IT COPIES. and CONSTRUCTS!! MADNESS
HomeCells::HomeCells(const HomeCells & copy)
	:m_home(copy.m_home)
{ }

//I will OP= YOU!
HomeCells & HomeCells::operator=(const HomeCells & copy)
{
	if (this != &copy)
	{
		m_home = copy.m_home;
	}
	return *this;
}

//I promise this wont hurt. (pun intended)
/**********************************************************************
* Purpose: Insert into HomeCells
*
* Precondition: this exists.
*
* Postcondition: If the card can fit, it gets pushed onto it's stack, and
*	returns true.
*
************************************************************************/
bool HomeCells::Insert(Card in)
{
	bool returns = true;
	switch (in.GetSuit())
	{
	case Hearts:
		if (m_home[0].isEmpty() && in.GetNum() == Ace)
			m_home[0].Push(in);
		else if (m_home[0].isEmpty())
			returns = false;
		else if (m_home[0].Peek().GetNum() + 1 == in.GetNum())
			m_home[0].Push(in);
		else
			returns = false;
		break;
	case Clubs:
		if (m_home[1].isEmpty() && in.GetNum() == Ace)
			m_home[1].Push(in);
		else if (m_home[1].isEmpty())
			returns = false;
		else if (m_home[1].Peek().GetNum() + 1 == in.GetNum())
			m_home[1].Push(in);
		else
			returns = false;
		break;
	case Diamonds:
		if (m_home[2].isEmpty() && in.GetNum() == Ace)
			m_home[2].Push(in);
		else if (m_home[2].isEmpty())
			returns = false;
		else if (m_home[2].Peek().GetNum() + 1 == in.GetNum())
			m_home[2].Push(in);
		else
			returns = false;
		break;
	case Spades:
		if (m_home[3].isEmpty() && in.GetNum() == Ace)
			m_home[3].Push(in);
		else if (m_home[3].isEmpty())
			returns = false;
		else if (m_home[3].Peek().GetNum() + 1 == in.GetNum() || in.GetNum() == Ace)
			m_home[3].Push(in);
		else
			returns = false;
		break;
	}
	return returns;
}

/**********************************************************************
* Purpose: Returns true if you WON THE LOTTERY(Or game.. same thing)
*
* Precondition: this exists.
*
* Postcondition: If you won the game, this returned true, else, false.
*
************************************************************************/
bool HomeCells::WonGame()
{
	bool returns = true;
	if (!m_home[0].isEmpty() && !m_home[1].isEmpty() && !m_home[2].isEmpty() && !m_home[3].isEmpty())
	{
		if (m_home[0].Peek().GetNum() != King)
			returns = false;

		if (m_home[1].Peek().GetNum() != King)
			returns = false;

		if (m_home[2].Peek().GetNum() != King)
			returns = false;

		if (m_home[3].Peek().GetNum() != King)
			returns = false;
	}
	else
		returns = false;
	return returns;
}

/**********************************************************************
* Purpose: Draws the HomeCells section to Console.
*
* Precondition: this exists.
*
* Postcondition: Displays all relevant information to console.
*
************************************************************************/
void HomeCells::DrawHome(COORD & cursor, bool isSelected, int selectCol)
{
	for (int i = 0; i < 4; i++)
	{
		if (m_home[i].isEmpty())
		{
			Card fake;
			if (isSelected && selectCol == i)
			{
				fake.DrawCard(cursor, true);
			}
			else
				fake.DrawCard(cursor, false);
		}
		else
		{
			if(isSelected && selectCol == i)
			{
				m_home[i].Peek().DrawCard(cursor, true);
			}
			else
			{
				m_home[i].Peek().DrawCard(cursor, false);
			}
		}

	}
}
