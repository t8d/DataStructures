/************************************************************************
* Class: FreeCells.h
*
* Purpose: Carry and operate on the Free Cells (for FreeCell)
*
* Manager functions:
*  *All canonical functions
*	
* Methods:
*	Insert(Card in, int index) Attempts to insert card to said position.
*	Retrieve(int col) Returns card at index.
*	Remove(int col) Removes card at index
*	DrawFree(stuff) Draw's the FreeCells section for game.
*	NumOfFreeCells() Returns how many empty/free cells that currently exist.
*************************************************************************/

#ifndef FREECELLS_H
#define FREECELLS_H

#include "Array.h"
#include "Card.h"

class FreeCells
{
public:
	FreeCells();
	~FreeCells();
	FreeCells(const FreeCells & copy);
	FreeCells & operator=(const FreeCells & copy);
	bool Insert(Card in, int index);
	Card Retrieve(int col);
	void Remove(int col);
	void DrawFree(COORD & cursor, bool isSelected, int selectCol);
	int NumOfFreeCells();
	
private:
	Array<Card> m_cells;
};

#endif