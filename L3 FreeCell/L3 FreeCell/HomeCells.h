/************************************************************************
* Class: HomeCells.h
*
* Purpose: Carry and operate on the Home Cells (stacks) (for FreeCell)
*
* Manager functions:
*  *All canonical functions
*
* Methods:
*	Insert(Card in) Attempts to insert card to said position.
*	WonGame() returns true if the game is won. (by all home cells being kings)
*	DrawHome(stuff) Draw's the HomeCells section for game.
*************************************************************************/

#ifndef HOMECELLS_H
#define HOMECELLS_H

#include "StackA.h"
#include "Card.h"

class HomeCells
{
public:
	HomeCells();
	~HomeCells();
	HomeCells(const HomeCells & copy);
	HomeCells & operator=(const HomeCells & copy);
	bool Insert(Card in);
	bool WonGame();
	void DrawHome(COORD & cursor, bool isSelected, int selectCol);
private:
	Array<StackA<Card>> m_home;
};

#endif