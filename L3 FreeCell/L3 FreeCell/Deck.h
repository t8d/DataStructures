/************************************************************************
* Class: Deck.h
*
* Purpose: Create a deck of cards, allow client to shuffle them, and draw
*	cards out of deck.
*
* Manager functions:
*  *All canonical functions
*
* Methods:
*	Shuffle() shuffles the deck.
*	PopOffTop() pop's off the top card from the deck.
*************************************************************************/

#ifndef DECK_H
#define DECK_H

#include "Card.h"
#include "Array.h"

class Deck
{
public:
	Deck();
	~Deck();
	Deck(const Deck & copy);
	Deck & operator=(const Deck & copy);
	void Shuffle();
	Card PopOffTop();
private:
	Array<Card> m_deck;
	int m_pointer;
};

#endif
