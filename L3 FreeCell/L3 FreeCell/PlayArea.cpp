#include "PlayArea.h"

const int NUMOFSTACKS = 8;

//I will default constructor you! (also, look up if you have questions here.)
PlayArea::PlayArea()
	:m_PA(NUMOFSTACKS)
{ }

//destructor.. Not a deconstructor.
PlayArea::~PlayArea()
{ }

//Copy constructor, not an op= (It calls another copy constructor!).
PlayArea::PlayArea(const PlayArea & copy)
	:m_PA(copy.m_PA)
{ }

//OP=, not a copy constructor. (an OP= that calls another OP=!)
PlayArea & PlayArea::operator=(const PlayArea & copy)
{
	if (this != &copy)
	{
		m_PA = copy.m_PA;
	}
	return *this;
}

/**********************************************************************
* Purpose: Draw's Play Area to console
*
* Precondition: this exists.
*
* Postcondition: Displays PlayArea to Console.. With all the informtion
*	you'd want from this.
*
************************************************************************/
void PlayArea::DrawPlay(COORD & cursor, bool selectInHere, int selectRow, int selectCol) const
{
	HANDLE hStdout = nullptr;
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

	Array<StackLL<Card>> temp_play_area = m_PA;
	Array<StackLL<Card>> inverse_play_area(8);
	for (int i = 0; i < 8; i++)
	{
		for (int y = 0; !temp_play_area[i].isEmpty(); y++)
		{
			inverse_play_area[i].Push(temp_play_area[i].Pop());
		}

	}
	int tmpy = cursor.Y;
	int y = 0;
	for (int i = 0; i < 8; i++)
	{
		cursor.X += 3;
		cursor.Y = tmpy;
		int tmpx = cursor.X;
		SetConsoleCursorPosition(hStdout, cursor);
		for (y = 0; !inverse_play_area[i].isEmpty(); y++)
		{
			if (selectInHere && i == selectCol && y == selectRow)
				inverse_play_area[i].Pop().DrawCard(cursor, true);
			else
				inverse_play_area[i].Pop().DrawCard(cursor, false);
			cursor.Y += 2;
			cursor.X = tmpx;
			SetConsoleCursorPosition(hStdout, cursor);
		}
		if (y == 0)
		{
			Card blank;
			if (selectInHere && i == selectCol)
				blank.DrawCard(cursor, true);
			else
				blank.DrawCard(cursor, false);
			cursor.Y += 2;
			cursor.X = tmpx;
			SetConsoleCursorPosition(hStdout, cursor);
		}
		cursor.X += 12;
		SetConsoleCursorPosition(hStdout, cursor);
	}

}

/**********************************************************************
* Purpose: Initializes PlayArea to a new game state.
*
* Precondition: this exists.
*
* Postcondition: PlayArea is filled with cards for a game of FreeCell
*	to start.
*
*	NOTE: You can comment out the shuffle to get a sorted deck in game.
************************************************************************/
void PlayArea::FillPlay()
{
	Deck cards;
	cards.Shuffle(); //this is that shuffle.
	int y = 0;
	for (int i = 0; i < 52; i++, y++)
	{
		if (y >= NUMOFSTACKS)
			y = 0;
		m_PA[y].Push(cards.PopOffTop());
		
	}
}

/**********************************************************************
* Purpose: Returns the stack at col, from row downward.
*
* Precondition: this exists.
*
* Postcondition: Returned the stack at col, from row downward.
*
************************************************************************/
StackLL<Card> PlayArea::GetStack(int row, int col)
{
	StackLL<Card> copy = m_PA[col];
	StackLL<Card> output;
	
	for (int i = 0; !copy.isEmpty() && i < m_PA[col].Size() - row; i++)
		output.Push(copy.Pop());
	StackLL<Card> inverseOutput;
	while (!output.isEmpty())
		inverseOutput.Push(output.Pop());
	return inverseOutput;
}

/**********************************************************************
* Purpose: Returns true if game logic allows pickup of stack.
*
* Precondition: this exists.
*
* Postcondition: Returned true if game logic allows pickup of stack,
*	else, false.
*
************************************************************************/
bool PlayArea::CanGetStack(int row, int col)
{
	bool returns = true;
	int toPop = SizeOfStack(col) - row;
	if (!m_PA[col].isEmpty() || m_PA[col].Size() == 1) //it will just return true if isEmpty, or just 1 card.
	{
		StackLL<Card> copy = m_PA[col];

		Card prevCard = copy.Pop();
		toPop--; //just poped one..
		for (int i = 0; !copy.isEmpty() && returns == true && i < toPop; i++)
		{
			Card nextCard = copy.Pop();
			if (prevCard.GetSuit() == Hearts || prevCard.GetSuit() == Diamonds)
			{
				if (nextCard.GetSuit() == Spades || nextCard.GetSuit() == Clubs)
				{
					if (prevCard.GetNum() + 1 != nextCard.GetNum())
						returns = false;
				}
				else
					returns = false;
			}
			else if (prevCard.GetSuit() == Spades || prevCard.GetSuit() == Clubs)
			{
				if (nextCard.GetSuit() == Hearts || nextCard.GetSuit() == Diamonds)
				{
					if (prevCard.GetNum() + 1 != nextCard.GetNum())
						returns = false;
				}
				else
					returns = false;
			}
			prevCard = nextCard;
		}
	}
	return returns;
}

/**********************************************************************
* Purpose: Removes stack at col, from row downward.
*
* Precondition: this exists. (You need to use CanGetStack to make sure
*	this operation is alright.)
*
* Postcondition: Removed the stack at col, from row downward.
*
************************************************************************/
void PlayArea::RemoveStack(int row, int col)
{
	int toPop = SizeOfStack(col) - row;
	for (int i = 0; i < toPop; i++)
	{
		m_PA[col].Pop();
	}
}

/**********************************************************************
* Purpose: Places inStack on top of stack at col.
*
* Precondition: this exists.
*
* Postcondition: Placed inStack on top of stack at col.
*
************************************************************************/
bool PlayArea::PlaceStack(StackLL<Card> inStack, int col)
{
	//FreeCellGame class will have to check if the supermove can handle the operation.
	bool returns = true; //returns true if operation completed successfully.
	//Check if beginning of inStack can fit on top of end of current col.
	StackLL<Card> CopyOfInStack = inStack;
	StackLL<Card> InverseInStack;
	while (!CopyOfInStack.isEmpty())
		InverseInStack.Push(CopyOfInStack.Pop());
	if (m_PA[col].isEmpty())
	{
		//put it in.
		while (!InverseInStack.isEmpty())
			m_PA[col].Push(InverseInStack.Pop());
	}
	else
	{

		//if it can fit by suit match
		if (InverseInStack.Peek().GetSuit() == Hearts)
		{
			if (m_PA[col].Peek().GetSuit() == Spades || m_PA[col].Peek().GetSuit() == Clubs)
			{
				//Check numbers
				if (m_PA[col].Peek().GetNum() == InverseInStack.Peek().GetNum() + 1)
				{
					//put it in.
					while (!InverseInStack.isEmpty())
						m_PA[col].Push(InverseInStack.Pop());
				}
				else
				{
					returns = false;
				}
			}
			else
			{
				returns = false;
			}
		}
		else if (InverseInStack.Peek().GetSuit() == Diamonds)
		{
			if (m_PA[col].Peek().GetSuit() == Spades || m_PA[col].Peek().GetSuit() == Clubs)
			{
				//Check numbers
				if (m_PA[col].Peek().GetNum() == InverseInStack.Peek().GetNum() + 1)
				{
					//put it in.
					while (!InverseInStack.isEmpty())
						m_PA[col].Push(InverseInStack.Pop());
				}
				else
				{
					returns = false;
				}
			}
			else
			{
				returns = false;
			}
		}
		else if (InverseInStack.Peek().GetSuit() == Clubs)
		{
			if (m_PA[col].Peek().GetSuit() == Hearts || m_PA[col].Peek().GetSuit() == Diamonds)
			{
				//Check numbers
				if (m_PA[col].Peek().GetNum() == InverseInStack.Peek().GetNum() + 1)
				{
					//put it in.
					while (!InverseInStack.isEmpty())
						m_PA[col].Push(InverseInStack.Pop());
				}
				else
				{
					returns = false;
				}
			}
			else
			{
				returns = false;
			}
		}
		else if (InverseInStack.Peek().GetSuit() == Spades)
		{
			if (m_PA[col].Peek().GetSuit() == Hearts || m_PA[col].Peek().GetSuit() == Diamonds)
			{
				//Check numbers
				if (m_PA[col].Peek().GetNum() == InverseInStack.Peek().GetNum() + 1)
				{
					//put it in.
					while (!InverseInStack.isEmpty())
						m_PA[col].Push(InverseInStack.Pop());
				}
				else
				{
					returns = false;
				}
			}
			else
			{
				returns = false;
			}
		}
	}
	return returns;
}

/**********************************************************************
* Purpose: Returns the size of the stack at col.
*
* Precondition: this exists.
*
* Postcondition: Returned the size of the stack at col.
*
************************************************************************/
int PlayArea::SizeOfStack(int col)
{
	return m_PA[col].Size();
}

/**********************************************************************
* Purpose: Returns the number of empty stacks.
*
* Precondition: this exists.
*
* Postcondition: Returned the number of empty stacks.
*
************************************************************************/
int PlayArea::NumOfEmptyStacks()
{
	int output = 0;
	for (int i = 0; i < NUMOFSTACKS; i++)
	{
		if (m_PA[i].isEmpty())
			++output;
	}
	return output;
}
