/************************************************************************
* Class: FreeCellGame.h
*
* Purpose: Control the game of FreeCell.
*
* Manager functions:
*  *All canonical functions
*
* Methods:
*	Public:
*		Start() starts the game. (allows for restart here)
*	Rest of the methods are private.
*
*	Game() Handles the game instance.
*	Refresh() redisplays current information to screen.
*	Prompt() Awaits and handles user input commands.
*	ChangeColor(int num) stolen from my Minesweeper, easy way to change
*		console color.
*	Cursor(COORD & in) Asks console to set coords to requested coordinates
*	canSuperMove() returns true if the situation allows for supermove.
*************************************************************************/

#ifndef FREECELLGAME_H
#define FREECELLGAME_H

#include "PlayArea.h"
#include "HomeCells.h"
#include "FreeCells.h"
#include "Deck.h"

class FreeCellGame
{
public:
	FreeCellGame();
	~FreeCellGame();
	FreeCellGame(const FreeCellGame & copy);
	FreeCellGame & operator=(const FreeCellGame & copy);
	void Start();
private:
	void Game(COORD & cursor);
	void Refresh(COORD & cursor); //refresh screen.
	void Prompt();
	static void ChangeColor(int number);
	static bool Cursor(COORD & in);
	bool canSuperMove();
	HomeCells m_home;
	FreeCells m_free;
	PlayArea m_play;
	bool m_exit;
	bool m_hasSelection;
	bool m_selectInHome;
	bool m_selectInFree;
	bool m_selectInPlay;
	int m_selectionRow;
	int m_selectionCol;
	int m_toMoveRow;
	int m_toMoveCol;
	bool m_toMoveInHome;
	bool m_toMoveInFree;
	bool m_toMoveInPlay;
};

#endif