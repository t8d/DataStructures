/************************************************************************
* Class: StackLL.h
*
* Purpose: LinkedList based Stack data structure.
*
* Manager functions:
*  *All canonical functions
*
* Methods:
* Push(T in) Pushes in into stack.
* Pop() Pops T from the stack.
* Peek() Returns a copy of the end of stack.
* Size() Returns size of stack.
* isEmpty() Returns if stack is empty
*************************************************************************/

#ifndef StackLL_H
#define StackLL_H

#include "doublelinkedlist.h"
#include "Exception.h"

template <typename T>
class StackLL
{
public:
	StackLL();
	StackLL(const StackLL & copy);
	~StackLL();
	StackLL & operator=(const StackLL & copy);
	void Push(T pushed);
	T Pop();
	T Peek();
	int Size() const;
	bool isEmpty() const;

private:
	DoubleLinkedList<T> m_stack;
	int m_pointer;
};

//Easy ol default constructor.
template<typename T>
inline StackLL<T>::StackLL()
	:m_pointer(0)
{ }

//Copy constructor.. Easy as pie.
template<typename T>
inline StackLL<T>::StackLL(const StackLL & copy)
	:m_stack(copy.m_stack), m_pointer(copy.m_pointer)
{ }

//THIS MAD destructor.
template<typename T>
inline StackLL<T>::~StackLL()
{
	m_stack.Purge();
	m_pointer = 0;
}

//Operator= is OP? Naaa.. It just copies data.
template<typename T>
StackLL<T> & StackLL<T>::operator=(const StackLL & copy)
{
	m_stack = copy.m_stack;
	m_pointer = copy.m_pointer;
	return *this;
}

/**********************************************************************
* Purpose: Push data to stack.
*
* Precondition: This exists
*
* Postcondition: Data is pushed to stack.
*
************************************************************************/

template<typename T>
inline void StackLL<T>::Push(T pushed)
{
		//m_stack[m_pointer++] = pushed;
	m_stack.Append(pushed);
	m_pointer++;
}

/**********************************************************************
* Purpose: Pop data from stack.
*
* Precondition: Stack has data
*
* Postcondition: Data is popped from stack.
*
************************************************************************/
template<typename T>
inline T StackLL<T>::Pop()
{
	T out;
	if (m_pointer > 0)
	{
		out = m_stack.Last();
		m_stack.Extract(out);
		m_pointer--;
	}
	else
		throw Exception("Stack Underflow Exception");
	return out;
}

/**********************************************************************
* Purpose: Peek data from stack.
*
* Precondition: Stack has data
*
* Postcondition: Copy of data is coppied from top of stack.
*
************************************************************************/
template<typename T>
inline T StackLL<T>::Peek()
{
	T out;
	if (m_pointer > 0)
		out = m_stack.Last();
	else
		throw Exception("Stack Underflow Exception");
	return out;
}

/**********************************************************************
* Purpose: Returns size of Stack
*
* Precondition: This exists.
*
* Postcondition: Size of stack is returned.
*
************************************************************************/
template<typename T>
inline int StackLL<T>::Size() const
{
	return m_pointer;
}

/**********************************************************************
* Purpose: Returns true if Stack is empty.
*
* Precondition: This exists.
*
* Postcondition: Returns true if Stack is empty.
*
************************************************************************/
template<typename T>
inline bool StackLL<T>::isEmpty() const
{
	return m_pointer == 0;
}

#endif