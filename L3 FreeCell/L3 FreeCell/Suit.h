/************************************************************************
* Enumeration: Suit
*
* Purpose: Enumeration for the 4 different types of suit's in a deck
*	of cards.
*
*************************************************************************/

#ifndef SUIT_H
#define SUIT_H

enum Suit
{
	Hearts,
	Diamonds,
	Clubs,
	Spades
};

#endif