/*************************************************************
* Lab: Lab 3 � FreeCell
* Overview: Play a game of FreeCell.
* Input: WASD keys to move, 'c' to exit, 'f' for quick move to
*	free, 'h' for quick move to home, and space for
*	select/unselect/action.
* Output:
*	The game.
************************************************************/

#include "FreeCellGame.h"
#include <iostream>

using std::cout;
using std::endl;

//I create the game instance.. and start it.. That's it. :P
int main()
{
	FreeCellGame game;
	game.Start();
	return 0;
}