/************************************************************************
* Class: QueueA.h
*
* Purpose: Array based Queue data structure.
*
* Manager functions:
*  *All canonical functions
*   QueueA(int Size)
*		Constructor initializing size to passed value.
*		
* Methods:
* Enqueue(T in) Queue's in.
* Dequeue() Dequeue's the first in line.
* Front() Returns reference to first in line.
* Size() Returns size of Queue.
* isEmpty() Returns if queue is empty
* isFull() Returns if queue is full
*************************************************************************/

#ifndef QUEUEA_H
#define QUEUEA_H

#include "Array.h"

template <typename T>
class QueueA
{
public:
	QueueA();
	QueueA(int size);
	~QueueA();
	QueueA(const QueueA & copy);
	QueueA & operator=(const QueueA & copy);
	void Enqueue(T in);
	T Dequeue();
	T & Front();
	int Size() const;
	bool isEmpty() const;
	bool isFull() const;
private:
	Array<T> m_queue;
	int m_size;
	int m_maxSize;
	int m_beginning;
	int m_end;
};

//Regular default constructor.. only thing special, default size is 100.
template<typename T>
inline QueueA<T>::QueueA()
	:m_queue(100), m_size(0), m_maxSize(100), m_beginning(0), m_end(0)
{ }

//1 arg constructor allowing custom size of array.
template<typename T>
inline QueueA<T>::QueueA(int size)
	:m_queue(size), m_size(0), m_maxSize(size), m_beginning(0), m_end(0)
{ }

//Regular ol' destructor
template<typename T>
inline QueueA<T>::~QueueA()
{
	m_queue.setLength(0);
	m_size = 0;
	m_beginning = 0;
	m_end = 0;
	m_maxSize = 0;
}

//Regular ol' copy constructor
template<typename T>
inline QueueA<T>::QueueA(const QueueA & copy)
	:m_queue(copy.m_queue), m_size(copy.m_size), m_maxSize(copy.m_maxSize), m_beginning(copy.m_beginning), m_end(copy.m_end)
{ }

//Regular ol' op= to copy data from copy, to this.
template<typename T>
inline QueueA<T> & QueueA<T>::operator=(const QueueA & copy)
{
	if (this != &copy)
	{
		m_queue = copy.m_queue;
		m_beginning = copy.m_beginning;
		m_end = copy.m_end;
		m_size = copy.m_size;
		m_maxSize = copy.m_maxSize;
	}
	return *this;
}

/**********************************************************************
* Purpose: Adds data to the queue
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Data is inserted to end of queue.
*
************************************************************************/
template<typename T>
inline void QueueA<T>::Enqueue(T in)
{
	if (isFull())
		throw Exception("Queue Overflow Exception");
	m_size++;

	m_queue[m_end++] = in;
	if (m_end >= m_maxSize)
		m_end = 0;

}

/**********************************************************************
* Purpose: Removes first in line from queue.
*
* Precondition:
*     Data exists.
*
* Postcondition:
*     First of the list is returned/popped out of Queue.
*
************************************************************************/
template<typename T>
inline T QueueA<T>::Dequeue()
{
	if (isEmpty())
		throw Exception("Queue Underflow Exception");
	T * out = &m_queue[m_beginning++];
	if (m_beginning >= m_maxSize)
		m_beginning = 0;
	
	m_size--;
	return *out;
}

/**********************************************************************
* Purpose: Returns reference to front of queue.
*
* Precondition:
*     Data exists
*
* Postcondition:
*     Data at front of queue is returned, allowing modification.
*
************************************************************************/
template<typename T>
inline T & QueueA<T>::Front()
{
	if (isEmpty())
		throw Exception("Queue Underflow Exception");

	return m_queue[m_beginning];
}

/**********************************************************************
* Purpose: Returns number of elements in queue.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Number of elements in queue is returned.
*
************************************************************************/
template<typename T>
inline int QueueA<T>::Size() const
{
	return m_size;
}

/**********************************************************************
* Purpose: Returns true of queue is empty.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns true if queue is empty.
*
************************************************************************/
template<typename T>
inline bool QueueA<T>::isEmpty() const
{
	return m_size == 0;
}

/**********************************************************************
* Purpose: Returns true of queue is full.
*
* Precondition:
*     This exists.
*
* Postcondition:
*     Returns true if queue is full.
*
************************************************************************/
template<typename T>
inline bool QueueA<T>::isFull() const
{
	return m_size >= m_maxSize;
}

#endif