#include "Deck.h"
#include <algorithm>
#include <vector>
#include <time.h>

using std::random_shuffle;
using std::vector;

const int SIZEOFDECK = 52;
const int SUITNUM = 4;
const int CARDNUMMAX = 13;

//Easy ol constructor. Sets it to values that make since for a deck of cards. (hint, look up)
Deck::Deck()
	:m_deck(SIZEOFDECK), m_pointer(SIZEOFDECK - 1)
{
	int ptr = 0;
	for (int i = 0; i < SUITNUM; i++) //initializing deck.
	{
		for (int y = 1; y < CARDNUMMAX + 1; y++)
		{
			m_deck[ptr].SetNum(static_cast<CardNumber>(y));
			m_deck[ptr].SetSuit(static_cast<Suit>(i));
			++ptr;
		}
	}
}

//destructor? That it? Cool!
Deck::~Deck()
{ }

//Copy constructor that MAKES SINCE! (totally not trying to sell something)
Deck::Deck(const Deck & copy)
	:m_deck(copy.m_deck), m_pointer(copy.m_pointer)
{ }

//Op= to copy? Yeaaaa.
Deck & Deck::operator=(const Deck & copy)
{
	if (this != &copy)
	{
		m_deck = copy.m_deck;
		m_pointer = copy.m_pointer;
	}
	return *this;
}

/**********************************************************************
* Purpose: Shuffle the deck of cards.
*
* Precondition: This is called BEFORE the pop. This wont make since otherwise.
*
* Postcondition: Deck is shuffled.
*
************************************************************************/
void Deck::Shuffle()
{
	std::srand(time(nullptr));
	vector<Card> tmp(SIZEOFDECK);
	for (int i = 0; i < SIZEOFDECK; i++)
	{
		tmp[i] = m_deck[i];
	}
	random_shuffle(tmp.begin(), tmp.end());
	for (int i = 0; i < SIZEOFDECK; i++)
	{
		m_deck[i] = tmp[i];
	}
}

/**********************************************************************
* Purpose: Pop card from top of deck.
*
* Precondition: There are still cards left.
*
* Postcondition: Top card is popped off and returned. (not actually deleting
*	card, just moving pointer.)
*
************************************************************************/
Card Deck::PopOffTop()
{
	if (m_pointer < 0)
		throw Exception("No more cards to pop!");

	return m_deck[m_pointer--];
}
